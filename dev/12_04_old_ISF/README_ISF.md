﻿
# Iterative Safe Finder

## Licence
/*
        Written by Romain Lannes, 2016-2018

        This file is part of IterativeSafeFinder.

        IterativeSafeFinder is shared under Creative commons licence:

        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)

        See https://creativecommons.org/licenses/by-nc-sa/4.0/
*/


## Description

IterativeSafeFinder aims to retrieve divergent homologues from seed sequences in large metagenomics datasets.
It also comes with an analysis pipe line that performs analyses allowing users to easily get some result and,
select sequences or clusters of interest.
First you should select genes of interest from a gene family. Those genes should form a Connected Component (CC).

Then using the main script you will get remote homologues of the interest family in the metagenome.
Then you could use the annotation script to have some processing done.




## Installation
This software runs with python 3+, on linux platform.
It has not been tested on MAC OS X and will not work on windows.

- dependencies
The pipe line runs with either blast and fastasplit or diamond. If you plan to use only one of this combination you don't need to install the other.

	- blast	

            sudo apt-get install blast 
            or from ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/
	- fastasplit
        
            sudo apt-get install exonerate
            or https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate
	- diamond
	
	        see:
            https://github.com/bbuchfink/diamond
	- ete3
    
            sudo pip3 install ete3
	- igraph
	
            sudo pip3 install python-igraph
            
            you may have to install those two other libs to make it work:
            build-essential and python-dev 

            sudo apt-get install build-essential python-dev


####It will be easier to use the script if you add those programs to your path.

- external ressource,
                
     if avalaible this software will align retrieved sequences to NR NCBI and will look for the
      taxonomy of the closest relatives.
        Inside ISF directory you have three bash scripts. That will help you to donwload NCBI ressources.
        If you want to use NCBI nr you will need the databases,
                    
        you can download it using make_blast_db 
           usage: bash make_blast_db  <OUTPUT_DIR>
        It will download the nr.gz ( nr in fasta, the file weight is in the order of tens of Gb)
        If you want to have the database formatted for blast or diamond uncomment the corresponding lines:
        Inside the script at the end there are 4 commented lines (begin with #)
        remove the # from the one you need and it will format the database.

        To retrieve taxonomical annotations of closest relatives in NR NCBI you need NCBI Taxonomy dump file.
        usage:
            bash make_taxo_db <OUTPUT_DIR>
        It will download and uncompress corresponding files in the specified Output Directory.



## How to use

You should keep all files as they are in this directory, do not move them around.
 

At least you must have:
-
       - A protein fasta of a gene family 
       - A protein fasta of environmental sequences.
       - A path to a non existing directory will be used as output
       - The number of CPU you want ISF to use
      
       python3 <path_to_isf_main.py> -wd <path to working directory> -query <path_to_query_fasta>
        -db_fa <path_to_your_environmental_fasta> -th <Number_of_CPU_to_use> 
        
##### BLAST
By default ISF uses NCBI BLAST. To work with blast it needs three binaries (executable) BLAST, makeblastdb and fastasplit (see installation).

If those programs are in your path (as they should be if you installed them as described previously)
you don't have anything to do.
Else you should tell ISF where it can find them. You can do that by adding this to the previous command
        
        -blast_ <path_to_blast_binary> -mkdb_ <path_to_makeblastdb_binary> -faa_split <path_to_fastasplit_binary> 
##### DIAMOND
If you want to use diamond you must also provide the path to the binary and add this to the minimal working example.
When using diamond you do not need fastasplit, neither makeblastdb or blast. 

        -diamond <path_to_diamond_binary>

##### Threshold settings
    
    -pident_thr <number_percentage>    default: 30.0 Lower threshold limit for percentage of identity.     
    -cov_thr <number_percentage>       default: 80.0 Lower threshold limit for mutual coverage.
    -min_size <number_percentage>      default: 80.0 
    -max_size <number_percentage>      default: 120.0

min size and max size: 
    
    minimum and maximum length of the input gene family sequences are computed.
    We only keep environmental sequences that are:
        minimum * (min_size / 100) < length(env sequence) < maximum * (max_size / 100)
        
 Those values are used to reduce environmental datasets size by only keeping sequences of similar size to the gene family of interest
    
    
Percentage of identity

Mutual Coverage

we highly stress out that you use this program one family at a time if you want to speed up the process, it will be way faster than pooling all your sequences.
A gene family is defined in Sequence Similarity Framework as a Connected Component.

###Output
ISF writes outputs in the specified working directory.
Outputs are the following:
          
    -working_dir/<input_file>:
        Copy of the input file
    -working_dir/sequence_found.faa:
        Fasta of all environnemental sequences found
    -working_dir/sequence_found_and_bases.faa:
        Fasta with input and found environmental sequences
    -working_dir/all_against_all.blastp:
        alignment all against all of input sequences and environmental sequences
        with blast or diamond depending on choosen option.  
    -working_dir/edges/_edges:
        a file that keeps trace of valid edges found for use in post analyses step.
    
#Post-analyses

### Usage
    python3 ISF/analisys_straight/analyses_main.py -isf_wd <output dir of isf> -th <number of cpu to use>
    


###Output   
    
   - First : try to give information about environmental sequences
            
           For each sequences in sequence_found_and_bases.faa it will say:
                if it is a reference or an env sequence.
                the minimal number of iteration needed to retrieve a given environmental sequences.
                One of the nearest (it randomly chooses one if several) reference sequences.
                
                
                
   - Second : perform a network analysis and output cluster of sequences clustering together.

### GUI Graphical User Interface
    In coming 
    # work in progress
    # swithched to PYGTK
    # UX and UI defined
    # Finish event handling
    
## Brief on Sequence Similarity Networks

SSN are graphs where nodes are sequences and an edge is drawn between two nodes if they share 
enough similarity based on threshold (usually coverage and similarity). 




> Written with [StackEdit](https://stackedit.io/).
