
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

# TODO duplication may be find a better ways
def get_seq_one_by_one(file_):
	"""Generator return prompt sequence for each sequence"""

	sequence = ''
	prompt = ''

	for line in file_:

		if line.startswith('>'):

			if sequence:
				yield [prompt, sequence]

			sequence = ''
			prompt = line.strip()[1:]

		else:
			sequence += line.strip()

	yield [prompt, sequence]


def chunck_sequence(sequence, sz=80):
	inc = sz
	cpt = 0
	seq = ""
	while cpt <= len(sequence):
		seq += sequence[cpt:cpt + inc] + '\n'
		cpt += inc
	return seq.strip()


def grep_fasta(sequence_file, id_file, out_file):

	set_id = set()
	if not isinstance(id_file, (set, str)):
		with open(id_file) as id_:
			for line in id_:
				set_id.add(line.strip())
	else:
		set_id = set([elem.replace(">", '') for elem in id_file])

	nb_seq = len(set_id)
	cpt = 0
	with open(sequence_file, 'r') as seqfile, open(out_file, 'w') as out_:
		for prompt, sequ in get_seq_one_by_one(seqfile):
			if prompt in set_id:
				out_.write(">{}\n{}\n".format(prompt, chunck_sequence(sequ)))
				cpt += 1
			if cpt == nb_seq:
				return
