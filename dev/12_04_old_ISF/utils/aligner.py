
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

from abc import ABCMeta, abstractmethod

# abstract class for aligner to add an aligner would be then more simpler
# herite it implement it and ass it in main loop aligner choice

class Aligner(metaclass=ABCMeta):

	@abstractmethod
	def make_db(self, input_, output_):
		pass

	@abstractmethod
	def align(self, **kwargs):
		pass

	@abstractmethod
	def clean(self, **kwargs):
		pass