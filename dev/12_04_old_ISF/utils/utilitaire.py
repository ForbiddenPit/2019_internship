#/usr/local/bin/python3


#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import os
import subprocess
import tempfile
import shutil


class IfbError(Exception):

	# base exeption class for this module
	pass


class NoSequenceFound(IfbError):

	def __init__(self, message):
		self.message = message



def line_generator(line_spt, reverse=False):
	""" input a line of the edges files allow to iterate over this line step by step"""
	# because I love generator
	# generator is beautiful
	# generator if life

	# input a line of the edges files:
	# allow to iterate over this line step by step

	if reverse:

		indice = 1

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[-indice], line_spt[-indice - 10].strip()
			if indice - 1 == 0:
				midle = line_spt[indice - 11: - 1]
			else:
				midle = line_spt[indice - 10:indice - 1]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)

	else:

		indice = 0

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[indice], line_spt[indice + 10].strip()
			midle = line_spt[indice + 1:indice + 10]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)


def rename_blast(file_, dico_file):
	dico = {}
	with open(dico_file) as dic:
		for line in dic:
			spt = line.strip().split("\t")
			dico[spt[1]] = spt[0]

	temp_file = file_ + ".tmp"
	with open(file_) as in_, open(temp_file, 'w') as out:
		for line in in_:
			spt = line.strip().split()
			spt[0] = dico[spt[0]]
			spt[1] = dico[spt[1]]

			out.write("{}\n".format("\t".join(spt)))

	shutil.move(temp_file, file_)



def get_seq_one_by_one(file_):
	"""Generator return prompt sequence for each sequence"""

	sequence = ''
	prompt = ''

	for line in file_:

		if line.startswith('>'):

			if sequence:
				yield [prompt, sequence]

			sequence = ''
			prompt = line.strip()[1:]

		else:
			sequence += line.strip()

	yield [prompt, sequence]


def chunck_sequence(sequence):
	inc = 80
	cpt = 0
	seq = ""
	while cpt <= len(sequence):
		seq += sequence[cpt:cpt + inc] + '\n'
		cpt += inc
	return seq.strip()


def get_all_sequence_id_from_fasta(fasta: str):
	result = []
	with open(fasta) as fa:
		for prompt, seq in fa:
			result.append(prompt)
	return result


def grep_fasta(sequence_file, id_, out_file):
	"""
	if sequence have similar name in the fasta it may not get them all
	this function will retrieve n sequence then return n being the number of uniq id
	:param sequence_file: a fasta file
	:param id_: can be a file one id line or an iterable
	:param out_file: output a fasta file
	:return: None
	"""

	set_id = set()
	if isinstance(id_, str):
		if os.path.isfile(id_):
			with open(id_) as id_1:
				for line in id_1:
					set_id.add(line.strip())
	else:  # iterator set or list
		#set_id = set([elem.replace(">", '') for elem in id_])
		set_id = id_

	nb_seq = len(set_id)
	cpt = 0
	with open(sequence_file, 'r') as seqfile, open(out_file, 'w') as out_:
		for prompt, sequ in get_seq_one_by_one(seqfile):
			if prompt in set_id:

				out_.write(">{}\n{}\n".format(prompt, chunck_sequence(sequ)))
				cpt += 1
			if cpt == nb_seq:
				return


def get_seq_number(file):
	cpt = 0
	with open(file) as in_:
		for pr, se in get_seq_one_by_one(in_):
			#print("seq_id: ", pr )
			cpt += 1
	return cpt


def update_edges_find(file_, run_, id_get):

	if run_ > 1:
		field = 11 + 10 * (run_ - 1)
	else:
		field = 11
	field -= 1
	with open(file_) as f_:
		for line in f_:
			spt = line.strip().split()
			if field < len(spt):
				id_ = spt[field]
				if id_ not in id_get:
					id_get.add(id_)

		id_get = set(list(filter(None, id_get)))
	return id_get


def remove(*args):

	for element in args:
		try:
			if os.path.isfile(element):
				os.remove(os.path.abspath(element))

			elif os.path.isdir(element):
				shutil.rmtree(os.path.abspath(element))

			else:
				print("Warning: file {} didn't exists and thus can't be remove".format(element))

		except:
			print("Warning: file {} didn't exists and thus can't be remove".format(element))


def make_fasta_from_edge_dir(edge_dir, out_file, metage_fasta):

	set_ids = set()

	list_of_edges_files = os.listdir(edge_dir)
	path_ = os.path.abspath(edge_dir)
	list_of_edges_files = [path_ + '/' + elem for elem in list_of_edges_files]

	for edge_file in list_of_edges_files:

		with open(edge_file) as ed_file:

			for line in ed_file:
				for ids, midle, round in line_generator(line.strip().split('\t')):
					id_1, id_2 = ids
					set_ids.add(id_1)
					set_ids.add(id_2)

	with open(out_file, 'a') as output_file, open(metage_fasta) as met_fa:

		for prompt, sequence in get_seq_one_by_one(file_=met_fa):
			if prompt in set_ids:
				output_file.write(">{}\n{}\n".format(prompt, chunck_sequence(sequence)))


def get_line_number(file_):
	cpt = 0
	with open(file_) as f:
		for line in f:
			if line.strip():
				cpt += 1
	return cpt


def rename_fasta(dico_file, fasta):
	my_dico = {}

	with open(dico_file) as dico:

		for line in dico:
			spt = line.strip().split("\t")
			my_dico[spt[1]] = spt[0]
	temp_fasta = fasta + ".temp"

	with open(temp_fasta, 'w') as my_temp_file, open(fasta) as fasta_:

		for prompt, seq in get_seq_one_by_one(fasta_):
				my_temp_file.write(">{}\n{}\n".format(my_dico[prompt], chunck_sequence(seq)))

	shutil.move(temp_fasta, fasta)


def copy_files(src: list, dest: str):
	with open(dest, 'wb') as out_:
		for input_file in src:
			with open(input_file, 'rb') as inp_:
				shutil.copyfileobj(inp_, out_, 1024*1024*10)


def concat_file(dest, *args):
	with open(dest, "ab") as wfd:
		for f in sorted(args):
			with open(f, 'rb') as fd:
				#print(f)
				shutil.copyfileobj(fd, wfd, 1024*1024*10)
				wfd.write(b"\n")


def update_set_from_Idfile(file_, set_):
	with open(file_) as f:
		for line in f:
			set_.add(line.strip())


### Managing input FASTA
def make_dictionary_and_get_size(input_, output, o_dico, start=1):

	first_bool = True
	id_indice = start  # id start for one by default

	with open(input_, buffering=1000000) as input_file, open(output, 'w') as out_file, open(o_dico, 'w') as dico_file:

		for prompt, sequence in get_seq_one_by_one(input_file):

			# This part aim to find the min max sequence length
			length_seq = len(sequence)
			if first_bool:
				min_seq = length_seq
				max_seq = length_seq
				first_bool = False

			if length_seq < min_seq:
				min_seq = length_seq

			if length_seq > max_seq:
				max_seq = length_seq
			# end

			# next It create the dictionary
			dico_file.write("{}\t{}\n".format(prompt, id_indice))
			out_file.write(">{}\n{}\n".format(id_indice, chunck_sequence(sequence)))

			# we increments the value of the indice
			id_indice += 1

	return [min_seq, max_seq, id_indice]


def screen_by_size_and_make_dico_and_fasta(min_len, max_len, indice_dico, input_, dico_o, fasta_o,
                                             min_factor, max_factor):

	# threshold for accepting a sequence
	min_sequence_len = int(min_len * (min_factor / 100)) - 1
	max_sequence_len = int(max_len * (max_factor / 100)) + 1

	cpt = 0
	metagenome = 0

	with open(input_, buffering=10000000) as input_file, open(dico_o, 'w') as dico_file, open(fasta_o, 'w') as fasta_out:

		for prompt, sequence in get_seq_one_by_one(input_file):

			# if the sequence is in the threshold we write
			if min_sequence_len <= len(sequence) <= max_sequence_len:
				dico_file.write("{}\t{}\n".format(prompt, indice_dico))
				fasta_out.write(">{}\n{}\n".format(indice_dico, chunck_sequence(sequence)))

				cpt += 1
				indice_dico += 1

			metagenome += 1
	return cpt, metagenome


def manage_input_fasta(interest_family, metagenome, dico_family_interest, dico_metagenome, fasta_small_metage,
		 interest_family_simpl,  min_factor, max_factor):
	"""  """
	# first make dictionnary of the family interest and return the min max len of sequence in
	min_len, max_len, indice_dico = make_dictionary_and_get_size(input_=interest_family, output=interest_family_simpl,
																 o_dico=dico_family_interest)

	# make dictionnary of metagenome also filter by coverage to select only possible hit and so
	#  limit the metagenome size.
	cpt, metagenome = screen_by_size_and_make_dico_and_fasta(min_len=min_len, max_len=max_len, indice_dico=indice_dico,
												input_=metagenome, dico_o=dico_metagenome,
												fasta_o=fasta_small_metage, min_factor=min_factor, max_factor=max_factor)

	print("Gene Family:\n")
	print("\tshortest sequence: {} aa\n\tlongest sequence: {} aa\n".format(min_len, max_len))
	print("From the {} sequences of input database:".format(metagenome))
	print("{} are in the length rage of the gene family and are selected".format(cpt))
# 	print("shortest sequence: {} aa\nlongest sequence: {} aa\n\
# metagenome size: {}\nnumber of selected sequences: {}".format(min_len, max_len, metagenome, cpt))

	if cpt <= 0 :
		print("no sequences selected")
		raise AssertionError

	# we return the number of sequence that pass the coverage filter for -dbsize option
	# 	nd the min max size
	return (cpt, min_len, max_len)


### End Managing input FASTA

def replace_round(id, dico, round, start_seq):
	node_instance = dico[id]

	if not node_instance.round:
		node_instance.round = round
		node_instance.relative_seq_start = start_seq

	else:
		if node_instance.round > round:
			node_instance.round = round
			node_instance.relative_seq_start = start_seq

	return None