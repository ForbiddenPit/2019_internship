
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

#!/usr/local/bin/python3.5


import time
from multiprocessing import Pool
import subprocess
import tempfile
import os
from shutil import copyfileobj, move

from utils.aligner import Aligner
from utils.utilitaire import get_seq_one_by_one, remove, get_seq_number, concat_file, get_line_number



def number_of_split(file_, maxsplit):
	"""return the number of split to do"""

	sequence_number = get_seq_number(file_)
	# int(subprocess.check_output(['grep', '-c', '>', '{}'.format(file_)]).decode('utf-8'))
	if sequence_number < maxsplit:
		toreturn = sequence_number - 1
	else:
		toreturn = maxsplit
	if toreturn < 2:
		toreturn = 1
	return toreturn


def blast(args_tuple):
	"""Just launch the blast"""

	query, database, output_file, blastp_ = args_tuple

	cmd = ['{}'.format(blastp_), '-query', '{}'.format(query), '-db', '{}'.format(os.path.abspath(database)),
		   '-out', '{}'.format(output_file),
		   '-outfmt', "6 qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen",
	        "-seg", "yes", "-soft_masking", "true", "-max_target_seqs", "10000"]

	try:
		# print("launching blast with following command = {}".format(" ".join(cmd)))
		child = subprocess.Popen(cmd)
		child.wait()
	except:
		raise
	else:
		return 0

class Blast(Aligner):

	def __init__(self, make_db_bin, blastp_bin, threads, fastasplit, working_dir):
		self.blastp = blastp_bin
		self.mkdb = make_db_bin
		self.threads = threads
		self.fastasplit = fastasplit
		self.working_dir = working_dir

	def make_db(self, input_, output_):
		cmd = '{} -dbtype prot -hash_index -parse_seqids -in {} -out {}'.format(self.mkdb, input_, output_)
		print("making blast_db  with the command line:\n\t {}".format(cmd))
		child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL)
		child.wait()


	def align(self, db, output, fasta, max_seq_target, cover, eval_, id, **kwargs):

		with tempfile.TemporaryDirectory(prefix='temp_split_dir', dir=self.working_dir) as tmp_split_dir,\
				tempfile.TemporaryDirectory(prefix='temp_outBlast_dir', dir=self.working_dir) as blast_out_dir:

			numberspl = number_of_split(file_=fasta, maxsplit=self.threads)

			start_time = time.time()
			number_of_sequence = get_seq_number(fasta)
			print('there are {} sequences to blast\n'.format(number_of_sequence))
			print("splitting the query fasta to launch several BLAST")
			# split fasta
			child = subprocess.Popen("{} -f {} -o  {} -c {}".format(self.fastasplit, fasta, tmp_split_dir, numberspl), shell=True)
			child.wait()


			liste_file = os.listdir(tmp_split_dir)  # then we get all subfile
			path = os.path.abspath(tmp_split_dir) + '/'  # absolute path to the file

			liste_file = [os.path.join(os.path.abspath(tmp_split_dir), element) for element in liste_file]  # file with absolute path

			liste_argument = list()  # this list will contain each argument tuple for multiprocess
			#print("{}\n".format("\n".join(liste_file)))
			for file in liste_file:
				if os.stat(file).st_size == 0:  # empty file
					continue

				query = file
				output_this = os.path.join(blast_out_dir,  'out_{}'.format(os.path.basename(query)))
				argument_tuple = (query, db, output_this, self.blastp)
				liste_argument.append(argument_tuple)


			print("Launching BLAST on splited files")
			with Pool(processes=self.threads) as pool:
				out_code = pool.map(blast, liste_argument)
			liste_file = [os.path.join(blast_out_dir, elem) for elem in os.listdir(blast_out_dir)]
			#print("{}\n".format("\n".join(liste_file)))
			concat_file(output, *liste_file)


		return 0

	def clean(self, align_file, thres_tpl, ban_list):

		header = "qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen".split()
		dictHe = dict((value, indice) for indice, value in enumerate(header))

		def cover_f(start, end, length):
			return (end - start + 1) / length

		def pass_threshold(spt, dictHe=dictHe, eval_=thres_tpl.eval, cov=thres_tpl.mutual_cov, pid=thres_tpl.pid):
			tcov = cover_f(int(spt[dictHe["sstart"]]), int(spt[dictHe["send"]]),
			               int(spt[dictHe["slen"]]))
			qcov = cover_f(int(spt[dictHe["qstart"]]), int(spt[dictHe["qend"]]),
			               int(spt[dictHe["qlen"]]))

			if tcov < cov or qcov < cov:
				return False

			elif float(spt[dictHe["pident"]]) < pid:
				return False

			elif float(spt[dictHe["evalue"]]) > eval_:
				return False

			else:
				return True

		#new_blast = align_file + ".cleaning.tmp"

		if not ban_list:
			ban_list = set()


		temp_file = align_file + ".temp_aln_file"
		id_0 = None
		dico_best_hit = {}
		with open(align_file) as in_f, open(temp_file, "w") as out:

			for line in in_f:
				spt = line.strip().split()
				if not spt or spt[0] == spt[1]:
					continue
				try:
					target_id = spt[1]
					query_id = spt[0]
				except:
					print(line, spt)
					raise

				if spt[1] in ban_list or not pass_threshold(spt):
					continue

				if id_0 != spt[0]:
					id_0 = spt[0]

					for k, v in dico_best_hit.items():

						out.write("{}\n".format("\t".join(v)))

					dico_best_hit = {(query_id, target_id): spt}

				else:
					key = (query_id, target_id)
					if key not in dico_best_hit:
						dico_best_hit[key] = spt

					else:
						old_spt = dico_best_hit[key]
						if float(old_spt[4]) < float(spt[4]):  # compare e-value
							dico_best_hit[key] = spt

			if dico_best_hit:
				for k, v in dico_best_hit.items():
					out.write("{}\n".format("\t".join(v)))


		move(temp_file, align_file)
