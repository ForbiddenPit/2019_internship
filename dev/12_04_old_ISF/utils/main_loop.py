
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

# Path_tuple = namedtuple("Path_tuple", ["db_dico", "db_faa_simpl", "db_faa_src", "db_fmt", "q_dico", "q_faa_simpl",
#                                       "aln_out_dir", "edges_dir", "current_faa", "ban", "id_found", "id_previous"])


import utils.utilitaire
import os
import time
import utils.edges_file_manager
import utils.new_edges

def main_isf(path_tpl, thres_tpl, args, aligner):
	"""
	This function is the main function of ISF it made the iterations.
	:param path_tpl: a named tuple with path
	:param args: the ars from the command line
	:param aligner: an instance of a class that herite Aligner
	:return: None or Error Code
	"""

	edges_find, ban_set, current_id_find, previous_id = set(), set(),  set(),  set()

	print('Alignment of all query against all query')
	print("")
	start_time = time.time()
	aln_family_ava = os.path.join(path_tpl.aln_out_dir, "family_AvA.aln")
	aligner.make_db(input_=path_tpl.db_faa_simpl, output_=path_tpl.db_fmt)
	aligner.align(db=path_tpl.db_fmt, output=aln_family_ava, fasta=path_tpl.q_faa_simpl,
	              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)
	print('alignment takes {} secondes'.format(round(time.time() - start_time, 2)))
	aligner.clean(align_file=aln_family_ava, thres_tpl=thres_tpl, ban_list=ban_set)

	if not os.path.exists(aln_family_ava) or os.stat(aln_family_ava).st_size == 0:
		print("Warning: your gene family is not a family")
	#
	# else:
	#
	# 	first_edges = os.path.join(path_tpl.edges_dir, '_edges_1')
	#
	# 	with open(aln_family_ava, 'r') as blast_out:
	#
	# 		for line in blast_out:
	# 			splited = line.strip().split('\t')
	#
	# 			if splited[0] == splited[1]:
	# 				continue
	#
	# 			with open(first_edges, 'a') as edges_f:
	# 				#to_write = splited[0] + '\t' + '\t'.join(splited[2:]) + '\t' + splited[1] + '\n'
	# 				#edges_f.write(to_write)

	print("")
	print("\t-------------------")
	print("")

	# now the main loop
	print('Starting alignment of query against db')
	print("")
	print("\t-------------------")
	print("")
	liste_run_find = [(0, 0)]  # a liste containing several tuple with number: run(s) , sequence(s) find
	run_number = 1
	edges_file = os.path.join(path_tpl.edges_dir, '_edges_')
	actual_query = ""
	while run_number <= args.run:
		# print("all edges found:", edges_find)
		# print("previous:  ",  len(previous_id), previous_id)
		# print("ban set", ban_set, "banset_size : ", len(ban_set))

		print("run number : {}\n".format(run_number))
		print('Starting the alignment procedure :')
		current_aln = os.path.join(path_tpl.aln_out_dir, '_' + str(run_number))
		start_time = time.time()
		try:

			if run_number == 1:
				aligner.align(db=path_tpl.db_fmt, output=current_aln, fasta=path_tpl.q_faa_simpl,
				              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)

			else:

				aligner.align(db=path_tpl.db_fmt, output=current_aln, fasta=actual_query,
				              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)

			print('alignment takes {} secondes'.format(round(time.time() - start_time, 2)))



		except:
			print(actual_query)
			print(run_number)
			raise

		try:
			print("")
			#print("aln_size: {}".format(utils.utilitaire.get_line_number(current_aln)))
			print("cleaning alignment\n")
			#print("run number: {}, ban set size: {}".format(run_number, len(ban_set)))
			aligner.clean(current_aln, thres_tpl=thres_tpl, ban_list=ban_set)

			# check empty file
			if os.stat(current_aln).st_size == 0:
				return aligner, edges_find
				#raise utils.NoSequenceFound("No sequences found")

		except:
			if run_number == 1:
				print("no homologous sequences were found.")
			else:
				print("\n\nlast iteration return empty file, considere the program ended, \
	if you are not satisfied try changing the parameter")
			raise utils.NoSequenceFound("No sequences found")

		#print("\t-------------------")
		print("updating edge file from the alignment in order to assert the conservation of alignment position")
		# we reading this file again after cleaning it.
		# It may seems a waste of cpu and time but i believe the overhead is worth the better clarity and simplicity
		# one function cleaning, the other updating form the cleaned
		# now we should look to overlap
		# and add corresponding sequence to the results file /edges/_edges_
		start_time = time.time()
		#print("size of current cleaned alignment : {}".format(utils.utilitaire.get_line_number(current_aln)))

		#current_id_find = utils.edges_file_manager.manages_edges_file(edges_file, current_aln, args.test_all_chain, args.cov_thr)
		current_id_find = utils.new_edges.manages_edges_file(edges_file, current_aln, args.test_all_chain, args.cov_thr, run=run_number)

		print('Updating edges takes {} secondes'.format(round(time.time() - start_time, 2)))
		try:
			assert current_id_find

		except AssertionError:
			return aligner, edges_find
			#raise utils.utilitaire.NoSequenceFound("no more sequences found round {}".format(str(run_number)))


		print("extracting newly found sequences and make the corresponding fasta\n")
		# get the sequence corresponding to blast output
		try:
			actual_query = os.path.join(path_tpl.fasta_dir, str(run_number) + '_fasta.fa')

			utils.utilitaire.grep_fasta(sequence_file=path_tpl.db_faa_simpl, id_=current_id_find, out_file=actual_query)
		except utils.utilitaire.NoSequenceFound:
			raise
		except:
			raise
		#print("current id len:{}: {}".format(len(current_id_find), sorted(list(current_id_find), key= lambda x: int(x))))
		edges_find.update(current_id_find)

		liste_run_find.append((run_number, len(edges_find)))

		print('run_number: {}, total sequences find: {}\n'.format(liste_run_find[-1][0], liste_run_find[-1][1]))

		if run_number >= 2:
				print("adding edge to ban list to avoid cycle")
				ban_set.update(previous_id)
				previous_id = current_id_find.copy()
		else:
			previous_id = current_id_find.copy()

		run_number += 1
		print("")
		print("\t-------------------")
		print("")
	return aligner, edges_find

# edges_find, ban_set, current_id_find, previous_id = set(), set(),  set(),  set()
# Path_tuple = namedtuple("Path_tuple", ["db_dico", "db_faa_simpl", "db_faa_src", "db_fmt", "q_dico", "q_faa_simpl",
#                                       "aln_out_dir", "edges_dir", "current_faa", "ban", "id_found", "id_previous"])
