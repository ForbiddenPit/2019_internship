
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import subprocess
import os
from utils.aligner import Aligner
import shutil
from utils.utilitaire import get_seq_number, copy_files

class Diamond(Aligner):

	def __init__(self, make_db_bin, diamond_bin, threads):


		self.threads = threads
		self.dmd_bin = diamond_bin
		self.make_db_bin = make_db_bin

	def make_db(self, input_, output_, **kwargs):
		cmd = "{} --in {} --db {}".format(self.make_db_bin, input_, output_)
		print("making a diamond databse with this current line:\n {}\n".format(cmd))
		child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		child.wait()

	def align(self, db, output, fasta
	          , max_seq_target,
	          cover, eval_, id, **kwargs):

		number_of_sequence = get_seq_number(fasta)
		print('there are {} sequences to blast'.format(number_of_sequence))

		cmd = "{} blastp --db {} --query {} --out {} --id {} --outfmt 6 qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen --threads {} \
--query-cover {} --subject-cover {} --evalue {} --id {} --max-target-seqs {}\
--more-sensitive".format(self.dmd_bin, db, fasta, output, id, self.threads, cover, cover, eval_, id, max_seq_target)
		#print("launching diamond with the current command:\n{}\n".format(cmd))
		child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		child.wait()

	def clean(self, align_file, thres_tpl, ban_list):

		if not ban_list:
			ban_list = set()


		temp_file = align_file + ".temp_aln_file"
		id_0 = None
		dico_best_hit = {}
		with open(align_file) as in_f, open(temp_file, "w") as out:

			for line in in_f:
				spt = line.strip().split()
				if spt[1] in ban_list or spt[0] == spt[1]:
					continue

				if id_0 != spt[0]:
					id_0 = spt[0]
					for k, v in dico_best_hit.items():
						out.write("{}\n".format("\t".join(v)))
					dico_best_hit = {(spt[0], spt[1]): spt}

				else:
					key = (spt[0], spt[1])
					if key not in dico_best_hit:
						dico_best_hit[key] = spt

					else:
						old_spt = dico_best_hit[key]
						if float(old_spt[2]) > float(spt[2]):  # compare e-value
							dico_best_hit[key] = spt

			if dico_best_hit:
				for k, v in dico_best_hit.items():
						out.write("{}\n".format("\t".join(v)))

		shutil.move(temp_file, align_file)

		return 0


