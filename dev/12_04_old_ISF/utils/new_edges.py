import os
import re
import shutil


def blast_todict(blast_output):
	dico_blast = dict()

	with open(blast_output) as blast_file:

		for line in blast_file:
			spt = line.strip().split('\t')
			to_keep = spt
			tar_id = spt[1]
			query_id = spt[0]

			# first time we see this id
			if query_id not in dico_blast:
				# if this query have match something
				dico_blast[query_id] = {tar_id: to_keep}

			else:
				# we have already this tar id but not the query id
				if tar_id not in dico_blast[query_id]:

					dico_blast[query_id][tar_id] = to_keep

				else:
					old_line = dico_blast[query_id][tar_id]

					if float(old_line.split('\t')[0]) > float(line.split('\t')[0]):
						dico_blast[query_id][tar_id] = to_keep

	return dico_blast

reg = re.compile("\((\d+),(\d+)\)")
def parse_edge_line(line, reg=reg):
	x = line.strip().split(";")
	round_ = x[0]
	x = x[1].strip().split()
	query = x[0]
	target = x[-1]
	search = reg.search(x[1])
	start = search.group(1)
	end = search.group(2)
	return (round_, query, target, start, end)


def overlap(min1, min2, max1, max2):

	return max(0, min(int(max2), int(max1)) - max(int(min1), int(min2)) + 1)

#qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen
#0      1       2       3       4       5       6   7       8   9   10

def add_eges_previous(run, edges_file, edges_file_tmp, dico_blast, threshold, id_get):

	set_test = set()

	with open(edges_file) as in_, open(edges_file_tmp, 'w') as out_:

		for line in in_:
			out_.write(line)
			round_, query, target, start, end = parse_edge_line(line)

			if target in dico_blast and target not in set_test:

				for key, v in dico_blast[target].items():
					if ((key, target)) not in set_test:
						set_test.add((key, target))
						#print(v)
						over = overlap(min1=int(start), min2=int(v[5]), max1=int(end),
						               max2=int(v[6]))

						if (over/int(v[10])) < threshold:
							continue

						else:

							id_get.add(key)
							out_.write("{} ; {}\t({},{})\t{}\n".format(run, target, v[7], v[8], key))


def add_edges_no(run, edges_file, edges_file_tmp, dico_blast, threshold, id_get):
	with open(edges_file, 'a') as f:
		for k, v in dico_blast:
			for k1, v1 in v:
				f.write("{} ; {}\t({},{})\t{}\n".format(run, k, v1[7], v1[8], k1))


def construct_dictionnary(dict_files):
	dico = {}
	with open(dict_files) as dict_fil:
		for line in dict_fil:
			spt = line.strip().split('\t')
			dico[spt[1]] = spt[0]

	return dico


def edge_str(round_, query, target, start, end):
	return "{} ; {}\t({},{})\t{}".format(round_, query, start, end, target)


def rewrite(edges, dico):
	with open(edges, 'r') as int_f, open("{}_.tmp".format(edges), 'w') as tmp:
		for line in int_f:
			round_, query, target, start, end = parse_edge_line(line)
			query = dico[query]
			target = dico[target]
			tmp.write("{}\n".format(edge_str(round_, query, target, start, end)))

	shutil.move("{}_.tmp".format(edges), edges)


def rename_edges(dico_db, dico_query, edges_dir):
	dico = {}
	dico.update(construct_dictionnary(dico_db))
	dico.update(construct_dictionnary(dico_query))

	all_edges = os.path.join(edges_dir, '_edges_')

	rewrite(all_edges, dico)


def manages_edges_file(edges_file: str, blast_output: str, all_chain, cover_thr, run):

	temp_file = edges_file + '.tmp'
	id_get = set()
	try:
		assert all_chain in ["no", "previous", "all"]

	except AssertionError:
		print(" all chain should have one this value only : no, previous, all")
		raise

	if os.path.isfile(edges_file):
		dico_blast = blast_todict(blast_output=blast_output)

		if all_chain == "previous":
			add_eges_previous(run, edges_file, temp_file, dico_blast, cover_thr, id_get)
			shutil.move(edges_file + '.tmp', edges_file)

		elif all_chain == "all":
			shutil.move(edges_file + '.tmp', edges_file)

		else:
			pass
			#

		#qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen
		#0      1       2       3       4       5       6   7       8   9   10
												#0      1   2       3   4   5


	else:
		with open(blast_output) as blast_file, open(edges_file, "w") as out_:
			for line in blast_file:
				line = line.strip().split()
				out_.write("{} ; {}\t({},{})\t{}\n".format(run, line[0], line[7], line[8], line[1]))
				id_get.add(line[1])
	return id_get