# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import os
import argparse
import re

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-a', '--annotfile',
                    metavar='FILE', help='Annotation file (REQUIRED). Default: none.',
                    default='', dest='annot_file')
parser.add_argument('-s', '--selectfile',
                    metavar='FILE', help='Selected file (REQUIRED). Default: none. ',
                    default='', dest='select_file')
parser.add_argument('-o', '--output',
                    metavar='DIR', help='Output directory (REQUIRED). Default: none. ',
                    default='', dest='output_dir')
args = parser.parse_args()

# Function opening the dictionary table and putting it in a dictionary
def open_dictionary(path):
    dic = dict()
    with open(path) as d:
        for line in d:
            ls = line.split("\t")
            id = ls[0]
            annot = ls[1]
            dic[id] = annot
    return dic

def annot_select(file, dictionary, dir):
    name = "annot_visual_select.txt"
    output = open('{0}/{1}.txt'.format(dir, name), 'a')
    with open(file) as f:
        for line in f:
            line = line.rstrip('\n')
            if re.match(r"^[0-9]", line) is None:
                output.write(line+'\n')
            else:
                output.write(line+'\t'+dic[line]+'\n')
    output.close()

########
# Main #
########

# Args var storage
sf = args.select_file
af = args.annot_file
od = args.output_dir

# Store files paths and create dictionary from dict file
dic = open_dictionary(af)

# Create new file appending a file to its annotation
annot_select(sf, dic, od)