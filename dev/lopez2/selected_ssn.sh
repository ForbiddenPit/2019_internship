#!/bin/bash

# Ex:
# $1 /home/disque_4To_A/pierre/data/lopez2/all_new_cc/selected/graph_files/visual_select.txt
# $2 /home/disque_4To_A/pierre/data/lopez2/all_new_cc/selected/graph_files

echo "best" > $1
find $2/*/best/* -printf "%f\n" | cut -d"." -f1 >> $1
echo "\n" >> $1

echo "good" >> $1
find $2/*/good/* -printf "%f\n" | cut -d"." -f1 >> $1
echo "\n" >> $1

echo "meh" >> $1
find $2/*/meh/* -printf "%f\n" | cut -d"." -f1 >> $1
echo "\n" >> $1

echo "no" >> $1
find $2/*/no/* -printf "%f\n" | cut -d"." -f1 >> $1