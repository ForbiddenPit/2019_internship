# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import os
import argparse
import glob
import multiprocessing

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-d', '--dictfile',
                    metavar='FILE', help='Dictionary file (REQUIRED). Default: none.',
                    default='', dest='dict_file')
parser.add_argument('-f', '--filesdir',
                    metavar='DIR', help='Selected files directory (REQUIRED). Default: current directory. Format: ./*/ or ~/*/',
                    default='.', dest='files_dir')
args = parser.parse_args()

# Function creating a dictionary containing every bam file path for each run
def files_handling(path):
    files = list()
    filesPath=path+"/*"
    # Fetching the files
    for d in glob.glob(filesPath):
        files.append(d)
    return(files)

# Function opening the dictionary table and putting it in a dictionary
def open_dictionary(path):
    dic = dict()
    with open(path) as d:
        for line in d:
            ls = line.split("\t")
            if ls[0] == "id_num":
                continue
            else:
                id = ls[0]
                taxo = ls[6].split("::")[0]
                dic[id] = taxo
    return dic

def genes_select(file, dictionary, output_dir):
    name_output = os.path.basename(file)
    tsv_output = open('{0}/{1}.tsv'.format(output_dir, name_output), 'a')
    tsv_output.write("source"+'\t'+"target"+'\t'+"annot"+'\t'+"annot"+'\t'+"e-val"+'\t'+"pid"+'\t'+"cov"+"\n")
    with open(file) as f:
        for line in f:
            ls = line.split('\t')
            
            id1 = ls[0]
            id2 = ls[1]
            
            taxo1 = dic[id1]
            taxo2 = dic[id2]
            
            e_val = ls[2]
            pid = ls[3]
            cov = ls[4]
            
            tsv_output.write(id1+'\t'+id2+'\t'+taxo1+'\t'+taxo2+'\t'+e_val+'\t'+pid+'\t'+cov)
    tsv_output.close()

########
# Main #
########

# Args var storage
files_dir = args.files_dir
dict_file = args.dict_file

# Store files paths and create dictionary from dict file
files = files_handling(files_dir)
dic = open_dictionary(dict_file)

# Make new directory if not exists
output_dir = os.path.join(files_dir, "graph_files") 
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Multiprocessing to treat all files at a time
jobs = list()
for f in files:
    jobs.append(multiprocessing.Process(target=genes_select, args=(f, dic, output_dir)))

for j in jobs:
    j.start()

for j in jobs:
    j.join()