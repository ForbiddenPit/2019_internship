#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import os
import argparse
import re
import glob
from Bio import SeqIO

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-f', metavar='DIR', help='Fasta directory (REQUIRED). Default: none.',
					default='', dest='fasta_dir')
parser.add_argument('-d', metavar='FILE', help='Dictionary file (REQUIRED). Default: none. ',
					default='', dest='dict_file')
args = parser.parse_args()

# Function opening the annotation table and putting it in a dictionary
def open_dictionary(dict_file):
	dic = dict()
	with open(dict_file) as d:
		for line in d:
			ls = line.split("\t")
			ids = int(ls[0])
			annot = '\t'.join(ls[1:])
			dic[ids] = annot
	return dic



########
# Main #
########

# Args var storage
fasta_dir = args.fasta_dir
dict_file = args.dict_file

# Creating annot dict
dic = open_dictionary(dict_file)

files_path = str(fasta_dir)+'*.fasta'

# Opening fasta and fetching annotation
output = open('{}/annot.txt'.format(fasta_dir), 'w')
for file in glob.glob(files_path):
	cc_number = os.path.basename(file).strip('.fasta')
	output.write('CC_'+str(cc_number)+': '+str(dic[int(cc_number)]+'\n'))