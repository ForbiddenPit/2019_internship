#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

# USAGE:
# python3 ~/Documents/2019_internship/dev/lopez2/fasta_sorting.py
# -f /home/disque_4To_A/pierre/data/ISF/lopez2/all_seq_new.faa

import os
import argparse
import re
import sys
import glob
from Bio import SeqIO

# Function opening the dictionary table and putting it in a dictionary
def open_fasta(fasta_file):
	fasta_dict = dict()
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, "fasta"):
			fasta_dict[record.id] = record.seq
	return fasta_dict

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-f', metavar='FILE', help='Fasta file (REQUIRED). Default: none.',
					default='', dest='fasta_file')
args = parser.parse_args()

########
# Main #
########

print('opening fasta file for reference...')
fasta_dict = open_fasta(args.fasta_file)
print('fasta file opened successfuly')

for i in glob.glob('./*.tsv'):
	output = open('{}.fasta'.format(os.path.basename(i).strip('.tsv')), 'w+')
	with open(i) as t:
		id_list = set()
		for line in t:
			if re.match('[0-9]', line):
				id_list.add(line.split('\t')[0])
			else:
				continue
		print('writing file {}...'.format('{}.fasta'.format(os.path.basename(i).strip('.tsv'))))
		for prot_id in id_list:
			output.write('>'+str(prot_id)+'\n'+str(fasta_dict[prot_id])+'\n')
		print('done')