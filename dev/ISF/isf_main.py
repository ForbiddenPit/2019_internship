#!/usr/local/bin/python3.5
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#        Contact romain.lannes@protonmail.com object ISF_report
#
# I would like to thanks all beta testers:
# Charles Bernand, Jerome Teuliere, Pierre Martin, Lucir Bittner, Michael Macey.
# I would also like to give special thanks to Lucie Bittner for being the first person to use it!

import argparse
import os
import sys
import shutil
from utils import blast_aligner, diamond_aligner, utilitaire, main_loop, mmseq_aligner, louvainCommunity#, edges_file_manager

from collections import namedtuple
import utils.new_edges


######################
### Start args #######
######################
parser = argparse.ArgumentParser(description='This script will make blast iteration.')

parser.add_argument('-wd', help='working directory', type=str, default='blast_thread_wd', required=True)
parser.add_argument('-th', help='number of thread to use', type=int, required=True)
parser.add_argument('-run', help='number of run to do', type=int, default=10**6)
# parser.add_argument('-verbose', help='add verbosity', action="store_true")


# TODO add evalue

parser.add_argument('-pident_thr', help='threshold limit for identity default = 30.0, value > 0.0',
					type=float, default=30.0)

parser.add_argument('-cov_thr', help='threshold limit fo coverage default = 80.0, value > 0.0',
					type=float, default=80.0)


parser.add_argument('-eval_thr', help='threshold limit fo evalue default = 0.00001, value > 0.0',
					type=float, default=0.00001)

parser.add_argument('-min_size', help=' %% of the shortest query size use to limit by size blasted sequence'
									  'default 80',
					 default=80, type=float)

parser.add_argument('-max_size', help=' %% of the longest query size use to limit by size blasted sequence'
									  'default 120',
					 default=120, type=float)

parser.add_argument('-test_all_chain', help='does we test conservation of alignment position, default previous',
                    action='store', choices=["no", "previous", "all"], default="previous")


#################################
#####  Path to some binaries  ###
#################################

parser.add_argument('-diamond', help="path to diamond binary if set the software will use diamond instead of blast.")
parser.add_argument('-mmseqs', help="path to mmseqs binary if set the software will use mmseqs instead of blast.")


parser.add_argument('-blast_', help=" where is blast ? default use: blastp"
									" if not in PATH, indicate the path to exe",
					default='blastp')

parser.add_argument('-mkdb_', help='where is makeblastdb ? default use: makeblastdb'
									' if not in PATH, indicate the path to exe',
					default='makeblastdb')

parser.add_argument('-faa_split', help='where is exonerate fastasplit, default use: fastasplit',
					default='fastasplit')

######################################
####  PATH TO FILES     ##############
######################################

parser.add_argument('-query', help='query a query in fasta', required=True)

parser.add_argument('-db_fa', help='fasta of the database fasta format', required=True)

parser.add_argument("-debug", action="store_true")

parser.add_argument('-nr_db', help='path to nr database, if set when the iteration finish \
results are blast against nr', type=str)

parser.add_argument('-NoAvA', help='if set ISF will not make all against all alignment of sequences (familly + found)',
                    action='store_true')
parser.add_argument('-Nocom', help='if set ISF will not compute community on the all agaisnt all alignment',
                    action='store_true')

parser.add_argument("-edge_file", help="keep edges file "
                                         "will increase the hard drive memory consumption",
                    action='store_true')

parser.add_argument("-remove_aln", help="remove aln file",
                    action='store_true')
args = parser.parse_args()

# Next step of analysis is a blast against nr I could do this here and add it to the output files
if args.diamond:
	print("alignment will be perform using diamond")


#args.cov_thr /= 100

# working dir
start_family = os.path.abspath(args.query)
database_fa = os.path.abspath(args.db_fa)
try:
	working_dir = os.path.abspath(args.wd)
	os.mkdir(working_dir)
	os.chdir(working_dir)
except FileExistsError:
	print("working dir exist, remove the existing file or change the name")
	sys.exit(1)
except:
	raise


db_dico = os.path.join(working_dir, 'database.dico'.format(os.path.basename(args.db_fa)))
db_simpl_fasta = os.path.join(working_dir, 'database_DBSimpleID.faa'.format(os.path.basename(args.db_fa)))

database_dir = os.path.join(working_dir, "ref_dir_db")

db_fmt = os.path.join(database_dir, "ref_db")
os.mkdir(database_dir)

db_base_family = os.path.join(database_dir, "db_base_family")


query_dico = os.path.join(working_dir, "query.dico")
query_simpl = os.path.join(working_dir, "query_SimpleID.faa")

aln_out_dir = os.path.join(working_dir, "aln_out_dir")
os.mkdir(aln_out_dir)

edge_dir = os.path.join(working_dir, "edges")
os.mkdir(edge_dir)

id_get_dir = os.path.join(working_dir, "id_get_ban_manage")
os.mkdir(id_get_dir)

ban_file = os.path.join(working_dir, "banned.txt")
newly_found_id = os.path.join(working_dir, "just_found.txt")
found_last_ron = os.path.join(working_dir, "found_previously.txt")

fasta_dir = os.path.join(working_dir, "fasta_dir")
os.mkdir(fasta_dir)
round_found = os.path.join(working_dir, "round_found.txt")
louvain_file = os.path.join(working_dir, "LouvainCommunity.txt")


current_fasta = os.path.join(fasta_dir, "current.faa")

Path_tuple = namedtuple("Path_tuple", ["db_dico", "db_faa_simpl", "db_faa_src", "db_fmt", "q_dico", "q_faa_simpl",
                                      "aln_out_dir", "edges_dir", "current_faa", "ban", "id_found", "id_previous", "fasta_dir",
                                       "round_found"])



# path_tpl = Path_tuple(db_dico=db_dico, db_faa_simpl=db_simpl_fasta, db_faa_src=args.db_fa, db_fmt=db_fmt,
#                       q_dico=query_dico, q_faa_simpl=query_simpl, aln_out_dir=aln_out_dir, edges_dir=edge_dir,
#                       current_faa=current_fasta,ban=ban_file, id_found=newly_found_id, id_previous=found_last_ron,
#                       fasta_dir=fasta_dir, round_found=round_found
#                       )

path_tpl = Path_tuple(db_dico=db_dico, db_faa_simpl=db_simpl_fasta, db_faa_src=db_base_family, db_fmt=db_fmt,
                      q_dico=query_dico, q_faa_simpl=query_simpl, aln_out_dir=aln_out_dir, edges_dir=edge_dir,
                      current_faa=current_fasta,ban=ban_file, id_found=newly_found_id, id_previous=found_last_ron,
                      fasta_dir=fasta_dir, round_found=round_found
                      )

Threshold_tuple = namedtuple("Threshod_tpl", ["eval", "pid", "mutual_cov", "all_chain"])
thres_tpl = Threshold_tuple(eval=float(args.eval_thr), pid=float(args.pident_thr), mutual_cov=float(args.cov_thr),
                             all_chain=args.test_all_chain)


print("Starting ISF")

db_size, min_size, max_size = utilitaire.manage_input_fasta(interest_family=start_family,
                                                            metagenome=database_fa,
						dico_family_interest=path_tpl.q_dico,
						dico_metagenome=path_tpl.db_dico, fasta_small_metage=path_tpl.db_faa_simpl,
						interest_family_simpl=path_tpl.q_faa_simpl, min_factor=args.min_size, max_factor=args.max_size)

# Construct aligner to add one add a module that herite from the aligner metaclass
# and add it here

log_cmd = os.path.join(working_dir, "launch_option.log.txt")
with open(log_cmd, "w") as ou:
	ou.write("{}\t{}\n".format("working directory", os.path.abspath(working_dir)))
	ou.write("{}\t{}\n".format("query", os.path.abspath(args.query)))
	ou.write("{}\t{}\n".format("target db", os.path.abspath(args.db_fa)))
	ou.write("{}\t{}\n".format("max number of run", args.run))
	ou.write("{}\t{}\n".format("test_all_chain", args.test_all_chain))
	ou.write("{}\t{}\n".format("pident threshold", args.pident_thr))
	ou.write("{}\t{}\n".format("coverage threshold", args.cov_thr))
	ou.write("{}\t{}\n".format("evalue threshold",args.eval_thr))
	ou.write("{}\t{}\n".format("size threshold", args.min_size))
	ou.write("{}\t{}\n".format("size threshold", args.max_size))
	if args.diamond:
		ou.write("{}\t{}\n".format("alignment", args.diamond))
	else:
		ou.write("{}\t{}\n".format("alignment", args.blast_))
	# ou.write("{}\t{}\n".format("", args.))



print("")
print("\t-------------------")
print("")
if args.diamond:
	aligner = diamond_aligner.Diamond("{} makedb".format(args.diamond), diamond_bin=args.diamond, threads=args.th)

elif args.mmseqs:
	temp_dir = os.path.join(working_dir, 'temp_dir_mmseq')
	os.makedirs(temp_dir)
	aligner = mmseq_aligner.MMseq2(binary=args.diamond, threads=args.th, temp_dir=temp_dir, sensitivity=8.5)

elif args.blast_:
	aligner = blast_aligner.Blast(make_db_bin=args.mkdb_,
	                             blastp_bin=args.blast_,
	                             fastasplit=args.faa_split,
	                             threads=args.th, working_dir=working_dir)


# get all ref sequences name,
# that may need refactor
q_dico = {}
with open(query_dico) as f_:
	for line in f_:
		spt = line.strip().split()
		q_dico[spt[0]] = spt[1]

utilitaire.update_round_found(round_file=path_tpl.round_found,sequences_found=list(q_dico.values()),

                              round_find=0, dict=query_dico)


try:
	aligner, seq_find = main_loop.main_isf(path_tpl, thres_tpl, args, aligner)
except utilitaire.NoSequenceFound:
	pass
except:
	raise

if not args.edge_file:
	utilitaire.remove(edge_dir)

print('Iteration done')

if len(seq_find) > 0:
	if not args.remove_aln:
		large_dico = os.path.join(working_dir, "large_dico")
		utilitaire.concat_file(large_dico, path_tpl.q_dico, path_tpl.db_dico)
		liste_file_to_rename = [os.path.join(path_tpl.aln_out_dir, elem) for elem in os.listdir(path_tpl.aln_out_dir)]
		for file in liste_file_to_rename:
			utilitaire.rename_blast(file, large_dico)
		utilitaire.remove(large_dico)
	else:
		utilitaire.remove(path_tpl.aln_out_dir)
	fasta_find = os.path.join(working_dir, 'sequence_found.faa')
	finded_and_base_family = os.path.join(working_dir, 'sequence_found_and_bases.faa')
	#
	query = os.path.join(working_dir, '{}.query.faa'.format(os.path.basename(args.query)))

	# grep all sequences found
	utilitaire.grep_fasta(sequence_file=path_tpl.db_faa_simpl, id_=seq_find, out_file=fasta_find)
	utilitaire.rename_fasta(path_tpl.q_dico, path_tpl.q_faa_simpl)
	utilitaire.rename_fasta(path_tpl.db_dico, fasta_find)
	utils.new_edges.rename_edges(dico_db=path_tpl.db_dico, dico_query=path_tpl.q_dico, edges_dir=path_tpl.edges_dir)
	utilitaire.concat_file(finded_and_base_family, fasta_find, path_tpl.q_faa_simpl)


	shutil.move(path_tpl.q_faa_simpl, query)

	if args.nr_db:
		aln_nr = os.path.join(working_dir, 'all_against_nr.blastp')
		print(aln_nr, fasta_find)
		aligner.align(db=args.nr_db, output=aln_nr, fasta=fasta_find,
	                    max_seq_target=10000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)
		aligner.clean(aln_nr, thres_tpl=thres_tpl, ban_list=set())

	if not args.NoAvA:
		print("making all against all alignment (seeds family + sequences found)")
		print("you can prevent ISF to make it by using the -NoAvA option")

		all_against_all = os.path.join(working_dir, 'all_against_all.blastp')
		all_against_alldb = os.path.join(database_dir, 'all_against_all.db')
		all_against_all_dico = os.path.join(working_dir, 'all_against_all.dico')
		finded_and_base_family_simpl = os.path.join(working_dir, 'sequence_found_and_bases._num.faa')

		utilitaire.make_dictionary_and_get_size(input_=finded_and_base_family, output=finded_and_base_family_simpl, o_dico=all_against_all_dico)
		aligner.make_db(input_=finded_and_base_family_simpl, output_=all_against_alldb)
		aligner.align(db=all_against_alldb, output=all_against_all, fasta=finded_and_base_family_simpl,
	                    max_seq_target=10000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)
		aligner.clean(all_against_all, thres_tpl=thres_tpl, ban_list=set())



		utilitaire.rename_blast(file_=all_against_all, dico_file=all_against_all_dico)


		utilitaire.remove(all_against_all_dico, finded_and_base_family_simpl, all_against_alldb )

		print("Alignment all against all done")
		print("alignment output is {}".format(os.path.abspath(all_against_all)))

		if not args.Nocom:

			print("computing networks community")
			louvainCommunity.main(all_against_all, louvain_file)

	print("\n\n")
	print("\t-------------------")
	print("ISF found: {} sequences".format(len(seq_find)))
	print("\t-------------------")
	print("Fasta of sequences found is {}".format(os.path.abspath(fasta_find)))
	print("\t-------------------")
	print("Merged fasta of initial gene family and sequence found is {}".format(os.path.abspath(finded_and_base_family)))
	print("\t-------------------")
	print("Fasta of initial seeds is {}".format(os.path.abspath(query)))


utilitaire.remove(path_tpl.db_dico, path_tpl.q_dico, path_tpl.fasta_dir,
                  id_get_dir, database_dir)#path_tpl.aln_out_dir,


