import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import os
import sys
import subprocess
from gi.repository import Gdk

from gi.repository import Pango

# class launcher:
#
# 	def __init__(self, **kwargs):
#


def where_is(looked_for):
	result = subprocess.check_output(["whereis", looked_for]).decode("utf-8").strip()
	path = result.split(':')
	if len(path) == 2:

		return path[1].split()[0]
	else:
		return looked_for

default_blast = where_is("blastp")
default_fastasplit = where_is("fastasplit")
default_makeblastdb = where_is("makeblastdb")
default_diamond = where_is("diamond")


class BlastBox(Gtk.Frame):

	def __init__(self, path_blast="blast", fasta_split="fastasplit", mkdb ="makeblast_db" ):

		super(Gtk.Frame, self).__init__()
		self.grid = Gtk.Grid()
		self.add(self.grid)

		#  blast binary
		self.choose = Gtk.Button(label="Blast binary")
		self.choose.connect("clicked", self.choose_file_clicked)
		self.path = path_blast
		self.label_path_blast = Gtk.Entry()
		self.label_path_blast.set_size_request(500,20)
		self.label_path_blast.set_text(self.path)
		self.grid.attach(self.choose, 1,2,1,1)
		self.grid.attach(self.label_path_blast, 2,2,1,1)

		# fasta_split
		self.choose_faa = Gtk.Button(label="fastasplit binary")
		self.choose_faa.connect("clicked", self.choose_file_clicked_faa)
		self.faa_splt = fasta_split
		self.label_path_faa = Gtk.Entry()
		self.label_path_faa.set_size_request(500,20)
		self.label_path_faa.set_text(self.faa_splt)
		self.grid.attach(self.choose_faa, 1,3,1,1)
		self.grid.attach(self.label_path_faa, 2,3,1,1)

		# makeblastdb
		self.choose_mk = Gtk.Button(label="makeblastdb binary")
		self.choose_mk.connect("clicked", self.choose_file_clicked_mkdb)
		self.makedb = mkdb
		self.label_path_mkdb = Gtk.Entry()
		self.label_path_mkdb.set_size_request(500,20)
		self.label_path_mkdb.set_text(self.makedb)
		self.grid.attach(self.choose_mk, 1,4,1,1)
		self.grid.attach(self.label_path_mkdb, 2,4,1,1)

		self.show_all()

	def choose_file_clicked(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
			Gtk.FileChooserAction.OPEN,
			 (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			 Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

		#self.add_filters(dialog)

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			global default_blast
			default_blast = dialog.get_filename()
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_blast.set_text(dialog.get_filename())
		dialog.destroy()

	def choose_file_clicked_mkdb(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
		                               Gtk.FileChooserAction.OPEN,
		                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

		# self.add_filters(dialog)

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			global default_makeblastdb
			default_makeblastdb = dialog.get_filename()
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_mkdb.set_text(dialog.get_filename())
		dialog.destroy()

	def choose_file_clicked_faa(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
		                               Gtk.FileChooserAction.OPEN,
		                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

		# self.add_filters(dialog)

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			global default_fastasplit
			default_fastasplit = dialog.get_filename()
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_faa.set_text(dialog.get_filename())
		dialog.destroy()


class DiamondBox(Gtk.Frame):

	def __init__(self, dmd):

		super(Gtk.Frame, self).__init__()
		self.grid = Gtk.Grid()
		self.add(self.grid)

		# diamond
		self.choose_dmd = Gtk.Button(label="diamond binary")
		self.choose_dmd.connect("clicked", self.choose_file_clicked)
		self.dmd = dmd
		self.label_path_dmd = Gtk.Entry()
		self.label_path_dmd.set_size_request(500, 20)
		self.label_path_dmd.set_text(self.dmd)
		self.grid.attach(self.choose_dmd, 1, 1, 1, 1)
		self.grid.attach(self.label_path_dmd, 2, 1, 1, 1)
		self.show_all()

	def choose_file_clicked(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
		                               Gtk.FileChooserAction.OPEN,
		                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

		# self.add_filters(dialog)

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			global default_diamond
			default_diamond = dialog.get_filename()
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_dmd.set_text(dialog.get_filename())
		dialog.destroy()

# TODO padding
class BoxMinimal(Gtk.Frame):

	def __init__(self):

		super(Gtk.Frame, self).__init__()

		self.grid = Gtk.Grid()
		self.add(self.grid)

		# query
		self.choose_query = Gtk.Button(label="query file")
		self.choose_query.connect("clicked", self.choose_file_clicked_query)
		self.query = "path_to_query"
		self.label_path_query = Gtk.Entry()
		self.label_path_query.set_size_request(500, 20)
		self.label_path_query.set_text(self.query)
		self.grid.attach(self.choose_query, 1,1,1,1)
		self.grid.attach(self.label_path_query, 2,1,1,1)

		# wd
		self.choose_wd = Gtk.Button(label="working directory")
		self.choose_wd.connect("clicked", self.choose_file_clicked_wd)
		self.wd = "working_directory_path"
		self.label_path_wd = Gtk.Entry()
		self.label_path_wd.set_size_request(500, 20)
		self.label_path_wd.set_text(self.wd)
		self.grid.attach(self.choose_wd, 1,2,1,1)
		self.grid.attach(self.label_path_wd, 2,2,1,1)


		# databases
		self.choose_db = Gtk.Button(label="environmental sequences")
		self.choose_db.connect("clicked", self.choose_file_clicked_env)
		self.env = "path_to_fasta_of_environmental_sequences"
		self.label_path_db = Gtk.Entry()
		self.label_path_db.set_size_request(500, 20)
		self.label_path_db.set_text(self.env)
		self.grid.attach(self.choose_db, 1,3,1,1)
		self.grid.attach(self.label_path_db, 2,3,1,1)
		self.show_all()

	def return_value(self):
		dico = {"query": str(self.label_path_query.get_text()),
		        "db_fa": str(self.label_path_db.get_text()),
		        "wd": str(self.label_path_wd.get_text())}
		return  dico

	def choose_file_clicked_query(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
		                               Gtk.FileChooserAction.OPEN,
		                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))


		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			self.query = dialog.get_filename()
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_query.set_text(dialog.get_filename())
		dialog.destroy()


	def choose_file_clicked_wd(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
		                               Gtk.FileChooserAction.OPEN,
		                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))


		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			self.wd = dialog.get_filename()

		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_wd.set_text(dialog.get_filename())
		dialog.destroy()



	def choose_file_clicked_env(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", None,
		                               Gtk.FileChooserAction.OPEN,
		                               (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))


		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Open clicked")
			print("File selected: " + dialog.get_filename())
			self.env = dialog.get_filename()
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		self.label_path_db.set_text(dialog.get_filename())
		dialog.destroy()


class Stringency(Gtk.Frame):

	def __init__(self):

		super(Gtk.Frame, self).__init__()

		self.grid = Gtk.Grid()
		self.add(self.grid)

		self.entry_id = Gtk.Entry()
		self.entry_id.set_size_request(100, 20)
		self.entry_id.set_text("30.0")
		self.label = Gtk.Label("percentage of identity")
		self.grid.attach(self.label, 1, 1, 1, 1)
		self.grid.attach(self.entry_id, 1, 2, 1, 1)

		self.entry_cov = Gtk.Entry()
		self.entry_cov.set_size_request(100, 20)
		self.entry_cov.set_text("80.0")
		self.label = Gtk.Label("minimal coverage")
		self.grid.attach(self.label, 2, 1, 1, 1)
		self.grid.attach(self.entry_cov, 2, 2, 1, 1)

		self.entry_min = Gtk.Entry()
		self.entry_min.set_size_request(100, 20)
		self.entry_min.set_text("80.0")
		self.label = Gtk.Label("min size ratio")
		self.grid.attach(self.label, 3, 1, 1, 1)
		self.grid.attach(self.entry_min, 3, 2, 1, 1)

		self.entry_max = Gtk.Entry()
		self.entry_max.set_size_request(100, 20)
		self.entry_max.set_text("120.0")
		self.label = Gtk.Label("maximal size ration")
		self.grid.attach(self.label, 4, 1, 1, 1)
		self.grid.attach(self.entry_max, 4, 2, 1, 1)

		# combo

		self.name_store = Gtk.ListStore(str)
		self.name_store.append(["no"])
		self.name_store.append(["all"])
		self.name_store.append(["previous"])

		self.name_combo = Gtk.ComboBox.new_with_model_and_entry(self.name_store)
		self.name_combo.connect('changed', self.on_changed)

		self.name_combo.set_entry_text_column(0)
		self.name_combo.set_active(1)

		self.label = Gtk.Label("select a procedure")
		self.grid.attach(self.label, 5, 1, 1, 1)
		self.grid.attach(self.name_combo, 5, 2, 1, 1)


		self.entry_th = Gtk.Entry()
		self.entry_th.set_size_request(100, 20)
		self.entry_th.set_text("1")
		self.label = Gtk.Label("Number of CPU")
		self.grid.attach(self.label, 1, 3, 1, 1)
		self.grid.attach(self.entry_th, 1, 4, 1, 1)


		self.entry_run = Gtk.Entry()
		self.entry_run.set_size_request(100, 20)
		self.entry_run.set_text("1000")
		self.label = Gtk.Label("maximum number of iteration")
		self.grid.attach(self.label, 2, 3, 1, 1)
		self.grid.attach(self.entry_run, 2, 4, 1, 1)

		self.show_all()


	def on_changed(self, widget):
		self.label.set_label(widget.get_active_text())
		return


def check_exist(widget):
	if os.path.exists(str(widget.get_text())):
		widget.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
		return str(widget.get_text())
	else:
		widget.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
		return None


def check_not_exist(widget):
	if not os.path.exists(str(widget.get_text())):
		widget.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
		return str(widget.get_text())
	else:
		widget.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
		return None

def check_number(widget):
	try:
		float(str(widget.get_text()))
	except:
		widget.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
		return None
	widget.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
	return str(widget.get_text())

class MyPageOne(Gtk.Grid):

	def __init__(self):
		super(Gtk.Grid, self).__init__()
		self.radio_button_box = Gtk.Box(spacing=6)

		self.blast_ = 'blast'
		self.add(self.radio_button_box)
		self.blast_ = Gtk.RadioButton.new_with_label_from_widget(None, "BLAST")
		self.blast_.connect("toggled", self.on_button_toggled, "BLAST")
		self.radio_button_box.pack_start(self.blast_, False, False, 0)

		self.diamond_ = Gtk.RadioButton.new_from_widget(self.blast_)
		self.diamond_.set_label("diamond")
		self.diamond_.connect("toggled", self.on_button_toggled, "diamond")
		self.radio_button_box.pack_start(self.diamond_,  False, False, 0)
		self.radio_button_box.show()

		self.blast = BlastBox(default_blast, default_fastasplit, default_makeblastdb)
		self.attach(self.blast, 0, 2, 2, 2)

		self.query = BoxMinimal()
		self.attach(self.query, 0, 4, 2, 2)

		self.stringency = Stringency()
		self.attach(self.stringency, 0, 6, 2, 2)

		self.launch_button = Gtk.Button.new_with_label("launch")
		self.launch_button.connect("clicked", self.launch_clicked)
		self.attach(self.launch_button, 4, 8, 2, 2)
		self.show_all()


	def launch_clicked(self, widget):

		do_nothing = False
		# first check all option!
		# minimal example
		dico = {}
		dico["my_query"] = check_exist(self.query.label_path_query)
		dico["db_fa"] = check_exist(self.query.label_path_db)
		dico["wd"] = check_not_exist(self.query.label_path_wd)

		if self.diamond_.get_active():
			dico["diamond"] = check_exist(self.diamond.label_path_dmd)

		elif self.blast_.get_active():

			dico["mkdb_"] = check_exist(self.blast.label_path_mkdb)
			dico["faa_split"] = check_exist(self.blast.label_path_faa)
			dico["blast_"] = check_exist(self.blast.label_path_blast)

		dico["pident_thr"] = check_number(self.stringency.entry_id)
		dico["cov_thr"] = check_number(self.stringency.entry_cov)
		dico["max_size"] = check_number(self.stringency.entry_max)
		dico["min_size"] = check_number(self.stringency.entry_min)
		dico["run"] = check_number(self.stringency.entry_run)
		dico["th"] = check_number(self.stringency.entry_th)

		path_script = os.path.abspath(os.path.dirname(sys.argv[0]))

		subprocess.Popen("")

		print(dico)


	def on_button_toggled(self, button, name):
		if not button.get_active():

			state = "off"
			if name == "diamond":
				self.remove(self.diamond)
			elif name == "BLAST":
				self.remove(self.blast)
			print("Button", name, "was turned", state)
		else:
			state = "on"

			if name == "diamond":
				self.diamond = DiamondBox(default_diamond)
				self.attach(self.diamond, 0, 2, 2, 2)

			elif name == "BLAST":
				self.blast = BlastBox(default_blast, default_fastasplit, default_makeblastdb)
				self.attach(self.blast, 0, 2, 2, 2)
			self.show_all()
			print("Button", name, "was turned", state)


	def on_button_clicked(self, widget):
		print("Hello World")



class MyWindow(Gtk.Window):

	def __init__(self):


		#
		Gtk.Window.__init__(self, title="Simple Notebook Example")
		self.set_border_width(3)
		self.set_default_size(1000, 500)
		self.notebook = Gtk.Notebook()
		self.add(self.notebook)

		self.page1 = MyPageOne()
		self.page1.set_border_width(10)
		self.notebook.append_page(self.page1, Gtk.Label('Main'))

		self.page2 = Gtk.Box()
		self.page2.set_border_width(10)
		self.page2.add(Gtk.Label('A page with an image for a Title.'))
		self.notebook.append_page(
			self.page2,
			Gtk.Image.new_from_icon_name(
				"help-about",
				Gtk.IconSize.MENU
			)
		)

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()