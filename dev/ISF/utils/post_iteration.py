
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import subprocess
import os
from multiprocessing import pool
from utils import utilitaire
from collections import namedtuple
import argparse

#-------- START CLASS DEFINITION -----------------

class Node:

	def __init__(self, id_):

		self.id_ = id_

		self.relative_seq_start = None
		self.round = 0
		self.dn_ds = None
		self.environmental = None  # Bolean
		self.abundance_mean = None
		self.abundance_site_upper_thr = None

		self.nearest_ncbi_percent_id = 0
		#  # # if found a tuple: prot_id, tax_id, top_name, basename, '/'.join(list(reversed(liste_all)))
		self.phylogeny = tuple(["unknown"])
		self.nearest_ncbi_domain = "undef"
		self.nearest_ncbi_prot = "undef"

	def dump_node(self):

		liste_ = [self.id_]
		if self.environmental:
			liste_.append("env")
			liste_.append(self.relative_seq_start)
		else:
			liste_.append("ref")
			liste_.append("none")


		liste_.append(self.round)
		liste_.append(self.dn_ds)
		liste_.append(self.nearest_ncbi_prot)
		liste_.append(self.nearest_ncbi_percent_id)
		liste_.append(";".join(self.phylogeny))

		return list(map(str, liste_))


#-------- END CLASS DEFINITION -----------------



def main_postIteration(working_dir, db_fna, query_fna, ):
	pass

if __name__ == "__main__":
	pass
