import os
import re
from multiprocessing import Pool
import shutil
from itertools import islice
from utils import utilitaire

def blast_todict(blast_output):
	dico_blast = dict()

	with open(blast_output) as blast_file:

		for line in blast_file:
			spt = line.strip().split('\t')
			to_keep = spt
			tar_id = spt[1]
			query_id = spt[0]

			# first time we see this id
			if query_id not in dico_blast:
				# if this query have match something
				dico_blast[query_id] = {tar_id: to_keep}

			else:
				# we have already this tar id but not the query id
				if tar_id not in dico_blast[query_id]:

					dico_blast[query_id][tar_id] = to_keep

				else:
					old_line = dico_blast[query_id][tar_id]

					if float(old_line.split('\t')[0]) > float(line.split('\t')[0]):
						dico_blast[query_id][tar_id] = to_keep

	return dico_blast

reg = re.compile("\((\d+),(\d+)\)")
def parse_edge_line(line, reg=reg):
	x = line.strip().split()
	query = x[0]
	target = x[-1]
	search = reg.search(x[1])
	start = search.group(1)
	end = search.group(2)
	return (query, target, start, end)


def overlap(min1, min2, max1, max2):

	return max(0, min(int(max2), int(max1)) - max(int(min1), int(min2)) + 1)


def add_edges_no(run, edges_file, edges_file_tmp, dico_blast, threshold, id_get):
	with open(edges_file, 'a') as f:
		for k, v in dico_blast:
			for k1, v1 in v:
				f.write("{} ; {}\t({},{})\t{}\n".format(run, k, v1[7], v1[8], k1))


def construct_dictionnary(dict_files):
	dico = {}
	with open(dict_files) as dict_fil:
		for line in dict_fil:
			spt = line.strip().split('\t')
			dico[spt[1]] = spt[0]

	return dico


def edge_str(query, target, start, end):
	return "{}\t({},{})\t{}".format(query, start, end, target)


def rewrite(edges, dico):
	with open(edges, 'r') as int_f, open("{}_.tmp".format(edges), 'w') as tmp:
		for line in int_f:
			if not line.strip():
				continue
			query, target, start, end = parse_edge_line(line)
			query = dico[query]
			target = dico[target]
			tmp.write("{}\n".format(edge_str(query, target, start, end)))

	shutil.move("{}_.tmp".format(edges), edges)


def rename_edges(dico_db, dico_query, edges_dir):
	dico = {}
	dico.update(construct_dictionnary(dico_db))
	dico.update(construct_dictionnary(dico_query))
	for root, dirs, files in os.walk(os.path.abspath(edges_dir)):
		for file in files:
			edges = os.path.join(root, file)
			rewrite(edges, dico)

	# all_edges = os.path.join(edges_dir, '_edges_')
	#
	# rewrite(all_edges, dico)


def add_eges_previous_v2(args):
	run, edges_file, edges_file_tmp, dico_blast, threshold = args
	id_get = set()

	set_test = set()

	with open(edges_file) as in_, open(edges_file_tmp, 'w') as out_:

		for line in in_:
			if not line.strip():
				continue
			out_.write(line)
			query, target, start, end = parse_edge_line(line)
			query, target = str(query), str(target)
			#if not target in dico_blast:
			#	print(target)

			#if target in dico_blast:  # and target not in set_test:

			for key, v in dico_blast.items():

				if ((key, target)) not in set_test:
					#print(query, target, start, end)
					#print(key, v)
					set_test.add((key, target))
					over = overlap(min1=int(start), min2=int(v[5]), max1=int(end),
								   max2=int(v[6]))

					if (over/int(v[10])) < threshold:
						#print("value over computed : {}, overlength: {} not passing: {} threshold".format(
						#  over,
						# over/int(v[10]),
						#   threshold))
						continue

					else:
						#print("pass")
						id_get.add(key)
						out_.write("{}\t({},{})\t{}\n".format(target, v[7], v[8], key))

	return id_get


def add_edges_all_v2(args):
	run, edges_file, edges_file_tmp, dico_blast, threshold = args

	id_get = set()
	#set_test = set()

	with open(edges_file) as in_, open(edges_file_tmp, 'w') as out_:
		#lines = in_.readlines()

		for line in in_:
			#for line in lines:
			#print(line)
			if not line.strip():
				continue
			out_.write(line)
			query, target, start, end = parse_edge_line(line)
			#print(line, target)
			#if target in dico_blast:# and target not in set_test:

			for key, v in dico_blast.items():
				#print(key, v)
				#set_test.add((key, target))

				over = overlap(min1=int(start), min2=int(v[5]), max1=int(end),
								   max2=int(v[6]))

				if (over/int(v[10])) < threshold:
					continue
				else:
					id_get.add(key)

					out_.write("{}\t({},{})\t{}\n".format(target, max([start, v[7]]), min([end, v[8]]), key))

	return id_get





def file_len(fname):
	try:
		with open(fname) as f:
			for i, l in enumerate(f):
				pass
		return i + 1
	except:
		return 0

def splitting_file(file_, out_dir):

	def sub_fiction(lines, out_dir):

		previous_id = ""
		current_line = []
		try:
			for line in sorted(lines, key=lambda x: x[-1]):
				spt = line
				id_ = spt[-1].strip()
				if id_ != previous_id:
					if current_line:
						file_name = os.path.join(out_dir, "{}.fasta".format(previous_id))

						with open(file_name, 'a') as out:
							out.write("\n".join(current_line) + "\n")
					current_line = []
					previous_id = id_

				current_line.append("\t".join(line))
		except IndexError:
			print(lines)
			raise

		file_name = os.path.join(out_dir, "{}.fasta".format(previous_id))
		with open(file_name, 'a') as out:
			out.write("\n".join(current_line) + "\n")

	#length = file_len(file_)

	with open(file_) as f:

		while True:
			next_n_lines = list(islice(f, 1000000))
			lines = [elem.split('\t') for elem in next_n_lines]
			sub_fiction(lines, out_dir)
			if not next_n_lines:
				break

	#
	# lines = []
	# with open(file_) as f:
	# 	#lines = f.readlines()
	# 	for line in f:
	# 		if line.strip():
	# 			lines.append(line.strip().split())




def manages_edges_file(edges_directory: str, blast_output: str, threads: int, run, all_chain,
					   cover_thr, keep_older):
	"""This function will manage the edge file"""

	id_get = set()

	# we looks the edge dir and find the corresponding direcory edges_run-1.fasta
	previous_run = run - 1
	previous_dir = os.path.join(edges_directory, "edges_{}.fasta".format(previous_run))



	# create the new directory
	run_dir = os.path.join(edges_directory,
						   "edges_{}.fasta".format(run))
	#print("debug: previous_dir: {}, current dir {}".format(previous_dir, run_dir))
	#print("debug blast output is : {}".format(blast_output))
	os.mkdir(run_dir)
	if os.path.isdir(previous_dir):
		dico_blast = blast_todict(blast_output=blast_output)
		#print(sorted(list(dico_blast.keys()), key=lambda x: int(x)))

		liste_args = []
		out_name = run_dir
		out_cpt = 0
		for file_name in os.listdir(previous_dir):
			id_ = file_name.split(".")[0]
			if id_ not in dico_blast:
				continue

			liste_args.append((run,
			os.path.join(os.path.abspath(previous_dir),
						 file_name),
			os.path.join(os.path.abspath(run_dir),
						 '{}.edges'.format(str(out_cpt))),
			dico_blast[id_],
			cover_thr))
			out_cpt += 1

		print("launching multiprocessing: {} Job".format(len(liste_args)))

		with Pool(processes=threads, maxtasksperchild=1) as pool:
			if all_chain == "previous":
				#print("previous")
				out_code = pool.map(add_eges_previous_v2, liste_args)

			elif all_chain == "all":
				#print("all")
				out_code = pool.map(add_edges_all_v2, liste_args)

			else:
				out_code = set([tuple(elem.keys()) for elem in list(dico_blast.values())])

		for elem in out_code:
			id_get.update(elem)

	else:

		with open(blast_output) as blast_file, open(os.path.join(run_dir, '0.edges'), 'w') as ed_file:

			for line in blast_file:

				spt = line.strip().split('\t')
				# write to the file query blast info target
				ed_file.write(spt[0] + "\t")
				ed_file.write("({},{})".format(str(max([spt[5], spt[7]])), str(min([spt[6], spt[8]]))))
				ed_file.write('\t' + spt[1] + '\n')

				id_get.add(spt[1])


	inter_bigger_file = os.path.join(run_dir, "all_edges")
	list_inter_file = [os.path.join(os.path.abspath(run_dir), elem) for elem in os.listdir(run_dir)]
	utilitaire.concat_file(inter_bigger_file, *list_inter_file)
	utilitaire.remove(*list_inter_file)
	splitting_file(inter_bigger_file, run_dir)
	utilitaire.remove(inter_bigger_file)

	if not keep_older:
		utilitaire.remove(previous_dir)

	return id_get
