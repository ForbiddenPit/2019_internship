#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#
file=$1
key=$2

sum="$(awk '{print $1}' $key)"
file_sum=$(md5sum $file| awk '{print $1}')

if [[ $sum == $file_sum ]]
    then
        echo true
    else
        echo false
fi