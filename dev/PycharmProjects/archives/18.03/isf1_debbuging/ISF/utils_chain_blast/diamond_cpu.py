
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import subprocess
import os
from utils_chain_blast.aligner import Aligner


class Diamond(Aligner):

	def __init__(self, make_db_bin, diamond_bin, threads):


		self.threads = threads
		self.dmd_bin = diamond_bin
		self.make_db_bin = make_db_bin

	def make_db(self, input_, output_, **kwargs):
		cmd = "{} --in {} --db {}".format(self.make_db_bin, input_, output_)
		print("making a diamond databse with this current line {}".format(cmd))
		child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL)
		child.wait()

	def align(self, db, output, fasta
	          , max_seq_target,
	          cover, eval_, id, **kwargs):
		cmd = "{} blastp --db {} --query {} --out {} --id {} --outfmt 6 qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen --threads {} \
--max-hsps 1 --no-self-hits --query-cover {} --subject-cover {} --evalue {} --max-target-seqs {} \
--more-sensitive".format(self.dmd_bin, db, fasta, output, id, self.threads, cover, cover, eval_, max_seq_target)
		print("launchind diamond with the current command:\n{}\n".format(cmd))
		child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL)
		child.wait()

	def filter_by_ban_list(self, aln_file, ban_set, temp_file):

		if len(ban_set) == 0:
			return

		with open(aln_file, 'r') as in_, open(temp_file, 'w') as out_:
			for line in in_:
				spt = line.strip().split()
				if not spt[1] in ban_set:
					out_.write(line)

		os.rename(temp_file, aln_file)


	def clean(self, **kwargs):
		# for diamond is already done do pass!
		pass

# def make_diamond_db(diamond, input_, db_name):
#
# 	cmd = "{} makedb --in {} --db {}".format(diamond, input_, db_name)
# 	print("making a diamond databse with this current line {}".format(cmd))
# 	child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL)
# 	child.wait()
#
#
# def diamond_(thread, db, output, fasta, diamond, max_seq_target=10000,
# 			 cover=80, eval_=0.00001, id=30, matrix_simil="BLOSUM62"):
#
# 	cmd = "{} blastp --db {} --query {} --out {} --id {} --outfmt 6 qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen --threads {} \
# 	--max-hsps 1 --no-self-hits --query-cover {} --subject-cover {} --evalue {} --max-target-seqs {} --matrix {} \
# 	--more-sensitive".format(diamond, db, fasta, output, id, thread, cover, cover, eval_, max_seq_target, matrix_simil)
# 	print("launchind diamond with the current command:\n{}\n".format(cmd))
# 	child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL)
# 	child.wait()
#
#
# def filter_banlist_diamond(dmd_file, ban_set, temp_file):
#
# 	if len(ban_set) == 0:
# 		return
#
# 	with open(dmd_file, 'r') as in_, open(temp_file, 'w') as out_:
# 		for line in in_:
# 			spt = line.strip().split()
# 			if not spt[1] in ban_set:
# 				out_.write(line)
#
# 	child = subprocess.Popen("mv {} {}".format(temp_file, dmd_file), shell=True)
# 	child.wait()


