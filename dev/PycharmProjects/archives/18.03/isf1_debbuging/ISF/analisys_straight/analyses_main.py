
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import subprocess
import os
from utils import classes, fonction, clustering_multi_louvain, graph_analyses, annot_nr_v3
import argparse
from multiprocessing import Pool
import igraph
from collections import namedtuple
from functools import reduce


file_path = namedtuple("file_path",["wd", "log_file", "node_file", "cluster_node", "cluster_file", "cluster_resume",
                                    "temp_dn_dsbase", "seq_find", "seq_find_bases", "allvsall", "edges_", "blast_nr",
                                    "seq_fna_file", "fna_seq"])

opti = namedtuple("option", ["blast_nr", "abundance", "dnds"])

def make_path_tuple(wd_, output_file_name=None):

	wd_ = wd_.rstrip('/') + '/'
	if not output_file_name:
		log_file = os.path.join(wd_, "log.analyses")
		node_file = os.path.join(wd_, "sequences_infos.tsv")
		cluster_file = os.path.join(wd_, "cluster_seq.tsv")
		cluster_resume = os.path.join(wd_, "cluster_infos.tsv")
	else:
		log_file = os.path.join(output_file_name, "log.analyses")
		node_file = os.path.join(output_file_name, "sequences_infos.tsv")
		cluster_file = os.path.join(output_file_name, "cluster_seq.tsv")
		cluster_resume = os.path.join(output_file_name, "cluster_infos.tsv")

	temp_dn_dsbase = os.path.join(wd_, "temp_dn_ds")

	seq_find = os.path.join(wd_, "sequence_found.faa")
	seq_find_bases = os.path.join(wd_, "sequence_found_and_bases.faa")
	allvsall = os.path.join(wd_, "all_against_all.blastp")
	edges_ = os.path.join(wd_, "edges/_edges_")
	blast_nr = os.path.join(wd_, "all_against_nr.blastp")
	seq_fna_file = os.path.join(wd_, "seq.fna")
	fna_seq = os.path.join(wd_, "db.fna")
	print(blast_nr)
	test = file_path(wd=wd_, log_file=log_file, node_file=node_file, cluster_node=cluster_file,
	                 cluster_resume=cluster_resume, temp_dn_dsbase=temp_dn_dsbase, seq_find=seq_find,
	                 allvsall=allvsall, edges_=edges_, blast_nr=blast_nr, cluster_file=cluster_file,
	                 seq_find_bases=seq_find_bases, seq_fna_file=seq_fna_file, fna_seq=fna_seq )
	print(test)
	return test


def make_node_instances(path_tuple):

	dico_nodes = {}

	print("getting nodes id from files: {} \n{}".format(path_tuple.seq_find_bases, path_tuple.seq_find))
	# Getting all Nodes
	all_sequence_id = subprocess.check_output("grep '>' {}".format(path_tuple.seq_find_bases), shell=True)\
											.decode('utf-8')\
											.split('\n')

	# getting all environmental nodes
	environmental = subprocess.check_output("grep '>' {}".format(path_tuple.seq_find), shell=True)\
											.decode('utf-8')\
											.split('\n')


	# make the difference of the two sets to have only the base family
	sequence_from_start = (set(environmental) ^ set(all_sequence_id))
	sequence_from_start = set([elem[1:] for elem in sequence_from_start])
	all_sequence_id = set([elem[1:] for elem in all_sequence_id if elem])

	print("{} references sequences\n{} environmental sequences\n".format(len(sequence_from_start), len(environmental)))
	print("making Node instance and the dictionnary")
	# putting node in dict
	for sequence_id in all_sequence_id:
		nodes_instance = classes.Node(id_=sequence_id)

		if sequence_id in sequence_from_start:
			nodes_instance.environemental = False
			nodes_instance.nearest_ncbi_percent_id = 100

		else:
			nodes_instance.environmental = True

		dico_nodes[sequence_id] = nodes_instance

	return dico_nodes, sequence_from_start, environmental


def make_fna_file(path_tuple, sequence_from_start, environmental, fna_db, fna_seq):


	if os.path.exists(path_tuple.seq_fna_file) and os.path.exists(path_tuple.db_fna_file):
		dn_ds = True

	elif fna_db and fna_seq:
		print("getting nuc sequences of interest for dn_ds")

		if not os.path.exists(path_tuple.seq_fna_file):
			fonction.grep_fasta(sequence_file=fna_seq, id_file=set(sequence_from_start), out_file=path_tuple.seq_fna_file)

		if not os.path.exists(path_tuple.db_fna_file):
			fonction.grep_fasta(sequence_file=fna_db, id_file=set(environmental), out_file=path_tuple.db_fna_file)

		dn_ds = True

	else:
		dn_ds = False

	return dn_ds


def update_abundances(dico_node, ab_file, thr=1):
	print("updating abundance value from file: {}".format(ab_file))
	def cond(x, thr=thr):
		if x >= thr:
			return 1
		else:
			return 0

	with open(ab_file) as f:
		header = f.readline()

		for line in f:
			spt = line.strip().split()

			if spt[0] in dico_node:
				# print(line)
				# for key, value in dico_node.items():
				# 	print(key, value.__dict__)
				node_instance = dico_node[spt[0]]

				value_list = [float(elem) for elem in spt[1:]]
				node_instance.abundance_mean = sum(value_list)/len(value_list)
				node_instance.abundance_site_upper_thr = len([elem for elem in value_list if elem >= thr])
				#node_instance.abundance_site_upper_thr = reduce(lambda count, i: count + cond(i), value_list)
				#print(node_instance.abundance_site_upper_thr, node_instance.__dict__)


def main_analyses(wd_, output_file_name=None, fna_db=None, fna_seq=None,
                  taxo_dir=None, thr_ab=1, abundance_file=None , th=5, cover_threshold=0.8, pid_threshold=30,
                  ask_ncbi=False, indexed_dump=True, pid_threshold_annot=0):


	path_ = make_path_tuple(wd_, output_file_name)   # return named tuple convenient to pass a lot of path between fonctions.

	if os.path.isfile(path_.blast_nr):
		blast_nr_done = True

	else:
		blast_nr_done = False


	print("getting nodes id")

	dico_nodes, sequence_from_start, environmental = make_node_instances(path_tuple=path_)
	if abundance_file:
		abundance = True
		update_abundances(dico_nodes, ab_file=abundance_file, thr=thr_ab)
	else:
		abundance = False

	dn_ds = make_fna_file(path_, sequence_from_start, environmental, fna_db, fna_seq)

	del sequence_from_start, environmental
	# round annot
	# assigning a round finded
	print("assign round find and seed seq")
	with open(path_.edges_) as edge_f:
		for line in edge_f:
			spt = line.strip().split('\t') # added
			print(spt)
			print(line)
			start_seq = spt[0]
			print(start_seq)
			for id_tuple, info, round_ in fonction.line_generator(spt):
				id1, id2 = id_tuple
				print(dico_nodes, id1, id2)
				fonction.replace_round(id=id1, dico=dico_nodes, round=round_-1, start_seq=start_seq)
				fonction.replace_round(id=id2, dico=dico_nodes, round=round_, start_seq=start_seq)
				# pass modify add relative seq from start


#def annotated(blast_nr, dump_dir, dico_node,
#		                    cover_threshold, pid_threshold, indexed, ask_ncbi):
	# If NR
	# def annotated(blast_nr, dump_dir, dico_node,
	#      cover_threshold, pid_threshold, indexed, ask_ncbi):
	nr_do = True
	if blast_nr_done and args.dump:
		print("add nr annotation and taxonomic informations")
		annot_nr_v3.annotated(blast_nr=path_.blast_nr, dump_dir=taxo_dir, dico_node=dico_nodes,
		                    cover_threshold=cover_threshold, pid_threshold=pid_threshold_annot,
		                      indexed=indexed_dump, ask_ncbi=ask_ncbi)
		#dico, set_id_base = annot_nr_v2.main(dump_dir=args.dump_dir, set_identifier=set_id, ask_ncbi=ask_ncbi)
		#blast_annot_nr.main(blast_vs_nr=path_.blast_nr, dump_dir=taxo_dir, dico_node=dico_nodes,
		#                    cover_threshold=cover_threshold)
	elif blast_nr_done:
		print("add nr hit but taxonomic annotations unavailable")
		annot_nr_v3.annotated(blast_nr=path_.blast_nr, dump_dir=None, dico_node=dico_nodes,
		                    cover_threshold=cover_threshold, pid_threshold=pid_threshold_annot,
		                      indexed=indexed_dump, ask_ncbi=ask_ncbi)
		#blast_annot_nr.main(blast_vs_nr=path_.blast_nr, dico_node=dico_nodes,
		#                    cover_threshold=cover_threshold)

	else:
		nr_do = False

	dn_ds_tuple = tuple()
	# stick with paml because is on apt-get
	if dn_ds:
		print("computing pairwise dn ds to seed sequence")
		#  compute dn/ds to relative from start seq
		# in this case we are going to make pairwise found vs seeds.
		cpt = 0
		liste_args = []
		liste_keys = list(dico_nodes.keys())

		for node_id in liste_keys:
			node_instance = dico_nodes[node_id]
			if not node_instance.environmental:
				continue

			cpt += 1
			tem_dn_ds = os.path.join(path_.temp_dn_dsbase(str(cpt), "/"))
			seed_name = node_instance.relative_seq_start
			tuple_args = (set([seed_name]), set([node_id]), path_.db_fna_file, path_.seq_fna_file, path_.seq_find_bases,
			              tem_dn_ds, path_.log_file)

			liste_args.append(tuple_args)

		with Pool(th) as pool:
			x = pool.map(fonction.get_dn_ds, liste_args)

		cpt = 0
		for node_id in liste_keys:
			node_instance = dico_nodes[node_id]
			if not node_instance.environmental:
				continue

			dico_nodes[node_id].dn_ds = x[cpt]
			cpt += 1
		del x, liste_args
		dn_ds_tuple = (path_.seq_fna_file, path_.db_fna_file, path_.seq_find_bases, path_.temp_dn_dsbase, path_.log_file)

	print("node_info in : {}".format(path_.node_file))

	# print the node infos
	with open(path_.node_file, "w") as nodes:
		header = ["id, nature, seed_ref, round, dn_ds, best_nr, best_nr_pident, taxo_best_nr"]
		nodes.write("\t".join(header) + '\n')
		for key, value in sorted(dico_nodes.items(), key=lambda x: x[1].round):
			to_add = value.dump_node()
			nodes.write("\t".join(to_add) + '\n')


	print("doing graph analyses")
	# igraph analyses

	dico_cluster, dico_seq_id_to_cluster, big_tree, node_real_names, node_igraph, g =\
										clustering_multi_louvain.main(dico_seq_idto_node_instance=dico_nodes,
										blast_=path_.allvsall,
										out_file_basename=wd_,
										dn_ds_tuple=dn_ds_tuple,
										cover_th=cover_threshold,
										pident_th=pid_threshold,
										nr=nr_do, ab=abundance)

	graph_analyses.main(big_tree, dico_cluster, dn_ds_tuple, th, g, node_igraph, dn_ds)

	# now we will dump a file describing the clusters for each levels,
	# and another file with all cluster_to > seq

	out_cluster = os.path.join(wd_, "cluster_info.tsv")
	liste_clusters = sorted(dico_cluster.keys(), key=lambda x: int(x.split('_')[0]))
	# liste.append(self.id_)
	# liste.append("{}".format(','.join(self.dist_to_ref)))
	#  liste.append(round(len(self.liste_nodes), 2))
	# 	# liste.append(self.proportion_environmental_sequence)
	#  liste.append(self.distance_all_vs_all)
	# liste.append(self.mean_percent_dist_all_vs_nr)

	# liste.append(self.density)
	# liste.append(self.dn_ds)

	# liste.append(self.mean_mean_abundance)
	# liste.append(self.mean_nb_site)

	header = ["id", "dist_ref", "number_seq", "prop_env", "mean_dist_AvA", "mean_dist_nr",  "density",
	          "dn_ds", "mean_mean_abundances", "mean_number_sites"]
	print(out_cluster)
	with open(out_cluster, "w") as output:
		output.write("\t".join(header) + "\n")

		for key in liste_clusters:
			output.write("{}\n".format("\t".join(dico_cluster[key].dump())))

	print("analyses step done")

if __name__ == "__main__":

	parse = argparse.ArgumentParser(description="This script will analyse the output of ISF, It will create several new files in the working directory please see readme for further information")
	parse.add_argument("-isf_wd", required=True, help="the output directory of ISF")
	parse.add_argument("-dump", default=None, help="required for nr taxonomy can be generated using make_taxo_db.sh")
	parse.add_argument("-fna_db", default=None, help="nucleotide fasta with seed sequence for dn ds")
	parse.add_argument("-fna_seq", default=None, help="nucleotide fasta with environmental sequence for dn ds")
	parse.add_argument("-th", default=5, type=int, help="number of thread to use")
	parse.add_argument("-cover_th", default=0.8, help="minimal mutual cover default 0.8")
	parse.add_argument("-pid_th", default=30.0, help="minimal pid default 30.0")
	parse.add_argument("-abundances", default=None, help="abundance matrix")
	parse.add_argument("-abundances_thr", default=1, type=float, help="abundance threshold, counting presence over this value")
	parse.add_argument("-pid_nr_annot", default=0, type=float, help="pid use to validate a best hit in ncbi [0]")
	parse.add_argument("-no_indexed_dump", action="store_true", help="prot_2_taxid file in ncbi dump dir is not naturally sort (sort -V option)")
	parse.add_argument("-ask_ncbi", action="store_true", help="if set to tru will send request to ncbi server with unfound taxo id be carreful "
	                                                          "can only send 500 id seconde and therefore can be really long")

	# parse.add_argument("-blast_header", default="qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen",
	#                    help="default: \"qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen\"")
	args = parse.parse_args()
	indexed_dump = True
	if args.no_indexed_dump:
		no_indexed_dump= False

	main_analyses(wd_=args.isf_wd, taxo_dir=args.dump, fna_db=args.fna_db,
	              fna_seq=args.fna_seq, th=args.th, abundance_file=args.abundances, thr_ab=args.abundances_thr,
	              ask_ncbi=args.ask_ncbi, indexed_dump=indexed_dump, pid_threshold_annot=args.pid_nr_annot,
	              cover_threshold=args.cover_th)
