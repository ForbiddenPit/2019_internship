from .fonction import check_internet_connection, check_cover
import urllib
import re


#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#


def get_tax_id(taxfile, my_set, dico, set_found):

	goal = len(my_set)
	cpt = 0
	with open(taxfile) as tax:
		tax.readline()

		for line in tax:
			spt = line.strip().split()

			if spt[1] in my_set:
				cpt += 1
				set_found.add(spt[1])
				if spt[2] in dico:
					dico[spt[2]].add(spt[1])

				else:
					dico[spt[2]] = set([spt[1]])

			if cpt == goal:
				return


def tax_id_by_tax_id(id_):

	base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id={}retmode=xml"

	url = base_url.format(id_)
	response = urllib.request.urlopen(url)
	data = response.read()
	data = data.decode('utf-8').split('\n')

	for element in data:
		if "TaxId" in element:
			tax_id = re.search('>(.*)<', element)
			print(url)
			return tax_id.group(1)


def tax_id_by_prot(id_, dico_names_to_taxo):
	base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id={}&retmode=xml"

	url = base_url.format(id_)
	response = urllib.request.urlopen(url)
	data = response.read()
	data = data.decode('utf-8').split('\n')

	for indice, element in enumerate(data):

		if 'GBSeq_organism' in element:
			Organism_names = re.search('>(.*)<', element).group(1)
			continue

		# in this file we have all the taxonomy associated
		# but the species don't exist so we hope the parent exist and we give the parent id instead
		if 'GBSeq_taxonomy' in element:
			reg_rez = re.search('>(.*)<', element)
			all_taxo = reg_rez.group(1)

			taxo_split = all_taxo.split(';')
			taxo_split = [elem.strip() for elem in taxo_split]

			return id_, dico_names_to_taxo[taxo_split[-1]], taxo_split[0], Organism_names, '/'.join(taxo_split)


# TODO remove afetr lopez 2!
def check_cover(split_line, cover_th):

	qcov = (int(split_line[6]) - int(split_line[5]) + 1) / int(split_line[9])
	scov = (int(split_line[8]) - int(split_line[7]) + 1) / int(split_line[10])

	if min([qcov, scov]) >= cover_th:
		return True
	else:
		return False

def main(blast_vs_nr, dico_node, cover_threshold, dump_dir=None):

	set_found_prot = set()

	dico_prot_node = {}

	dico_taxId_setProt = {}
	set_annotated_prot = set()
	set_pdb, set_prot = set(), set()


	# SCRAP BLAST
	# also annot node pid, and prot*
	pdb_reg = re.compile("\w{4}_\w{1}")
	with open(blast_vs_nr) as blast:
		for line in blast:
			spt = line.strip().split()

			if not check_cover(split_line=spt, cover_th=cover_threshold):

				continue
			#print(spt)
			try:
				prot_match = spt[1]
				#print(prot_match)
			except:
				print(line, spt)
				raise
			if pdb_reg.match(prot_match):
				set_pdb.add(prot_match)
			else:
				set_prot.add(prot_match)

			if prot_match in dico_prot_node:
				dico_prot_node[prot_match].append(dico_node[spt[0]])
			else:

				dico_prot_node[prot_match] = [dico_node[spt[0]]]

			dico_node[spt[0]].nearest_ncbi_percent_id = spt[3]
			dico_node[spt[0]].nearest_ncbi_prot = spt[1]

	if not dump_dir:
		print("no dump dir")
		return

	dump_dir = dump_dir.rstrip('/') + '/'
	pdb_tax_file = dump_dir + "pdb.accession2taxid"
	prot_tax_file = dump_dir + "prot.accession2taxid"
	complete_file = dump_dir + "fullnamelineage.dmp"
	# update dico_taxId_setProt
	#print(set_prot, set_pdb)
	if set_pdb:
		print('pdb')
		get_tax_id(pdb_tax_file, set_pdb, dico_taxId_setProt, set_found_prot)
	if set_prot:
		print("prot")
		get_tax_id(prot_tax_file, set_prot, dico_taxId_setProt, set_found_prot)


	not_find = list(set(list(dico_prot_node.keys())) - set_found_prot)
	if not_find:
		print(not_find)
	print("get annotation from tax id")
	set_tax_id_found = set()
	# now we parse the complete file and we update node info
	goal = len(dico_taxId_setProt.keys())
	cpt = 0

	with open(complete_file) as file_in:

		for line in file_in:
			spt = line.strip().split("|")
			spt = [elem.strip() for elem in spt if elem]

			if spt[0] in dico_taxId_setProt:

				cpt += 1
				species = spt[1]
				set_tax_id_found.add(spt[0])

				if len(spt) == 2:  # base node
					complete = spt[2].split(';')
					taxo = complete[:-1] + [species]

				else:
					complete = spt[2].split(';')
					taxo = complete[:-1] + [species]

				for prot_name in dico_taxId_setProt[spt[0]]:

					for node_instance in dico_prot_node[prot_name]:

						node_instance.phylogeny = taxo
						node_instance.nearest_ncbi_prot = prot_name


			if cpt == goal:
				return
	if check_internet_connection():


		pass
	# done
