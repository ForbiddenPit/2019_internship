import urllib
import numpy as np
import subprocess
from .dn_ds import dn_ds
import os

#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#


def check_internet_connection():
	try:
		urllib.request.urlopen("http://google.com.", timeout=1)
		return True
	except urllib.error.URLError:
		return False
	except:
		raise


def line_generator(line_spt, reverse=False):
	""" input a line of the edges files allow to iterate over this line step by step"""
	# because I love generator
	# generator is beautiful
	# generator if life

	# input a line of the edges files:
	# allow to iterate over this line step by step

	if reverse:

		indice = 1

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[-indice], line_spt[-indice - 10].strip()
			if indice - 1 == 0:
				midle = line_spt[indice - 11: - 1]
			else:
				midle = line_spt[indice - 10:indice - 1]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)

	else:

		indice = 0

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[indice], line_spt[indice + 10].strip()
			midle = line_spt[indice + 1:indice + 10]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)


def replace_round(id, dico, round, start_seq):
	node_instance = dico[id]

	if not node_instance.round:
		node_instance.round = round
		node_instance.relative_seq_start = start_seq

	else:
		if node_instance.round > round:
			node_instance.round = round
			node_instance.relative_seq_start = start_seq

	return None


# TODO duplication may be find a better ways
def get_seq_one_by_one(file_):
	"""Generator return prompt sequence for each sequence"""

	sequence = ''
	prompt = ''

	for line in file_:

		if line.startswith('>'):

			if sequence:
				yield [prompt, sequence]

			sequence = ''
			prompt = line.strip()[1:]

		else:
			sequence += line.strip()

	yield [prompt, sequence]


def grep_fasta(sequence_file, id_file, out_file):

	set_id = set()
	if not isinstance(id_file, (set, str)):
		with open(id_file) as id_:
			for line in id_:
				set_id.add(line.strip())
	else:
		set_id = set([elem.replace(">", '') for elem in id_file])

	nb_seq = len(set_id)
	cpt = 0
	with open(sequence_file, 'r') as seqfile, open(out_file, 'w') as out_:
		for prompt, sequ in get_seq_one_by_one(seqfile):
			if prompt in set_id:
				out_.write(">{}\n{}\n".format(prompt, chunck_sequence(sequ)))
				cpt += 1
			if cpt == nb_seq:
				return


def get_dn_ds(tuple_args):

	idref, idenv, reffna, enffna, seq_finded, temp_dir, log = tuple_args

	temp_dir = temp_dir.rstrip('/') + '/'
	os.makedirs(temp_dir)

	temp_file0 = temp_dir + 'temp_base'
	temp_file1 = temp_dir + 'temp_env'
	temp_file_pro = temp_dir + 'prot_pairwise_dn_ds'
	temp_file_nuc = temp_dir + 'nuc_pairwise_dn_ds'

	grep_fasta(sequence_file=reffna, id_file=idenv, out_file=temp_file1)
	grep_fasta(sequence_file=enffna, id_file=idref, out_file=temp_file0)
	child = subprocess.Popen("cat {} {} >> {}".format(temp_file0, temp_file1, temp_file_nuc), shell=True)
	child.wait()
	os.remove(temp_file0)
	os.remove(temp_file1)

	grep_fasta(sequence_file=seq_finded, id_file=idenv, out_file=temp_file1)
	grep_fasta(sequence_file=seq_finded, id_file=idref, out_file=temp_file0)
	child = subprocess.Popen("cat {} {} >> {}".format(temp_file0, temp_file1, temp_file_pro), shell=True)
	child.wait()
	os.remove(temp_file0)
	os.remove(temp_file1)

	tuple_args = (temp_file_pro, temp_file_nuc, "mafft", temp_dir, "yn00", log)
	try:
		dn_ds_value = dn_ds.main(tuple_args)
	except:
		dn_ds_value = "NA"

	return dn_ds_value


def chunck_sequence(sequence):
	inc = 80
	cpt = 0
	seq = ""
	while cpt <= len(sequence):
		seq += sequence[cpt:cpt + inc] + '\n'
		cpt += inc
	return seq.strip()


def mean(liste_):
	return sum(liste_) / len(liste_)

def check_cover(split_line, cover_th, dico_header=None):
	# 	qcov = (int(split_line[dico_header["qend"]]) - int(split_line[dico_header["qstart"]]) + 1) / \
	# 	       int(split_line[dico_header["qlen"]])
	# 	scov = (int(split_line[dico_header["send"]]) - int(split_line[dico_header["sstart"]]) + 1) / \
	# 	       int(split_line[dico_header["slen"]])
	qcov = (int(split_line[6]) - int(split_line[5]) + 1) / int(split_line[9])
	scov = (int(split_line[8]) - int(split_line[7]) + 1) / int(split_line[10])

	if min([qcov, scov]) >= cover_th:
		return True
	else:
		return False

def standard_deviation(liste_):
	mean_liste = mean(liste_)
	return sum([(x - mean_liste) ** 2 for x in liste_]) ** 1 / 2


def get_all_quartile(liste_):
	return np.percentile(np.array(liste_), np.arange(0, 100, 25)).tolist()


def total_possible_edges(number_nodes):

	return (number_nodes * (number_nodes - 1)) / 2


def number_edges(graph):

	return len(graph.es)


def sum_edges_weigth(graph):

	return sum(graph.es["weights"])

