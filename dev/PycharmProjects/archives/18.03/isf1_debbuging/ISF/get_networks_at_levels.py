import argparse
import igraph
import ete3

parse = argparse.ArgumentParser(description="")
parse.add_argument("-isf_wd", required=True, help="the output directory of ISF")
parse.add_argument("-levels", default=None, help="")
parse.add_argument("-out", default=None, help="")
args = parse.parse_args()


def my_concat(graph, liste_membership):
	"""

	:param graph: a graph from igraph
	:param liste_membership: a list memebership like [0, 0, 2, 2, 4, 5, 5]
	:return: a new concat graph
	"""

	g = graph.copy()
	g.contract_vertices(liste_membership)
	g.simplify()

	to_delete_ids = [v.index for v in g.vs if v.degree() == 0]
	g.delete_vertices(to_delete_ids)

	return g


def get_subg_for_a_levels(clusters_list, levels, graph, node_igraph, dico_cluster):

	liste_clustering = [0] * len(graph.vs)

	cluster_num = 0
	dico_subg_clusterId_to_igraph = {}
	dico_subg_igraph_to_clusterId = {}

	my_liste = clusters_list[levels]

	for cluster_node in my_liste:

		cluster_id = cluster_node.name
		cluster_instance = dico_cluster[cluster_id]
		for node in cluster_instance.liste_nodes:
			liste_clustering[node_igraph[node.id_]] = cluster_num


		dico_subg_clusterId_to_igraph[cluster_instance.id_] = cluster_num
		dico_subg_igraph_to_clusterId[cluster_num] = cluster_instance.id_
		cluster_num += 1
	sub_g = my_concat(graph, liste_clustering)

	return sub_g, dico_subg_clusterId_to_igraph, dico_subg_igraph_to_clusterId



def check_cover(split_line, cover_th):
	qcov = (int(split_line[6]) - int(split_line[5]) + 1) / int(split_line[9])
	scov = (int(split_line[8]) - int(split_line[7]) + 1) / int(split_line[10])

	if min([qcov, scov]) >= cover_th:
		return True
	else:
		return False


def parse_blastp_AvA(blastp, cover_th=None, pident_th=None):
	"""
	# this function do not check anything blast must be cleaned before hand
	:param blastp: a cleaned blast file
	:return: a dico edges -> tuple(liste[node_id], line.strip().split())
	"""

	dico_edges = {}
	liste_node = set()
	with open(blastp) as in_file:
		for line in in_file:
			spt = line.strip().split('\t')

			if cover_th and not check_cover(split_line=spt, cover_th=cover_th):
				continue

			if pident_th and float(spt[3]) < pident_th:
				continue

			# get the sequence id
			id_1 = spt[0]
			id_2 = spt[1]

			# edges is undirected so sorting them allow to consider reciprocal hit
			edges = tuple(sorted([id_1, id_2]))

			# if the edges exist we keep the ones with the best (minimal) evalue
			# may be better adding a scoring evelu and coverage?

			dico_edges[edges] = spt
			liste_node.add(id_1)
			liste_node.add(id_2)

	return list(liste_node), dico_edges


def make_graph(dico_edges, list_of_actual_nodes):
	"""

	:param dico_edges: a dico edges (tuple(id1, id2)) -> blast line.strip().split()
	:param list_of_actual_nodes: a list of all nodes ids
	:return:
	"""

	edges = []
	nodes = []
	node_real_names = {}  # will contain dico[igraph_id] = realnames
	node_igraph = {}  # will contain dico[realnames] = igraph_id

	weights = []

	for indice, node in enumerate(list_of_actual_nodes):
		node_real_names[indice] = node
		node_igraph[node] = indice
		nodes.append(indice)
		#dico_nodes[node].igraph_id = indice

	for keys in dico_edges:

		try:
			edges.append((node_igraph[keys[0]], node_igraph[keys[1]]))
			midle = dico_edges[keys]
			weights.append(float(midle[3]))
		except:
			print(dico_edges)
			print(node_igraph)
			print(keys)
			print(keys[0])
			raise

	return (edges, nodes, node_real_names, node_igraph, weights)


def build_networks_from_blast(blastp, cover_th=None, pident_th=None):
	liste_node, dico_edges = parse_blastp_AvA(blastp=blastp,  cover_th=cover_th, pident_th=pident_th)
	edges, nodes, node_real_names, node_igraph, weights = make_graph(list_of_actual_nodes=liste_node,
																	 dico_edges=dico_edges)
	g = igraph.Graph(vertex_attrs={"labels": nodes}, edges=edges, directed=False)
	g.es["weights"] = weights
	return (g, node_real_names, node_igraph)


blast_file = args.isf_wd.rstrip("/") + "/all_against_all.blastp"
tree_file = args.isf_wd.rstrip("/") + "/outtree_networks.nwk"
tree = ete3.Tree(tree_file, format=3)
sequ_level_file = args.isf_wd.rstrip("/") + "/outtree_networks_which_seq.nwk"

def get_nodes_of_levels(tree, levels: int):
	"""

	:param tree: ete3 tree with levels attributes on node
	:param levels: an int
	:return:
	"""
	liste = []
	for leaf in tree.traverse():
		if not leaf.name:
			leaf.name = "0_0"
		if int(leaf.name.split('_')[0]) == levels:
			liste.append(leaf)
	return liste
#	return [leaf for leaf in tree.traverse() if int(name.split('_')[0]) == levels]



def get_clusters_id_by_levels(big_tree):
	"""

	:param big_tree: ete3 describing a clustering, with special naming convention for leaf name : levels_sub_uniq
	:return: a [[clusteids levels top], ...,[ clustersid levels bottom]] || carefull if a cluster ends ( no leaf) before the bottom
	is added to succesive levels
	"""

	levels = 0
	node_list_levels = get_nodes_of_levels(big_tree, levels)

	leaf_at_levels = {}
	community_at_levels = {}
	maxlevels = big_tree.get_farthest_leaf()[1] + 1

	# second condition seems to be enough may be I will may be remove the first
	while node_list_levels and levels < maxlevels:

		node_list_levels = get_nodes_of_levels(big_tree, levels)  # get all node at a levels
		community_at_levels[levels] = node_list_levels  # assign it to our dict to tract

		for node in node_list_levels:

			if node.is_leaf():

				if levels in leaf_at_levels:
					leaf_at_levels[levels].append(node)

				else:
					leaf_at_levels[levels] = [node]

		for levels_range in range(0, levels, 1):

			if levels_range in leaf_at_levels:
				community_at_levels[levels].extend(leaf_at_levels[levels_range])

		levels += 1

	return community_at_levels, levels - 1

community_at_levels, levels = get_clusters_id_by_levels(tree)
g, node_real_names, node_igraph = build_networks_from_blast(blast_file, cover_th=0.80, pident_th=30.0)

sub_g, dico_subg_clusterId_to_igraph, dico_subg_igraph_to_clusterId = get_subg_for_a_levels(clusters_list=community_at_levels,
																							levels=int(args.level),
                                                                                            graph=g,
                                                                                            node_igraph=node_igraph,
                                                                                            dico_cluster=)


#
#
# parse.add_argument("-isf_wd", required=True, help="the output directory of ISF")
# parse.add_argument("-levels", default=None, help="")
# parse.add_argument("-out", default=None, help="")