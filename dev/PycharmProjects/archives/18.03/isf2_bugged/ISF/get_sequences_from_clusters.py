import os
import argparse
import ete3

parse = argparse.ArgumentParser(description="return a list of sequences identifier corresponding to a cluster, if option fasta is set return the fasta instead")
parse.add_argument("-isf_wd", required=True, help="the output directory of ISF")
parse.add_argument("-cluster", default=None, help="the cluster identifier")
parse.add_argument("-out", default=None, help="if set outfile else stout")
parse.add_argument("-faa", action="store_true")
parse.add_argument("-fna", action="store_true")
args = parse.parse_args()


def get_seq_one_by_one(file_):
	"""Generator return prompt sequence for each sequence"""

	sequence = ''
	prompt = ''

	for line in file_:

		if line.startswith('>'):

			if sequence:
				yield [prompt, sequence]

			sequence = ''
			prompt = line.strip()[1:]

		else:
			sequence += line.strip()

	yield [prompt, sequence]


def chunck_sequence(sequence, inc=60):
	cpt = 0
	seq = ""
	while cpt <= len(sequence):
		seq += sequence[cpt:cpt + inc] + '\n'
		cpt += inc
	return seq.strip()


tree_file = args.isf_wd.rstrip("/") + "/outtree_networks.nwk"
tree = ete3.Tree(tree_file, format=3)
node = tree.search_nodes(name=args.cluster)[0]

# get a set of all leaves
my_set_of_leaves = set()
for leave in node.iter_leaves():
	my_set_of_leaves.add(leave.name)


identifier_file = args.isf_wd.rstrip("/") + "/outtree_networks_which_seq.nwk"

with open(identifier_file) as id_file:
	current = "-1"
	liste_seq = []
	for line in id_file:
		if line.startswith(">"):
			current = line.strip()[1:]
		else:
			if current in my_set_of_leaves:
				liste_seq.append(line.strip())
#print(my_set_of_leaves)
my_set_of_seq = set(liste_seq)


if args.faa:
	fasta_file = args.isf_wd.rstrip("/") + "/sequence_found_and_bases.faa"
	if args.out:
		with open(args.out, 'w') as out_, open(fasta_file) as fasta_:
			for prompt, seq in get_seq_one_by_one(fasta_):

				if prompt in my_set_of_seq:
					out_.write(">{}\n{}\n".format(prompt, chunck_sequence(seq)))
	else:
		with open(fasta_file) as fasta_:
			for prompt, seq in get_seq_one_by_one(fasta_):
				#print(prompt)
				if prompt in my_set_of_seq:
					print(">{}\n{}".format(prompt, chunck_sequence(seq)))
elif args.fna:
	#v db.dna, seq.fna
	fasta_file = args.isf_wd.rstrip("/") + "/db.fna"
	f2 = args.isf_wd.rstrip("/") + "/seq.fna"
	if args.out:
		with open(args.out, 'w') as out_, open(fasta_file) as fasta_, open(f2) as fasta_2:

			for prompt, seq in get_seq_one_by_one(fasta_):
				if prompt in my_set_of_seq:
					out_.write(">{}\n{}\n".format(prompt, chunck_sequence(seq)))

			for prompt, seq in get_seq_one_by_one(fasta_2):
				if prompt in my_set_of_seq:
					out_.write(">{}\n{}\n".format(prompt, chunck_sequence(seq)))
	else:
		with open(fasta_file) as fasta_, open(f2) as fasta_2:
			for prompt, seq in get_seq_one_by_one(fasta_):
				#print(prompt)
				if prompt in my_set_of_seq:
					print(">{}\n{}".format(prompt, chunck_sequence(seq)))

			for prompt, seq in get_seq_one_by_one(fasta_2):
				if prompt in my_set_of_seq:
					print(">{}\n{}".format(prompt, chunck_sequence(seq)))

else:
	if args.out:
		with open(args.out, 'w') as out_:
			for elem in liste_seq:
				out_.write("{}\n".format(elem))
	else:
		for elem in liste_seq:
			print(elem)
