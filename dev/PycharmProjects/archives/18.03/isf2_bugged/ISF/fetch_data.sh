#!/usr/bin/awk

function search_db(tx_db, id) {
	while(id != searched_id) {
		getline l < tx_db;
		split(l, lf, "\t|\t");
		searched_id = lf[1];
	}
	gsub("; ", "\t", lf[3]);
	close(tx_db);
	return(lf[2] lf[3]);
}

BEGIN { 
	file_idx = 0; k = 0; FS = "\t";
}

FNR == 1 {
	file_idx++;
}

file_idx == 1 {
	id[k] = $2;
	k++;
}

file_idx == 2 && FNR == 1 {
	k = 0;
}
file_idx == 2 {
	tx[k] = $0;
	k++;
}

END {
	for(i = 0; i < k; i++) {
		lin[tx[i]] = search_db(tx_db, tx[i]);
		print id[i] "\t" tx[i] lin[tx[i]];
	}
}
' "$ID_L" "$TX_L" >> "$TX_TAB";

