
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

#!/usr/local/bin/python3.5

import subprocess
import os
import sys

from utils_chain_blast.utils import utilitaire

# TODO may be use the csv module

""" This modules manage the edges creation and verification I have try to cut it into small function pieces,
 But may be OOP may be better """


def line_generator(line_spt, reverse=False):
	""" input a line of the edges files allow to iterate over this line step by step"""
	# because I love generator
	# generator is beautiful
	# generator if life
	
	# input a line of the edges files:
	# allow to iterate over this line step by step
	
	if reverse:
		
		indice = 1
		
		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[-indice], line_spt[-indice - 10].strip()
			if indice - 1 == 0:
				midle = line_spt[indice - 11: - 1]
			else:
				midle = line_spt[indice - 10:indice - 1]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)
	
	else:
		
		indice = 0
		
		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[indice], line_spt[indice + 10].strip()
			midle = line_spt[indice + 1:indice + 10]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)


def clean_blast(blast_file, id_, cov,  header=False):
	""" Clean a blast file depending on cov and id value erase old file"""
	
	new_blast = '{}_clean'.format(blast_file)
	child = subprocess.Popen(['touch', '{}'.format(new_blast)])
	child.wait()
	
	# to sort by ident et coverage we use a awk line
	# also we check for mutual cover
	# POssible TODO refarctor with personal binary # or when reading the file
	#// qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen
	if header:
		child = subprocess.Popen("".join(["awk ",
		                                  "\'BEGIN", "{a=0}", "{a+=1; if(a == 1)\
                {print $0}", "else if",
		                                  "($4>={0} && ($7-$6+1)/$10>={1} && ($9-$8+1)/$11>={1} && $1!=$2) ".format(
			                                  id_, cov),
		                                  "{print $0}}\' ", " {}".format(blast_file), " > ", "{}".format(new_blast)]), \
		                         shell=True)
		child.wait()
	else:
		cmd = ["awk ",
		       "\'($4>={0} && ($7-$6)/$10>={1} && ($9-$8)/$11>={1} && $1!=$2)".format(id_, cov),
		       "{print $0}\' ", "{}".format(blast_file), " > ", "{}".format(new_blast)]
		
		child = subprocess.Popen("".join(cmd), shell=True)
		child.wait()
	
	# erase old blast file
	child = subprocess.Popen(['mv', '{}'.format(new_blast), '{}'.format(blast_file)])
	child.wait()
	
	
	# check fasta not empty
	try:
		assert os.path.getsize(blast_file) > 0
	except AssertionError:
		print('cleanfasta return empty files you may condider the program ended')
		raise utilitaire.NoSequenceFound("no seuqnece found clean fasta")

def blast_todict(blast_output):
	dico_blast = dict()
	
	with open(blast_output) as blast_file:
		
		for line in blast_file:
			spt = line.split('\t')
			tar_id = spt[1]
			query_id = spt[0]
			
			# first time we see this id
			if query_id not in dico_blast:
				# if this query have match something
				dico_blast[query_id] = {tar_id: line}
			
			else:
				# we have already this tar id but not the query id
				if tar_id not in dico_blast[query_id]:
					
					dico_blast[query_id] = {tar_id: line}
				
				else:
					old_line = dico_blast[query_id][tar_id]
					# we check the e-value but we could do it for other parameters
					# TODO check if e-value most pertinent
					if int(old_line.split('\t')[2]) < int(line.split('\t')[2]):
						dico_blast[query_id][tar_id] = line
	
	return dico_blast


# TODO here I put the single core version I may want to change this for the multi core version

# TODO here manage the line to may be able to print it directly

	
def overlap(min1, min2, max1, max2):

	return max(0, min(int(max2), int(max1)) - max(int(min1), int(min2)))


def check_size_to_first(old_line_splt, line_splt, cover):
	max = (1 + (1 - cover))
	# get the size
	origin, value = int(old_line_splt[8]), int(line_splt[10])
	if origin * cover < value < origin * max:
		return True
	
	else:
		return False
	
	
def check_projection(old_line_splt, line_splt, threshold=0.8):
	
	project = overlap(int(old_line_splt[-5]), int(line_splt[-6]), int(old_line_splt[-4]), int(line_splt[-5]))
	if (project / int(line_splt[9])) >= threshold:
		return True
	else:
		return False


def check_tolast_fromstart(old_line, line_splt=None):
	
	# these are the follower of the conservative projection
	start_more, end_more = 0, 0  # for a more explicit
	
	if line_splt:
		# old element represent the current blast ant element the n - 1 blast
		old_element = line_splt[2:]
	else:
		old_element = None
	
	# the line_generator is an iterator wich return a tuple((id1,id2), other info (i.e.:the rest of the blast line), number of iteration)
	for elem in line_generator(old_line, reverse=True):
		element = elem[1]
		element = [float(element) for element in element]
		
		# in the case no line_spt provided take the current element as old_element
		if not old_element:
			old_element = element
			continue
		
		old_element = [float(element) for element in old_element]
		
		if old_element[3] + start_more > element[5]:  # the start
			delta = abs(old_element[3] - element[5] - start_more)
			start_more = delta
		
		else:
			point_start = element[4]
			start_more = 0
		
		if old_element[4] < element[6] - end_more:  # the end
			delta = element[6] - old_element[4] - end_more
			end_more = delta
		
		else:
			point_end = element[6]
			end_more = 0
		
		old_element = element
	
	try:
		assert element
	except:
		print(old_line)
		print(line_splt)
		print(old_element)
		print(element)
		raise
	
	if ((element[6] - end_more) - (element[5] + start_more)) / element[7] >= 0.8:
		return True

	else:
		return False


def check_for_edges(old_line_splt, line_splt, test_all_chain, threshold=0.8):
	"""

	:param old_line_splt: string -> line in edges file
	:param line_splt:  sting -> blast line
	:param test_all_chain: -> enumeration should be one of this value: "no", "previous", "all" no means no test function return True,
	 previous will test conservation of alignment for the last sequence only (based on the threshold parameter and assert the size of the new sequence compare to the original one)
	 "all" will test conservation of the alignment position from original sequences to the new one)
	:param threshold: threshold to compare with projection of alignment
	:return: a bolean, True or False may raise Assertion error if parameter incorect
	"""

	try:
		assert test_all_chain in ["no", "previous", "all"]

	except AssertionError:
		print("test all chain should have one this value only : no, previous, all")
		raise
	# testing test_all chein that way allow to the minimum refractor of code
	if test_all_chain == "no":  # no need to check
		return True

	else:
		bool1 = check_projection(old_line_splt, line_splt, threshold=threshold)
		bool2 = check_size_to_first(old_line_splt, line_splt, cover=threshold)

	if test_all_chain == "all":
		bool3 = check_tolast_fromstart(old_line_splt, line_splt)
		return bool1 and bool2 and bool3

	elif test_all_chain == "previous":
		return bool1 and bool2

								
def manages_edges_file(edges_file, blast_output, all_chain=False, cover_thr=0.8):
	"""This function will manage the edge file"""
	
	
	# the files already exist
	if os.path.isfile(edges_file):
		
		# first let's make a  dict of the blast in the form dico[query][tar] = line
		dico_blast = blast_todict(blast_output=blast_output)
		
		with open(edges_file, 'r') as ed_file, open(edges_file + '.tmp', 'w') as new_ed_file:
			
			for line in ed_file:
				
				spt = line.strip().split('\t')
				pre_last_id, last_id = spt[-11], spt[-1].strip()
				if last_id in dico_blast:
					# for every target match by this query  # the last target sequence have been use for the blast
					for key in dico_blast[last_id]:
						blast_line = dico_blast[last_id][key].strip().split('\t')

						# here I check the projection last to previous, the size, and the  projection on all the path
						if check_for_edges(spt, blast_line, all_chain, cover_thr):
							new_line = line.strip() + '\t'
							new_line += '\t'.join(blast_line[2:])
							new_line += '\t' + blast_line[1] + '\n'
							
							new_ed_file.write(new_line)
							
						else:
							new_ed_file.write(line)
				else:
					new_ed_file.write(line)
				
		child = subprocess.Popen('mv {} {}'.format(edges_file + '.tmp', edges_file), shell=True)
		child.wait()
		pass
	
	# the file don't exist it is the fist iteration
	else:
		
		with open(blast_output) as blast_file, open(edges_file, 'w') as ed_file:
		
			for line in blast_file:
				
				spt = line.split('\t')
				# remove the end line car
				spt[-1] = spt[-1].strip()
				# write to the file query blast info target
				ed_file.write(spt[0] + '\t')
				ed_file.write('\t'.join(spt[2:]))
				ed_file.write('\t' + spt[1] + '\n')
				
	# then we suppress the blast files
	os.remove(blast_output)
