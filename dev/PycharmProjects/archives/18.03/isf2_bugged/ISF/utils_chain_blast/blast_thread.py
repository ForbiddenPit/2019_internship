
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

#!/usr/local/bin/python3.5


import time
from multiprocessing import Pool
import subprocess
import os
from shutil import copyfileobj
from utils_chain_blast.utils.utilitaire import get_seq_one_by_one, remove
from utils_chain_blast.aligner import Aligner


def get_seq_number(file):
	cpt = 0
	with open(file) as in_:
		for pr, se in get_seq_one_by_one(in_):
			cpt += 1
	return cpt


def number_of_split(file_, maxsplit):
	"""return the number of split to do"""

	sequence_number = get_seq_number(file_)
	# int(subprocess.check_output(['grep', '-c', '>', '{}'.format(file_)]).decode('utf-8'))
	if sequence_number < maxsplit:
		toreturn = sequence_number - 1
	else:
		toreturn = maxsplit
	if toreturn < 2:
		toreturn = 1
	return toreturn


def blast(args_tuple):
	"""Just launch the blast"""

	query, database, output_file, blastp_ = args_tuple

	cmd = ['{}'.format(blastp_), '-query', '{}'.format(query), '-db', '{}'.format(os.path.abspath(database)),
		   '-out', '{}'.format(output_file),
		   '-outfmt', "6 qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen",
	        "-seg", "yes", "-soft_masking", "true", "-max_hsps", "1", "-max_target_seqs", "10000"]

	try:
		print("launching blast with following command = {}".format(" ".join(cmd)))
		child = subprocess.Popen(cmd)
		child.wait()
	except:
		raise
	else:
		return 0

class Blast(Aligner):

	def __init__(self, make_db_bin, blastp_bin, threads, fastasplit):
		self.blastp = blastp_bin
		self.mkdb = make_db_bin
		self.threads = threads
		self.fastasplit = fastasplit

	def make_db(self, input_, output_):
		cmd = '{} -dbtype prot -hash_index -parse_seqids -in {} -out {}'.format(self.mkdb, input_, output_)
		print("making blast_db  with the command line {}".format(cmd))
		child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL)
		child.wait()


	def align(self, db, output, blast_out_dir, split_dir, fasta
	          , max_seq_target,
	          cover, eval_, id, **kwargs):
		#out_dir, wd_split, thread, db, max_split, fasta, blastp_, fastasplit,  db_size=None, ban_liste=None):
		# (out_dir, wd_split, thread, db, blast_header, blast_other_arg,
		#  max_split, fasta, blastp_, fastasplit, db_size=None, ban_liste=None):
		numberspl = number_of_split(file_=fasta, maxsplit=self.threads)

		start_time = time.time()
		number_of_sequence = get_seq_number(fasta)
		print('there is {} sequence to blast'.format(number_of_sequence))

		# split fasta
		child = subprocess.Popen("{} -f {} -o  {} -c {}".format(self.fastasplit, fasta, split_dir, numberspl), shell=True)
		child.wait()

		print('splitfasta for blast takes {}'.format(round(time.time() - start_time, 2)))

		liste_file = os.listdir(split_dir)  # then we get all subfile
		path = os.path.abspath(split_dir) + '/'  # absolute path to the file

		liste_file = [os.path.join(path + element) for element in liste_file]  # file with absolute path

		liste_argument = list() # this list will contain each argument tuple for multiprocess

		for file in liste_file:
			if os.stat(file).st_size == 0:  # empty file
				continue
			# for each file we prepare a job the parameter are put inside a tuple in this order
			# This order have to be the same of the blast fonction unpack (blast first line)
			# query, database, output_file, output_format_arg, other_args, blastp_, db_size, ban_list
			# not super clean to ensure to set it to none where it don't exist yet
			# if ban_liste and os.stat(ban_liste).st_size == 0:
			# 	ban_liste = None
			# make the output file name
			query = file
			output_this = os.path.join(blast_out_dir,  'out_{}'.format(os.path.basename(query)))
			argument_tuple = (query, db, output_this, self.blastp)
			liste_argument.append(argument_tuple)

		start_time = time.time()
		with Pool(processes=self.threads) as pool:
			out_code = pool.map(blast, liste_argument)
			print('blast  takes {}'.format(round(time.time() - start_time, 2)))

		print("concatenated blast output and cleaning the split dir")
		# concate the file
		list_file = [os.path.join(blast_out_dir, elem) for elem in os.listdir(blast_out_dir)]

		with open(output, 'wb') as wfd:
			for file in list_file:
				with open(file, 'rb') as fd:
					copyfileobj(fd, wfd, 1024*1024*10)

		# cleaning split_dir
		remove(*list_file)  # output blast
		remove(*liste_file)  # query splited

	def filter_by_ban_list(self, aln_file, ban_set, temp_file):

		if len(ban_set) == 0:
			return

		with open(aln_file, 'r') as in_, open(temp_file, 'w') as out_:
			for line in in_:
				spt = line.strip().split()
				if not spt[1] in ban_set:
					out_.write(line)

		os.rename(temp_file, aln_file)

	def clean(self, align_file, cover, eval_, id):

		def cover_f(start, end, length):
			return (end - start + 1) / length

		header = "qseqid sseqid evalue pident bitscore qstart qend sstart send qlen slen".split()
		dictHe = dict((value, indice) for indice, value in enumerate(header))

		new_blast = align_file + ".cleaning.tmp"

		with open(align_file) as file, open(new_blast, 'w') as out_:
			for line in file:
				spt = line.strip().split()
				tcov = cover_f(int(spt[dictHe["sstart"]]), int(spt[dictHe["send"]]),
				               int(spt[dictHe["slen"]]))
				qcov = cover_f(int(spt[dictHe["qstart"]]), int(spt[dictHe["qend"]]),
				               int(spt[dictHe["qlen"]]))

				if tcov < cover or qcov < cover:
					continue

				elif float(spt[dictHe["pident"]]) < id:
					continue

				elif float(spt[dictHe["evalue"]]) > eval_:
					continue

				else:
					out_.write(line)

		os.rename(new_blast, align_file)
