import os
from utils_chain_blast.utils import utilitaire

"""
This module just manage the functionality to outptu a networks with edges find valid by ISF
"""


### Do I insert duplicate in it or not?
## No because a dummy user cannot use it and an expert user can make it.

def make_safe_networks(path_tuple):
	edges_dir = path_tuple.edges
	out_file = path_tuple.net_safe
	set_edges = set()
	with open(os.path.join(edges_dir, "_edges_1")) as in_, open(out_file, "w") as out_:

		for line in in_:
			spt = line.strip().split()
			edg = tuple(sorted([spt[0], spt[-1]]))
			if edg not in set_edges:
				out_.write("{}\t{}\n".format(spt[0], spt[-1]))
				set_edges.add(edg)


	with open(os.path.join(edges_dir, "_edges_")) as in_, open(out_file, "a") as out_:

		for edges, info, round in utilitaire.line_generator(in_):

			edg = tuple(sorted(list(edges)))
			if edg not in set_edges:
				out_.write("{}\t{}\n".format(edg[0], edg[1]))
				set_edges.add(edg)

