
from abc import ABCMeta, abstractmethod

# abstract class for aligner to add an aligner would be then more simpler
# herite it implement it and ass it in main loop aligner choice

class Aligner(metaclass=ABCMeta):

	@abstractmethod
	def make_db(self, input_, output_):
		pass

	@abstractmethod
	def align(self, **kwargs):
		pass

	@abstractmethod
	def filter_by_ban_list(self, **kwargs):
		pass

	@abstractmethod
	def clean(self, **kwargs):
		pass