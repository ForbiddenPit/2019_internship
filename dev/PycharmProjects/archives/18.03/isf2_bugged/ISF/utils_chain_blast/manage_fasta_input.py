# This module manage the creation f dictionnary and also select only
# Sequence in database that could align (cover threshold)

#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#
from .utils import utilitaire


def make_dictionary_and_get_size(input_, output, o_dico, start=1):

	first_bool = True
	id_indice = start  # id start for one by default

	with open(input_, buffering=1000000) as input_file, open(output, 'w') as out_file, open(o_dico, 'w') as dico_file:

		for prompt, sequence in utilitaire.get_seq_one_by_one(input_file):

			# This part aim to find the min max sequence length
			length_seq = len(sequence)
			if first_bool:
				min_seq = length_seq
				max_seq = length_seq
				first_bool = False

			if length_seq < min_seq:
				min_seq = length_seq

			if length_seq > max_seq:
				max_seq = length_seq
			# end

			# next It create the dictionary
			dico_file.write("{}\t{}\n".format(prompt, id_indice))
			out_file.write(">{}\n{}\n".format(id_indice, utilitaire.chunck_sequence(sequence)))

			# we increments the value of the indice
			id_indice += 1

	return [min_seq, max_seq, id_indice]


# TODO if I want to speed up here split in subfiles
def screen_by_size_and_make_dico_and_gifasta(min_len, max_len, indice_dico, input_, dico_o, fasta_o,
                                             min_factor, max_factor):

	# threshold for accepting a sequence
	min_sequence_len = int(min_len * (min_factor / 100)) - 1
	max_sequence_len = int(max_len * (max_factor / 100)) + 1

	cpt = 0
	metagenome = 0

	with open(input_, buffering=10000000) as input_file, open(dico_o, 'w') as dico_file, open(fasta_o, 'w') as fasta_out:

		for prompt, sequence in utilitaire.get_seq_one_by_one(input_file):

			# if the sequence is in the threshold we write
			if min_sequence_len <= len(sequence) <= max_sequence_len:
				dico_file.write("{}\t{}\n".format(prompt, indice_dico))
				fasta_out.write(">{}\n{}\n".format(indice_dico, utilitaire.chunck_sequence(sequence)))

				cpt += 1
				indice_dico += 1

			metagenome += 1
	return cpt, metagenome


def main(interest_family, metagenome, dico_family_interest, dico_metagenome, fasta_small_metage,
		 interest_family_simpl,  min_factor, max_factor):
	"""  """
	# first make dictionnary of the family interest and return the min max len of sequence in
	min_len, max_len, indice_dico = make_dictionary_and_get_size(input_=interest_family, output=interest_family_simpl,
																 o_dico=dico_family_interest)

	# make dictionnary of metagenome also filter by coverage to select only possible hit and so
	#  limit the metagenome size.
	cpt, metagenome = screen_by_size_and_make_dico_and_gifasta(min_len=min_len, max_len=max_len, indice_dico=indice_dico,
												input_=metagenome, dico_o=dico_metagenome,
												fasta_o=fasta_small_metage, min_factor=min_factor, max_factor=max_factor)

	print("shortest sequence: {} aa\nlongest sequence: {} aa\n \
    metagenome size: {}\nnumber of selected sequences: {}".format(min_len, max_len, metagenome, cpt))

	# we return the number of sequence that pass the coverage filter for -dbsize option
	# 	nd the min max size
	return (cpt, min_len, max_len)
