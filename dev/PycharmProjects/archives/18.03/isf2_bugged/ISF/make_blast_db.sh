#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

# download the nr databases from ncbi
# and make it

# TODO
#ftp://ftp.ncbi.nlm.nih.gov/blast//db/FASTA/nr.gz

boolean="false"
while [[ $boolean == "false" ]]
do
    wget ftp://ftp.ncbi.nlm.nih.gov/blast//db/FASTA/nr.gz
    wget ftp://ftp.ncbi.nlm.nih.gov/blast//db/FASTA/nr.gz.md5

    boolean=$($check_sum "nr.gz" "nr.gz.md5")

    if [[ $boolean == "false" ]]
        then
            rm nr.gz nr.gz.md5
    fi
done

# blastp db
# gunzip -c nr.gz | makeblastdb -in - -dbtype prot -title "nr_db"  -parse_seqids -hash_index -out nr_db_blast

# diamond db
# diamond makedb -in nr.gz -db nr_db_dmd
