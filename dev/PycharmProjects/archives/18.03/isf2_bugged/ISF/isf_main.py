#!/usr/local/bin/python3.5
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#        Contact romain.lannes@protonmail.com object ISF_report
#


import argparse
import os
import subprocess
import sys
import shutil
from utils_chain_blast import blast_thread, renames_edges_files, manage_fasta_input, diamond_cpu
from utils_chain_blast.utils import utilitaire
from collections import namedtuple
import utils_chain_blast.main_loop_uniq

######################
### Start args #######
######################
parser = argparse.ArgumentParser(description='This script will make blast iteration.')

parser.add_argument('-wd', help='working directory', type=str, default='blast_thread_wd', required=True)
parser.add_argument('-th', help='number of thread to use', type=int, required=True)
parser.add_argument('-run', help='number of run to do', type=int, default=10**6)

# TODO add evalue

parser.add_argument('-pident_thr', help='threshold limit for identity default = 30.0, value > 0.0',
					type=float, default=30.0)

parser.add_argument('-cov_thr', help='threshold limit fo coverage default = 80.0, value > 0.0',
					type=float, default=80.0)


parser.add_argument('-eval_thr', help='threshold limit fo evalue default = 0.00001, value > 0.0',
					type=float, default=0.00001)

parser.add_argument('-min_size', help=' %% of the shortest query size use to limit by size blasted sequence'
									  'default 80',
					 default=80, type=float)

parser.add_argument('-max_size', help=' %% of the longest query size use to limit by size blasted sequence'
									  'default 120',
					 default=120, type=float)

parser.add_argument('-test_all_chain', help='does we test conservation of alignment position, default previous',
                    action='store', choices=["no", "previous", "all"], default="previous")

# parser.add_argument('-metage_AvA', help='If there is a all against all for the metagenome')
# parser.add_argument('-family_vs_metage', help='only use if metage_AvA, the blast of the gene family agaisnt the metagenom')
# parser.add_argument('-family_metage_vs_nr', help='only use if metage_AvA, the blast of the gene family + metagenome agaisnt nr')


######################
### blast_args #######
# ######################

# parser.add_argument('-blast_other_arg', help='other arguments for blast default =\
#  -seg yes -soft_masking true -max_hsps 1 -max_target_seqs=10000', nargs='*', default="-seg yes -soft_masking true -max_hsps 1 -max_target_seqs=10000")
# chaneg hsps to 10 or something sometimes best e val hit not the best

#################################
#####  Path to some binaries  ###
#################################

parser.add_argument('-diamond', help="path to diamond binary if set the software will use diamond instead of blast.")

parser.add_argument('-blast_', help=" where is blast ? default use: blastp"
									" if not in PATH, indicate the path to exe",
					default='blastp')

parser.add_argument('-mkdb_', help='where is makeblastdb ? default use: makeblastdb'
									' if not in PATH, indicate the path to exe',
					default='makeblastdb')

parser.add_argument('-faa_split', help='where is exonerate fastasplit, default use: fastasplit',
					default='fastasplit')

######################################
####  PATH TO FILES     ##############
######################################

parser.add_argument('-query', help='query a query in fasta', required=True)

parser.add_argument('-db_fa', help='fasta of the database fasta format', required=True)

parser.add_argument('-nr_db', help='path to nr database, if set when the iteration finish \
results are blast against nr', type=str)

# Next step of analysis is a blast against nr I could do this here and add it to the output files
# It may be usefull and allow to gain some precious time

#################################################
# references files?                           ###
# a reference database like nr ok             ###
# but what to do with personnal annotations?  ###
#################################################

args = parser.parse_args()
if args.diamond:
	print("alignment will be perform using diamond")

args.cov_thr /= 100

######################
### End ARGS   #######
######################
# init the variable which add all edges find is a set so no duplicate
edges_find = set()

##########################################
# make a working directory ###############
##########################################

# it will have several sub folder
subfolder = ["split", "blast_out", "edges", "concated_blast", "id", "current_fasta"]
try:
	for element in subfolder:
		os.makedirs(os.path.join(args.wd, element))
except:
	print("this working directory already exist please give an other name or erase the old one")
	sys.exit(1)

split_dir = os.path.join(args.wd, subfolder[0]) + '/'
blast_out_dir = os.path.join(args.wd, subfolder[1]) + '/'
edge_dir = os.path.join(args.wd, subfolder[2]) + '/'
concat_dir = os.path.join(args.wd,  subfolder[3]) + '/'
id_dir = os.path.join(args.wd,  subfolder[4])
current_fasta_dir = os.path.join(args.wd, subfolder[5]) + '/'

# make a ban list file
ban_list = os.path.join(args.wd, 'banlist')


child = subprocess.Popen(['touch', '{}'.format(os.path.abspath(ban_list))])
child.wait()

#############################################################################################
# managing dico and simplified fasta also filter by size coverage to limit size of database #
#############################################################################################

print("starting making dictionnary, and filter metagenome by size (coverage)")
databse_dir = os.path.join(args.wd, 'database')
os.mkdir(databse_dir)
metage_database = os.path.join(databse_dir, 'blast_db')


db_dico = os.path.join(args.wd, '{}.dico'.format(os.path.basename(args.db_fa)))
db_simpl_fasta = os.path.join(args.wd, '{}_simplified_id.faa'.format(os.path.basename(args.db_fa)))

query_dico = os.path.join(args.wd, '{}.dico'.format(os.path.basename(args.query)))
query_simpl_fasta = os.path.join(args.wd, '{}_simplified_id.faa'.format(os.path.basename(args.query)))

net_safe = os.path.join(args.wd, "net_safe")
# this structure will simplify the path management
Path_tuple = namedtuple("Path_tuple", ["db_dico", "db_faa_simpl", "db_faa_src", "db_fmt", "q_dico", "q_faa_simpl",
                                      "split", "blast_out", "edges", "concated_blast", "id", "current_faa_dir", "ban_list",
                                       "net_safe"])

path_tpl = Path_tuple(db_dico=db_dico, db_faa_simpl=db_simpl_fasta, db_faa_src=args.db_fa, db_fmt=metage_database,
                      q_dico=query_dico, q_faa_simpl=query_simpl_fasta, split=split_dir, blast_out=blast_out_dir,
                      edges=edge_dir, concated_blast=concat_dir, id=id_dir, current_faa_dir=current_fasta_dir, ban_list=ban_list,
                      net_safe=net_safe)

db_size, min_size, max_size = manage_fasta_input.main(interest_family=args.query, metagenome=args.db_fa,
						dico_family_interest=query_dico,
						dico_metagenome=db_dico, fasta_small_metage=db_simpl_fasta,
						interest_family_simpl=query_simpl_fasta, min_factor=args.min_size, max_factor=args.max_size)


if args.diamond:
	aligner = diamond_cpu.Diamond("{} makedb".format(args.diamond), diamond_bin=args.diamond, threads=args.th)

elif args.blast_:
	aligner = blast_thread.Blast(make_db_bin=args.mkdb_,
	                             blastp_bin=args.blast_,
	                             fastasplit=args.faa_split,
	                             threads=args.th)

try:
	utils_chain_blast.main_loop_uniq.main_(path_tpl, args, aligner)
except utilitaire.NoSequenceFound:
	pass
except:
	raise


print('Iteration done')

if os.path.exists(os.path.join(args.wd, os.path.join('edges','_edges_'))):
	
	fasta_find = os.path.join(args.wd, 'sequence_found.faa')
	blast_nr = os.path.join(args.wd, 'all_against_nr.blastp')
	
	print('make a fasta with all the sequence get')
	utilitaire.make_fasta_from_edge_dir(edge_dir=edge_dir, out_file=fasta_find, metage_fasta=path_tpl.db_faa_simpl)

	# rename back sequences.
	utilitaire.rename_fasta(path_tpl.db_dico, fasta_find)
	utilitaire.rename_fasta(path_tpl.q_dico, path_tpl.q_faa_simpl)
	renames_edges_files.main_part(dico_db=path_tpl.db_dico, dico_query=path_tpl.q_dico, edges_dir=path_tpl.edges)

	finded_and_base_family = os.path.join(args.wd, 'sequence_found_and_bases.faa')
	
	basename_query = os.path.basename(args.query)

	#utilitaire.copy_files([path_tpl.q_faa_simpl], os.path.join(args.wd, args.query))
	shutil.move(path_tpl.q_faa_simpl, os.path.join(args.wd, args.query))

	utilitaire.copy_files([fasta_find, os.path.join(args.wd, args.query)], finded_and_base_family)


	# blast against nr
	if args.nr_db:
		aligner.align(db=args.nr_db, output=blast_nr, fasta=fasta_find,
	              max_seq_target=10, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr,
	                  blast_out_dir=path_tpl.blast_out, split_dir=path_tpl.split)

	
	print('make db for all against all blast')
	
	finded_all_against_all_db = os.path.join(databse_dir, 'all_finded')
	allvall = os.path.join(args.wd, 'all_against_all.blastp')

	print('make the all against all blast')

	aligner.make_db(input_=finded_and_base_family, output_=finded_all_against_all_db)
	aligner.align(db=finded_all_against_all_db, output=allvall, fasta=finded_and_base_family,
	              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr,
	              blast_out_dir=path_tpl.blast_out, split_dir=path_tpl.split)

	print('done')

utilitaire.remove(db_dico, db_simpl_fasta, query_dico,
					query_simpl_fasta, databse_dir,
					ban_list, id_dir, blast_out_dir, concat_dir, split_dir,
					current_fasta_dir)

print('done')
