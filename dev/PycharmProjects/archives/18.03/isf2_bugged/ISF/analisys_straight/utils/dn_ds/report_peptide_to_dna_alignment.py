#!usr/bin/python3
import argparse
from . import fonction as fonct


#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#


codontable = {
	'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
	'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
	'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
	'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
	'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
	'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
	'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
	'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
	'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
	'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
	'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
	'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
	'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
	'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
	'TAC': 'Y', 'TAT': 'Y', 'TAA': '*', 'TAG': '*',
	'TGC': 'C', 'TGT': 'C', 'TGA': '*', 'TGG': 'W',
}


def assert_start(nuc_seq, prot_seq, log_file, codon_dico=codontable):
	start = 0
	if len(nuc_seq) % 3 != 0:
		start = len(nuc_seq) % 3
	nuc_seq_0 = nuc_seq[start:]

	cpt = 0
	for i in range(0, len(nuc_seq_0), 3):
		try:
			assert codon_dico[nuc_seq_0[i: i+3]] == prot_seq[cpt]
		except:
			with open(log_file, 'a') as log:
				log.write("\n{}, {} , {}, {}, {}, {}, {}\n".format(start, assert_start, i, cpt,
				                                               nuc_seq_0[i: i+3], codon_dico[nuc_seq_0[i: i+3]],
				                                               prot_seq[cpt]))
			raise
		cpt += 1

	return nuc_seq_0



def check_size(prot_seq, nc_seq):
	if len(prot_seq.replace("-", "")) == (len(nc_seq) / 3):
		return True
	else:
		return False


list_codon_stop = set(['TAA', 'TAG', 'TGA'])
def has_stop_codon(nuc, list_codon_stop=list_codon_stop):
	if nuc[-3:] in list_codon_stop:
		return True
	else:
		return False



	
def get_the_dna(nuc_seq, sequence, log_file):

	# sometimes genbank dna extracting sequence do not work properly because
	# someone not use the +1 but the 0 start transciption
	# I need something more solid than waht I have now!
	if has_stop_codon(nuc_seq):
		nuc_seq = nuc_seq[0:-3]
	try:
		if not check_size(prot_seq=sequence, nc_seq=nuc_seq):
			nuc_seq = assert_start(nuc_seq=nuc_seq, prot_seq=sequence.replace("-", ""), log_file=log_file)
	except:

		with open(log_file, 'a') as log:
			log.write("{}\n {}\n {}\n".format(assert_start, nuc_seq, sequence))
		raise

	try:
		assert check_size(prot_seq=sequence, nc_seq=nuc_seq)

	except:
		with open(log_file, 'a') as log:
			log.write("{}\n {}\n {}\n".format(check_size, nuc_seq, sequence))
		raise
	# if stop:
	# 	nuc_seq = nuc_seq[0:-3]
		
	indice_dna, indice_prot = 0, 0
	new_seq = ""

	while True:
		if indice_prot == len(sequence):
			break

		if sequence[indice_prot] == '-':
			new_seq += '-' * 3
			indice_prot += 1
			continue
			
		else:
			new_seq += nuc_seq[indice_dna*3:(indice_dna * 3 + 3)]
			indice_prot += 1
			indice_dna += 1
	
	return new_seq

	
def main(aligment_file, out_file, log_file, fasta_fna_file=None, fna_dico=None):
	

	# assert we have one of these files
	assert (fasta_fna_file or fna_dico) and not (fasta_fna_file and fna_dico)

	#
	if fasta_fna_file:
		dico_nucleic_sequence = {}
	
		with open(fasta_fna_file) as nucl:
	
			for prompt, sequence in fonct.get_seq_one_by_one(nucl):
				dico_nucleic_sequence[prompt] = sequence.strip()
				
	else:
		dico_nucleic_sequence = fna_dico
	
	if out_file:
		with open(aligment_file, 'r') as align, open(out_file, 'w') as out_:
			
			for prompt, sequence in fonct.get_seq_one_by_one(align):
				# get sequences # we want them all so if we threw a key error is nice that point out a problem
				identifiant = prompt
				with open(log_file, 'a') as log:
					log.write("{}\t{}\n".format(identifiant, prompt))

				nuc_seq = dico_nucleic_sequence[identifiant].strip()

				try:
					new_seq = get_the_dna(nuc_seq, sequence, log_file)
				except:
					with open(log_file, 'a') as log:
						log.write("{}\t{}\n".format(identifiant, prompt))
					raise
				out_.write(">{}\n{}\n".format(prompt, fonct.chunck_sequence(new_seq, sz=60)))
	
	else:
		with open(aligment_file, 'r') as align:
			
			for prompt, sequence in fonct.get_seq_one_by_one(align):

				# get sequences # we want them all so if we threw a key error is nice that point out a problem
				identifiant = prompt
				with open(log_file, 'a') as log:
					log.write("{}\t{}\n".format(identifiant, prompt))
				nuc_seq = dico_nucleic_sequence[identifiant]


				# check the size are equivalent take check presence of codon stop
				# return true is ther is stop else false
				stop = check_size(prot_seq=sequence, nc_seq=nuc_seq)
				if stop:
					nuc_seq = nuc_seq[0:-3]
					
				new_seq = get_the_dna(nuc_seq, sequence)

				print('>{}\n{}'.format(prompt, fonct.chunck_sequence(new_seq, sz=60)))

if __name__ == '__main__':
	
	parser = argparse.ArgumentParser(description='')
	parser.add_argument("-align", type=str)
	parser.add_argument("-fasta", type=str)
	parser.add_argument("-out", type=str)
	args = parser.parse_args()
	
	main(aligment_file=args.align, fasta_fna_file=args.fasta, out_file=args.out)
