import os
from urllib import request
import re
import argparse
import copy
import time
import xml.etree.ElementTree as ET
import pickle
import time
import natsort

#
#        Written by Romain Lannes, 2016-2018
#
#        is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#       please report bug to romain.lannes@protonmail.com with "ncbi_annotation BUG report" as object.
#


def index_file(id_file: str, index_file: str):

	start = time.time()
	line_cpt = 1
	print("indexing prot_id file may take some times (only required first time)")
	with open(id_file, "r") as id_file, open(index_file, "w") as index_:

		line = id_file.readline()
		id_ = line.strip().split()[0]
		index_.write("{}\t{}\n".format(id_, "0"))

		while line:

			line_cpt += 1
			if line_cpt % 10000 == 0:  # chunck
				id_ = line.strip().split()[0]
				index_.write("{}\t{}\n".format(id_, id_file.tell()))

			line = id_file.readline()

	print("indexing takes {}".format(time.time() - start))


def read_index_file(index_file: str) -> dict:
	dico = {}
	with open(index_file) as file_:
		for line in file_:
			spt = line.strip().split()
			dico[spt[0]] = int(spt[1])
	return dico


def make_dico_for_index_parsing(index_file, my_set) -> (dict, dict):

	dico_index = read_index_file(index_file)
	set_ref = set(list(dico_index.keys()))

	liste_all = list(set.union(set_ref, my_set))
	liste_all_sorted = natsort.natsorted(liste_all)

	dico_index_prot = {}
	set_prot = set()

	for elem in liste_all_sorted:

		if elem in set_ref:

			if not set_prot:
				current_ref = elem

			else:
				dico_index_prot[current_ref] = set_prot
				set_prot = set()
				current_ref = elem

		else:
			try:
				assert current_ref # first reference must be the first elem of the list
			except:
				print(elem)
				raise
			set_prot.add(elem)
		if set_prot:  # last elem
			dico_index_prot[current_ref] = set_prot

	return dico_index_prot, dico_index


def parse_indexed_taxfile(taxfile_indexed: str, my_set, dico, set_found, index_file):

	dico_parsing, dico_index = make_dico_for_index_parsing(index_file, my_set)
	with open(taxfile_indexed, "r") as input_:
		# key ref, set(prot)
		for key, value in sorted(dico_parsing.items(), key=lambda x: x[1]):   # sorted in index order
			this_index = dico_index[key]  # get file index
			input_.seek(this_index)
			not_all_found = True
			cpt = 0
			line_cpt = 0

			while not_all_found:
				line = input_.readline()
				line_cpt += 1
				if line_cpt % 20000 == 0:
					break

				spt = line.strip().split()
				try:
					if spt[0] in value:
						cpt += 1
						set_found.add(spt[0])

						if spt[2] in dico:
							dico[spt[2]].add(spt[0])

						else:
							dico[spt[2]] = set([spt[0]])

						if cpt == len(value):
							not_all_found = False
				except IndexError:
					print("Warning: ",spt, line, key, this_index, value)
					break
				except:
					raise
	return

reg = re.compile("taxon:(\d+)")

def parse_xml_from_prot_id(file_string, dico_out, set_found):
	tree = ET.fromstring(file_string)
	for Seq in tree:

		prot_id = Seq.find("GBSeq_primary-accession").text
		qualifier_table = Seq.find("GBSeq_feature-table/GBFeature/GBFeature_quals")

		for qual in qualifier_table:
			for sub in qual:
				if "taxon" in sub.text:
					taxon = sub.text.split(':')[-1]
		set_found.add(prot_id)
		dico_out[prot_id] = taxon


def parse_xml_from_tax_id(file_string, dico):

	def add_to_lineage(line, rank, name):

		if rank != "no rank":
			to_add = "{}{{{}}}".format(name, rank)
		else:
			to_add = name
		line.append(to_add)

		pass

	tree = ET.fromstring(file_string)

	for taxa in tree:
		my_lineage = []
		taxid = taxa.find("./TaxId").text
		lineage_ex = taxa.find("./LineageEx")

		for child in lineage_ex:
			rank = child.find("Rank").text
			name = child.find("ScientificName").text

			add_to_lineage(my_lineage, rank=rank, name=name)

		dico[taxid] = "; ".join(my_lineage[::-1])



def get_tax_id(taxfile, my_set, dico, set_found):

	goal = len(my_set)
	cpt = 0
	with open(taxfile) as tax:
		tax.readline()

		for line in tax:
			spt = line.strip().split()

			if spt[0] in my_set:
				cpt += 1
				set_found.add(spt[0])

				if spt[2] in dico:
					dico[spt[2]].add(spt[0])

				else:
					dico[spt[2]] = set([spt[0]])

			if cpt == goal:
				return




def tax_id_by_tax_id(id_, chuncks=200):

	dico_id_lineage = {}
	base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id={}&retmode=xml"
	id_ = list(id_)

	for i in range(0, len(id_), chuncks):
		sub_list = id_[i:i + chuncks]
		url = base_url.format(','.join(sub_list))
		response = request.urlopen(url)
		data = response.read()
		data = data.decode('utf-8')
		parse_xml_from_tax_id(data, dico_id_lineage)

	return dico_id_lineage


def tax_id_by_prot(id_, dico_out, set_found, chuncks=200):
	base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id={}&retmode=xml"
	my_id = list(id_)
	for i in range(0, len(my_id), chuncks):
		sub_list = my_id[i:i + chuncks]
		url = base_url.format(','.join(sub_list))
		response = request.urlopen(url)
		data = response.read()
		data = data.decode('utf-8')
		parse_xml_from_prot_id(data, dico_out, set_found=set_found)
		time.sleep(1)


def get_taxonomic_lineage(dico_resume, dico_final, dico_protid):
	"""

	:param dico_resume: a dico that represent taxonomic tree
	:param dico_final:  a final dict
	:param set_tax_id_found:
	:param: dico_prot_id
	:return:
	"""

	set_not_found = set()
	for id_to_find in dico_protid:

		if id_to_find not in dico_resume:
			set_not_found.add(id_to_find)

		else:
			lineage = dico_resume[id_to_find].dump_lineage(dico_resume)
			id_to_look_for = dico_protid[id_to_find]

			if isinstance(id_to_look_for, list):
				for prot_id in dico_protid[id_to_find]:
					dico_final[prot_id] = lineage
			else:
				dico_final[id_to_look_for] = lineage

	return set_not_found


class TaxonomicalNodes:

	"""
	Represent a taxonomical node

	"""
	def __init__(self, id_, rank_name, parent=None, child=None, name=None):

		self.id_ = id_
		self.name = name
		self.parent = parent
		self.child = child
		self.rank_name = rank_name

	def add_name(self, name):
		self.name = name

	def set_child(self, childid):
		self.child = childid

	def get_name_and_rank(self, no_rank=False):
		"""

		:param no_rank: is set to true will add no rank value
		:return:
		"""
		if self.rank_name != "no rank":
			return "{}{{{}}}".format(self.name, self.rank_name)
		else:
			return self.name


	def dump_lineage(self, dico):

		liste_lineage = [self.get_name_and_rank()]
		parent = self.parent

		while parent:

			my_parent_node = dico[parent]
			liste_lineage.append(my_parent_node.get_name_and_rank())
			parent = my_parent_node.parent

		liste_lineage = liste_lineage[::-1]
		return (self.id_, "; ".join(liste_lineage))


def build_taxo_tree(node_file, full_name_lineages):
	"""

	:param node_file:
	:param full_name_lineages:
	:return:
	"""
	dico_id_node = {}
	with open(node_file) as file_:
		for line in file_:

			spt = line.strip().split("|")[0:3]  # we don't need the rest
			parent = spt[1].strip()
			selfid = spt[0].strip()

			if selfid == parent:
				parent = None  # no self loop

			my_node = TaxonomicalNodes(id_=selfid, rank_name=spt[2].strip(), parent=parent)
			dico_id_node[selfid] = my_node

			if parent in dico_id_node:	 # this node is child
				parent_node = dico_id_node[parent]
				parent_node.set_child(selfid)

	# now we look and add the actual names
	with open(full_name_lineages) as file_:
		for line in file_:
			spt = line.strip().split('|')
			name = spt[1].strip()
			selfid = spt[0].strip()
			dico_id_node[selfid].add_name(name)

	return dico_id_node


def pickle_(dump_dir):

	pickle_file = os.path.join(dump_dir, 'pickle_resume.plk')
	print(pickle_file)
	if os.path.isfile(pickle_file):
		with open(pickle_file, 'rb') as handle:
			dico = pickle.load(handle)
		return dico

	else:

		# build the db:
		dico = build_taxo_tree(node_file=os.path.join(dump_dir, "nodes.dmp"),
					full_name_lineages=os.path.join(dump_dir, "fullnamelineage.dmp"))

		# Store data (serialize)
		with open(pickle_file, 'wb') as handle:
			pickle.dump(dico, handle, protocol=pickle.HIGHEST_PROTOCOL)

		return dico


def prot_id_part(dump_dir, set_identifier, set_found, dico_id_tax_id, ask_ncbi=False, indexing=False):
	"""

	:param dump_dir:
	:param set_identifier:
	:param ask_ncbi:
	:param indexing:
	:return:
	"""
	start_time = time.time()
	reg = re.compile("^\w{4}_?\w?$")
	set_identifier = copy.copy(set_identifier)

	prot_id_file = os.path.join(dump_dir, "prot.accession2taxid")
	pdb_tax_file = os.path.join(dump_dir, "pdb.accession2taxid")

	#  full_name_file = dump_dir.rstrip('/') + "/fullnamelineage.dmp"

	pdb_set = {value for value in set_identifier if reg.match(value)}

	if len(pdb_set) > 0:
		prot_set = {value for value in set_identifier if not reg.match(value)}

	else:
		prot_set = set_identifier

	# removing version number
	prot_set = {elem.split('.')[0] for elem in prot_set}
	dead_tax = os.path.join(dump_dir, "dead_prot.accession2taxid")
	if not indexing:
		if pdb_set:

			get_tax_id(taxfile=pdb_tax_file, my_set=pdb_set,
					   dico=dico_id_tax_id, set_found=set_found)

		get_tax_id(taxfile=prot_id_file, my_set=prot_set,
				   dico=dico_id_tax_id, set_found=set_found)

		if (len(set_found) < len(prot_set) + len(pdb_set))  and os.path.isfile(dead_tax):

			if os.path.isfile(dead_tax):
				get_tax_id(taxfile=dead_tax, my_set=prot_set,
				   dico=dico_id_tax_id, set_found=set_found)

	else:
		if pdb_set:
			pdb_index_file = os.path.join(dump_dir, "pdb_to_acc.index")

			if not os.path.exists(pdb_index_file):
				index_file(id_file=pdb_tax_file, index_file=pdb_index_file)
			parse_indexed_taxfile(taxfile_indexed=pdb_tax_file, my_set=pdb_set, dico=dico_id_tax_id,
								  set_found=set_found, index_file=pdb_index_file)

		prot_index_file = os.path.join(dump_dir, "prot_to_acc.index")

		if not os.path.exists(prot_index_file):
			index_file(id_file=prot_id_file, index_file=prot_index_file)
		parse_indexed_taxfile(taxfile_indexed=prot_id_file, my_set=prot_set, dico=dico_id_tax_id,
							  set_found=set_found, index_file=prot_index_file)

		if (len(set_found) < len(prot_set) + len(pdb_set)) and os.path.isfile(dead_tax):
			dead_index_file = os.path.join(dump_dir, "dead_to_acc.index")

			if not os.path.exists(dead_index_file):
				index_file(id_file=dead_tax, index_file=dead_index_file)

			if os.path.isfile(dead_tax):
				get_tax_id(taxfile=dead_tax, my_set=prot_set,
				           dico=dico_id_tax_id, set_found=set_found)

	set_id_base = set.union(prot_set, pdb_set)

	print("takes {} secondes to look for taxonomical id\n\
		  {} find over {}".format(time.time() - start_time, len(set_found) , len(prot_set) + len(pdb_set) ))

	unfound_prot = set.difference(set_id_base, set_found)

	print("{} correspondace prot - tax id not found\n".format(len(unfound_prot)))

	if ask_ncbi and len(unfound_prot) > 0:
			prot_id_ncbi_start = time.time()
			list_unfound = list(unfound_prot)
			tax_id_by_prot(unfound_prot, dico_id_tax_id, set_found=set_found)
			print("takes {} secondes to look for unfound prot id with ncbi".format(time.time() - prot_id_ncbi_start))

	return set_id_base

def main(dump_dir, set_identifier, ask_ncbi=False, indexing=False, from_tax_id=False):
	"""

	:param dump_files: a path to ncbi taxonomy  dump directory with at least fullnamelineage.dmp prot.accession2taxid  pdb.accession2taxid
	:param set_identifier: a set of identifier to recover
	:param ask_ncbi: some times you won't be able to found a prot_id in the dump file and associate a taxid
	if this option is set to true we will ask ncbi using efetch
	:return:
	a dictionnary[identifier] -> (tax_id, taxonomy)
	"""

	start_time = time.time()
	dico_id_tax_id = {}
	set_found = set()
	pickle_time = time.time()
	dico_resume = pickle_(dump_dir)
	print("take {} seconde to construct phylogeny tree".format(time.time() - pickle_time))
	dico_final = {}
	set_tax_id_found = set()
	main_time = time.time()


	if not from_tax_id:
		# parsing prot_file
		set_id_base = prot_id_part(dump_dir, set_identifier, set_found, dico_id_tax_id, ask_ncbi, indexing)


	else:
		dico_id_tax_id = dict((elem, elem) for elem in list(set_identifier))
		set_id_base = set_identifier


	unfound_tax_id = get_taxonomic_lineage(dico_resume=dico_resume, dico_protid=dico_id_tax_id,
						  dico_final=dico_final)

	print("takes {} secondes to find lineages from taxonomical id\n".format(time.time() - main_time))

	unfound_prot = set.difference(set_id_base, set_found)
	print("{} correspondace tax id - lineage not found".format(len(unfound_tax_id)))


	if ask_ncbi and len(unfound_tax_id) > 0:
		tax_id_start = time.time()
		dico_tax_id_lineage = tax_id_by_tax_id(id_=unfound_tax_id)
		for tax_id, lineage in dico_tax_id_lineage.items():

			for prot_id in dico_tax_id_lineage[tax_id]:
				dico_final[prot_id] = (tax_id, lineage)

		print("takes {} secondes to look for unfound tax id with ncbi".format(time.time() - tax_id_start))

	return dico_final, set_id_base

	# else:
	# 	pickle_time = time.time()
	# 	dico_resume = pickle_(dump_dir)
	# 	print("take {} seconde to construct phylogeny tree".format(time.time() - pickle_time))
	#
	# 	dico_id = dict((elem, elem) for elem in list(set_identifier))
	#

def check_cover(split_line, cover_th):

	qcov = (int(split_line[6]) - int(split_line[5]) + 1) / int(split_line[9])
	scov = (int(split_line[8]) - int(split_line[7]) + 1) / int(split_line[10])

	if min([qcov, scov]) >= cover_th:
		return True
	else:
		return False


def annotated(blast_nr, dump_dir, dico_node,
		                    cover_threshold, pid_threshold, indexed, ask_ncbi):

	set_found_prot = set()
	dico_prot_node = {}

	with open(blast_nr) as b:
		set_id = {}

		for line in b:
			spt = line.strip().split()

			if check_cover(spt, cover_threshold) and spt[3] >= pid_threshold:
				prot_match = spt[1]
				dico_node[spt[0]].nearest_ncbi_percent_id = spt[3]
				dico_node[spt[0]].nearest_ncbi_prot = prot_match
				set_id.add(spt[1])

				if prot_match in dico_prot_node:
					dico_prot_node[prot_match].append(dico_node[spt[0]])

				else:
					dico_prot_node[prot_match] = [dico_node[spt[0]]]

	if dump_dir:
		dico, set_id_base = main(dump_dir=dump_dir, set_identifier=set_id, ask_ncbi=ask_ncbi, indexing=indexed)
		#(tax_id, lineage)

		for key, values in dico.items():

			for env_prot in dico_prot_node[key]:
				node_instance = dico_node[env_prot]
				node_instance.phylogeny = values


# 	with open(args.id_file) as file_:
# 		for line in file_:
# 			set_id.add(line.strip())
# 	print("takes {} seconde(s) to read your file".format(time.time() - time_start_read))


#
# if __name__ == "__main__":
#
# 	parse = argparse.ArgumentParser(description="take a list of ncbi prot id return taxonomic annotation,"
# 												" For NCBI ID accession number for pdb format full, the first time you use this script,"
# 												" It will takes some times to creattes special files in order to "
# 												" speed things up after!, pdb id should be full ncbi prot id may be primary or accession version"
# 												"We use pickle and indexing to make things faster if you change file in directory pls remove index and pickel file"
# 												"because indexing imply a depedency is optional also prot_id file should be sorted "
# 												"(by natural ordering -V option)if this is to hard due to large size please see this post https://www.linuxquestions.org/questions/linux-general-1/bash-sort-uses-too-much-memory-4175419612/"
# 												"dependecies: natsort-5.4.1 (instalable by pip)")
# 	parse.add_argument("-id_file", required=True, help="file with ncbi prot id one by line")
# 	parse.add_argument("-dump_dir", required=True, help="a path to ncbi taxonomy  dump directory with at least fullnamelineage.dmp prot.accession2taxid  pdb.accession2taxid")
# 	parse.add_argument("-out_file", required=True, help="outfile")
# 	parse.add_argument("-ask_ncbi", action="store_true", help="if set will send request to ncbi")
# 	parse.add_argument("-unfound", help="if set write unfound id in this file")
# 	parse.add_argument("-indexing", action="store_true")
# 	parse.add_argument("-tax_id", action="store_true")
# 	args = parse.parse_args()
#
# 	#  look for pickle else create it and return a dico
#
#
# 	time_start_read = time.time()
# 	set_id = set()
# 	with open(args.id_file) as file_:
# 		for line in file_:
# 			set_id.add(line.strip())
# 	print("takes {} seconde(s) to read your file".format(time.time() - time_start_read))
#
# 	ask_ncbi = False
# 	if args.ask_ncbi:
# 		ask_ncbi = True
#
# 	dico, set_id_base = main(dump_dir=args.dump_dir, set_identifier=set_id, ask_ncbi=ask_ncbi, indexing=args.indexing,
# 	                         from_tax_id=args.tax_id)
#
# 	with open(args.out_file, "w") as out_file:
# 		for key, value in dico.items():
# 			out_file.write("{}\t{}\t{}\n".format(key, value[0], value[1]))
#
# 	set_find = set(list(dico.keys()))
# 	if args.unfound:
# 		unfound = set.difference(set_id_base, set_find)
# 		with open(args.unfound, 'w') as file_:
# 			for elem in unfound:
# 				file_.write("{}\n".format(elem))
#
# 	print("done")
