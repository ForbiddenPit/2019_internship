import argparse
import subprocess
import os
import re
from . import report_peptide_to_dna_alignment, fonction



#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#


def write_ctl_file(ctl_file, seq_file, outfile, verbose=1, icode=0, weighting=0, commonf3x4=0,
                   runmode=-2, codonfreq=2):
	
	with open(ctl_file, 'w') as file_:
		file_.write("seqfile = {}\n".format(seq_file))
		file_.write("outfile = {}\n".format(outfile))
		file_.write("verbose = {}\n".format(verbose))
		file_.write("icode = {}\n".format(icode))
		file_.write("weighting = {}\n".format(weighting))
		file_.write("commonf3x4 = {}\n".format(commonf3x4))


def parse_yn00_file(yn_file):

	rega = re.compile("\s?\(A\)")
	reg = re.compile("\s?\(B\)")
	reg2 = re.compile("\s?\(C\)")

	bol1, bol2 = False, False
	dico = {}
	cpt = 1
	bol_cpt = 0
	somme = 0
	number = 0
	with open(yn_file) as yn:

		for line in yn:

			if reg2.match(line):
				break

			if reg.match(line):
				bol2 = True
				bol_cpt = 0
				bol1 = False

			if bol1 and line.split():

				bol_cpt += 1
				if bol_cpt < 4:
					continue
				dico[str(cpt)] = line.split()[0]
				cpt += 1

			elif not line:
				bol1 = False

			if rega.match(line):
				bol1 = True

			if bol2 and line.split():
				bol_cpt += 1
				if bol_cpt < 5:
					continue

				spt = line.strip().split()
				somme += float(spt[6])
				number += 1

	return (somme / number)


def align(in_file, out_file, align_soft=None):
	
	if align_soft.split('/')[-1] == 'mafft':
		child = subprocess.Popen("{} --auto  --reorder {} > {}".format(align_soft, in_file, out_file),
		                         shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		child.wait()
		
	elif align_soft.split('/')[-1] == 'muscle':
		child = subprocess.Popen("{} -in {} -out {}".format(align_soft, in_file, out_file),
		                         shell=True, stdout=subprocess.DEVNULL,  stderr=subprocess.DEVNULL)
		child.wait()


def clean_stop(file_, new_file, prot=True):

	if not prot:
		list_codon_stop = set(['TAA', 'TAG', 'TGA', "taa", "tag", "tga"])
		with open(file_) as in_, open(new_file, "w") as out:

			for prompt, seq in fonction.get_seq_one_by_one(in_):

				if seq[-3:] in list_codon_stop:
					out.write(">{}\n{}\n".format(prompt, seq[:-3]))
				else:
					out.write(">{}\n{}\n".format(prompt, seq))

	else:
		with open(file_) as in_, open(new_file, "w") as out:

			for prompt, seq in fonction.get_seq_one_by_one(in_):

				if seq[-1:] == '*':
					out.write(">{}\n{}\n".format(prompt, seq[:-1]))
				else:
					out.write(">{}\n{}\n".format(prompt, seq))


def main(tuple_args):
	proteic_file, dna_file, align_soft, dna_align_dir, yn00, log_file = tuple_args

	# dna_align_dir = dna_align_dir.rstrip('/') + '/'
	# os.makedirs(dna_align_dir)

	prot_align = dna_align_dir + "pro_align"
	temp_ctl_file = dna_align_dir + 'yn00.ctl'
	tempo_yn_output = dna_align_dir + "yn00.out"
	new_prot_file = dna_align_dir + 'prot_clean.faa'
	new_dna_file = dna_align_dir + 'dna_clean.fna'
	clean_stop(file_=proteic_file, new_file=new_prot_file)
	clean_stop(file_=dna_file, new_file=new_dna_file, prot=False)
	proteic_file = new_prot_file
	dna_file = new_dna_file
	dna_file_paml = new_dna_file + ".paml"

	dna_align_file = dna_align_dir + "{}_msa.fna".format(os.path.basename(proteic_file.split('.')[0]))
	
	# align prot
	align(in_file=proteic_file, out_file=prot_align, align_soft=align_soft)

	try:
		# transform that in dna
		report_peptide_to_dna_alignment.main(aligment_file=prot_align,
		                                     out_file=dna_align_file,
		                                     fasta_fna_file=dna_file,
											 fna_dico=None, log_file=log_file)
	except:
		for file in os.listdir(dna_align_dir):
			os.remove(dna_align_dir + file)
		os.rmdir(dna_align_dir)
		raise
	## warning: trimal is use to convert format not to trim msa!
	cmd = "trimal -phylip_paml -in {} -out {}".format(dna_align_file, dna_file_paml)

	child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL )
	child.wait()


	write_ctl_file(ctl_file=temp_ctl_file, seq_file=dna_file_paml, outfile=tempo_yn_output )

	cmd = "{} {}".format(yn00, temp_ctl_file)


	child = subprocess.Popen(cmd, shell=True, stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
	child.wait()

	child = subprocess.Popen("rm 2YN.dS 2YN.dN 2YN.t rst rub rst1", shell=True, stderr=subprocess.DEVNULL)
	child.wait()

	value = parse_yn00_file(tempo_yn_output)

	for file in os.listdir(dna_align_dir):
		os.remove(dna_align_dir + file)
	os.rmdir(dna_align_dir)

	return value
	
	
if __name__ == '__main__':
	parse = argparse.ArgumentParser()
	
	parse.add_argument("-proteic_file", type=str)
	parse.add_argument("-dna_file")
	parse.add_argument("-align_soft")
	parse.add_argument("-dna_align_dir")
	parse.add_argument("-yn00", default="yn00")

	args = parse.parse_args()
	tuple_args = (args.proteic_file, args.dna_file, args.align_soft, args.dna_align_dir, args.yn00)
	print(str(main(tuple_args)))
	

