import os
from urllib import request
import re
import argparse
import copy
import time
import xml.etree.ElementTree as ET
import pickle
import time

#
#        Written by Romain Lannes, 2016-2018
#
#        is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#       please report bug to romain.lannes@protonmail.com with "ncbi_annotation BUG report" as object.
#
reg = re.compile("taxon:(\d+)")
def parse_xml_from_prot_id(file_string, dico_out):
	tree = ET.fromstring(file_string)
	#root = tree.getroot()
	taxon = reg.search(file_string).group(1)

	for Seq in tree:

		v = Seq.findall("./GBSeq_feature-table/GBFeature/GBFeature_quals/GBQualifier/GBQualifier_value")

		# for sub in v:
		# 	if sub.text.startswith("taxon"):
		# 		taxon = sub.text.split(':')[-1]


		v = Seq.findall("./GBSeq_primary-accession")
		id_ = v[0].text

		# v = Seq.findall("./GBSeq_organism")
		# species = v[0].text
		#
		# v = Seq.findall("./GBSeq_taxonomy")
		# taxonomy = v[0].text


		# taxo = "{}; {}".format(";".join(taxonomy.split(";")), species)
		if not taxon:
			break

		dico_out[id_] = taxon


def parse_xml_from_tax_id(file_string, dico):

	def add_to_lineage(line, rank, name):

		if rank != "no rank":
			to_add = "{}{{{}}}".format(name, rank)
		else:
			to_add = name
		line.append(to_add)

		pass

	tree = ET.fromstring(file_string)

	for taxa in tree:
		my_lineage = []
		taxid = taxa.find("./TaxId").text
		lineage_ex = taxa.find("./LineageEx")

		for child in lineage_ex:
			rank = child.find("Rank").text
			name = child.find("ScientificName").text

			add_to_lineage(my_lineage, rank=rank, name=name)

		dico[taxid] = "; ".join(my_lineage[::-1])



def get_tax_id(taxfile, my_set, dico, set_found):

	goal = len(my_set)
	cpt = 0
	with open(taxfile) as tax:
		tax.readline()

		for line in tax:
			spt = line.strip().split()

			if spt[0] in my_set:
				#print(set_found)
				cpt += 1
				set_found.add(spt[0])

				if spt[2] in dico:
					dico[spt[2]].add(spt[0])

				else:
					dico[spt[2]] = set([spt[0]])

			if cpt == goal:
				return


def tax_id_by_tax_id(id_, chuncks=200):

	dico_id_lineage = {}
	base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id={}&retmode=xml"
	id_ = list(id_)

	for i in range(0, len(id_), chuncks):
		sub_list = id_[i:i + chuncks]
		url = base_url.format(','.join(sub_list))
		response = request.urlopen(url)
		data = response.read()
		data = data.decode('utf-8')
		parse_xml_from_tax_id(data, dico_id_lineage)

	return dico_id_lineage


def tax_id_by_prot(id_, dico_out, chuncks=200):
	base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id={}&retmode=xml"
	my_id = list(id_)
	for i in range(0, len(my_id), chuncks):
		sub_list = my_id[i:i + chuncks]
		url = base_url.format(','.join(sub_list))
		response = request.urlopen(url)
		data = response.read()
		data = data.decode('utf-8')
		parse_xml_from_prot_id(data, dico_out)
		time.sleep(1)


def get_taxonomic_lineage(dico_resume, dico_final, dico_protid):
	"""

	:param dico_resume: a dico that represent taxonomic tree
	:param dico_final:  a final dict
	:param set_tax_id_found:
	:param: dico_prot_id
	:return:
	"""

	set_not_found = set()
	for id_to_find in dico_protid:

		if id_to_find not in dico_resume:
			set_not_found.add(id_to_find)

		else:
			lineage = dico_resume[id_to_find].dump_lineage(dico_resume)

			for prot_id in dico_protid[id_to_find]:
				dico_final[prot_id] = lineage

	return set_not_found


class TaxonomicalNodes:

	"""
	Represent a taxonomical node

	"""
	def __init__(self, id_, rank_name, parent=None, child=None, name=None):

		self.id_ = id_
		self.name = name
		self.parent = parent
		self.child = child
		self.rank_name = rank_name

	def add_name(self, name):
		self.name = name

	def set_child(self, childid):
		self.child = childid

	def get_name_and_rank(self, no_rank=False):
		"""

		:param no_rank: is set to true will add no rank value
		:return:
		"""
		if self.rank_name != "no rank":
			return "{}{{{}}}".format(self.name, self.rank_name)
		else:
			return self.name


	def dump_lineage(self, dico):

		liste_lineage = [self.get_name_and_rank()]
		parent = self.parent

		while parent:

			my_parent_node = dico[parent]
			liste_lineage.append(my_parent_node.get_name_and_rank())
			parent = my_parent_node.parent

		liste_lineage = liste_lineage[::-1]
		return (self.id_, "; ".join(liste_lineage))


def build_taxo_tree(node_file, full_name_lineages):
	"""

	:param node_file:
	:param full_name_lineages:
	:return:
	"""
	dico_id_node = {}
	with open(node_file) as file_:

		for line in file_:

			spt = line.strip().split("|")[0:3]  # we don't need the rest
			parent = spt[1].strip()
			selfid = spt[0].strip()

			if selfid == parent:
				parent = None  # no self loop

			my_node = TaxonomicalNodes(id_=selfid, rank_name=spt[2].strip(), parent=parent)
			dico_id_node[selfid] = my_node

			if parent in dico_id_node:	 # this node is child
				parent_node = dico_id_node[parent]
				parent_node.set_child(selfid)

	# now we look and add the actual names
	with open(full_name_lineages) as file_:
		for line in file_:
			spt = line.strip().split('|')
			name = spt[1].strip()
			selfid = spt[0].strip()
			dico_id_node[selfid].add_name(name)

	return dico_id_node


def pickle_(dump_dir):

	pickle_file = os.path.join(dump_dir, 'pickle_resume.pickle')

	if os.path.isfile(pickle_file):
		with open(pickle_file, 'rb') as handle:
			dico = pickle.load(handle)
		return dico

	else:

		# build the db:
		dico = build_taxo_tree(node_file=os.path.join(dump_dir, "nodes.dmp"),
	                full_name_lineages=os.path.join(dump_dir, "fullnamelineage.dmp"))

		# Store data (serialize)
		with open(pickle_file, 'wb') as handle:
			pickle.dump(dico, handle, protocol=pickle.HIGHEST_PROTOCOL)

		return dico


def main(dump_dir, set_identifier, ask_ncbi=False):
	"""

	:param dump_files: a path to ncbi taxonomy  dump directory with at least fullnamelineage.dmp prot.accession2taxid  pdb.accession2taxid
	:param set_identifier: a set of identifier to recover
	:param ask_ncbi: some times you won't be able to found a prot_id in the dump file and associate a taxid
	if this option is set to true we will ask ncbi using efetch
	:return:
	a dictionnary[identifier] -> (tax_id, taxonomy)
	"""
	start_time = time.time()

	reg = re.compile("^\w{4}_?\w?$")
	set_identifier = copy.copy(set_identifier)

	prot_id_file = os.path.join(dump_dir, "prot.accession2taxid")
	pdb_tax_file = os.path.join(dump_dir, "pdb.accession2taxid")
	#  full_name_file = dump_dir.rstrip('/') + "/fullnamelineage.dmp"

	dico_id_tax_id = {}
	set_found = set()

	pdb_set = {value for value in set_identifier if reg.match(value)}

	if len(pdb_set) > 0:
		prot_set = {value for value in set_identifier if not reg.match(value)}

	else:
		prot_set = set_identifier

	# removing version number
	prot_set = {elem.split('.')[0] for elem in prot_set}
	if pdb_set:

		get_tax_id(taxfile=pdb_tax_file, my_set=pdb_set,
				   dico=dico_id_tax_id, set_found=set_found)

	get_tax_id(taxfile=prot_id_file, my_set=prot_set,
			   dico=dico_id_tax_id, set_found=set_found)

	set_id_base = set.union(prot_set, pdb_set)

	print("takes {} secondes to look for taxonomical id\n\
	      {} find over {}".format(time.time() - start_time, len(set_found) , len(prot_set) + len(pdb_set) ))

	unfound_prot = set.difference(set_id_base, set_found)

	print("{} correspondace prot - tax id not found\n".format(len(unfound_prot)))

	if ask_ncbi and len(unfound_prot) > 0:
			prot_id_ncbi_start = time.time()
			list_unfound = list(unfound_prot)
			tax_id_by_prot(unfound_prot, dico_id_tax_id)
			print("takes {} secondes to look for unfound prot id with ncbi".format(time.time() - prot_id_ncbi_start))


	dico_resume = pickle_(dump_dir)

	dico_final = {}
	set_tax_id_found = set()

	main_time = time.time()

	unfound_tax_id = get_taxonomic_lineage(dico_resume=dico_resume, dico_protid=dico_id_tax_id,
	                      dico_final=dico_final)

	print("takes {} secondes to find lineages from taxonomical id\n".format(time.time() - main_time))

	unfound_prot = set.difference(set_id_base, set_found)
	print("{} correspondace tax id - lineage not found", len(unfound_tax_id))


	if ask_ncbi and len(unfound_tax_id) > 0:
		tax_id_start = time.time()
		dico_tax_id_lineage = tax_id_by_tax_id(id_=unfound_tax_id)
		print(dico_tax_id_lineage)
		for tax_id, lineage in dico_tax_id_lineage.items():

			for prot_id in dico_tax_id_lineage[tax_id]:
				dico_final[prot_id] = (tax_id, lineage)

		print("takes {} secondes to look for unfound tax id with ncbi".format(time.time() - tax_id_start))

	return dico_final, set_id_base


if __name__ == "__main__":

	parse = argparse.ArgumentParser(description="take a list of ncbi prot id return taxonomic annotation,"
	                                            " For NCBI ID accession number for pdb format full, the first time you use this script,"
	                                            " It will takes some times to creattes special files in order to "
	                                            " speed things up after!, pdb id should be full ncbi prot id may be primary or accession version")
	parse.add_argument("-id_file", required=True, help="file with ncbi prot id one by line")
	parse.add_argument("-dump_dir", required=True, help="a path to ncbi taxonomy  dump directory with at least fullnamelineage.dmp prot.accession2taxid  pdb.accession2taxid")
	parse.add_argument("-out_file", required=True, help="outfile")
	parse.add_argument("-ask_ncbi", action="store_true", help="if set will send request to ncbi")
	parse.add_argument("-unfound", help="if set write unfound id in this file")
	args = parse.parse_args()

	#  look for pickle else create it and return a dico


	time_start_read = time.time()
	set_id = set()
	with open(args.id_file) as file_:
		for line in file_:
			set_id.add(line.strip())
	print("takes {} seconde(s) to read your file".format(time.time() - time_start_read))

	ask_ncbi = False
	if args.ask_ncbi:
		ask_ncbi = True


	dico, set_id_base = main(dump_dir=args.dump_dir, set_identifier=set_id, ask_ncbi=ask_ncbi)

	with open(args.out_file, "w") as out_file:
		for key, value in dico.items():
			out_file.write("{}\t{}\t{}\n".format(key, value[0], value[1]))

	set_find = set(list(dico.keys()))
	if args.unfound:
		unfound = set.difference(set_id_base, set_find)
		with open(args.unfound, 'w') as file_:
			for elem in unfound:
				file_.write("{}\n".format(elem))

	print("done")
