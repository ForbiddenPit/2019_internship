# First ISF launch: 18/03
Needed to debug some python code: version was too old. Results show a list of query sequences found in the reference dataset, according to the BLAST scoring. I will need to change BLAST and integrate HMM profiling:

- HMMER (hammer) will be used for this

# Debugging:
Some things have to be changed in the code, if not already done by now:

- File copy using wrong filenames (basename was required)
- ID using in `analysis_main.py` doesn't work with any sequence ID type: if there are spaces with a full name, the ID is false. This needs to be at least stated in the README, if not corrected. 