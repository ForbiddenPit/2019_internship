from utils_chain_blast import diamond_cpu, blast_thread, cover_to_blast
from utils_chain_blast.utils import utilitaire
import subprocess
import os

import sys

def main_(path_tpl, args, aligner):



	ban_set = set()
	# making the env db
	aligner.make_db(input_=path_tpl.db_faa_simpl, output_=path_tpl.db_fmt)

	print('Alignement of all query against all query')
	temp_for_ban  = args.wd + '/temporary_file_dmd_no_use'
	#  it is interesting to known connectivity of our querys
	#  so we blast them against them
	first_blast_dir = args.wd + '/first_blast_dir'
	try:
		os.mkdir(first_blast_dir)
	except FileExistsError:
		print("Warning: file : {} already exist".format(first_blast_dir))
	except:
		raise

	first_blast_dir += '/'
	first_db = first_blast_dir+'db'
	first_blast_out = path_tpl.concated_blast+'_0_first'
	edges_find = set()

	# make database
	aligner.make_db(input_=path_tpl.q_faa_simpl, output_=first_db)
	# align

	aligner.align(db=first_db, output=first_blast_out, fasta=path_tpl.q_faa_simpl,
	              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr,
	              blast_out_dir= path_tpl.blast_out, split_dir=path_tpl.split)
	# filter by eval thr mainly for blast! for diamond is just pass
	aligner.clean(align_file=first_blast_out, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)

	if not os.path.exists(first_blast_out) or os.stat(first_blast_out).st_size == 0:
		print("Warning: your gene family is not a family")

	else:

		first_edges = path_tpl.edges + '_edges_1'

		with open(first_blast_out, 'r') as blast_out:

			for line in blast_out:
				splited = line.strip().split('\t')

				if splited[0] == splited[1]:
					continue

				with open(first_edges, 'a') as edges_f:
					to_write = splited[0] + '\t' + '\t'.join(splited[2:]) + '\t' + splited[1] + '\n'
					edges_f.write(to_write)

	#child = subprocess.Popen('rm {}'.format(first_blast_out), shell=True)   # no more needed information extracted
	#child.wait()


	# now the main loop
	print('Start alignment of query against db')
	liste_run_find = [(0, 0)]  # a liste containing several tuple with number: run(s) , sequence(s) find
	run_number = 1
	edges_file = path_tpl.edges + '_edges_'

	while run_number <= args.run:
		print("run number : ", run_number)

		try:
			print('doing the alignment')
			current_blast = path_tpl.concated_blast + '_' + str(run_number)
			if run_number == 1:
				aligner.align(db=path_tpl.db_fmt, output=current_blast, fasta=path_tpl.q_faa_simpl,
	              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr,
	                          blast_out_dir=path_tpl.blast_out, split_dir=path_tpl.split)

			else:
				actual_query = path_tpl.current_faa_dir + str(run_number - 1) + '_fasta.fa'
				aligner.align(db=path_tpl.db_fmt, output=current_blast, fasta=actual_query,
				              max_seq_target=1000000, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr,
				              blast_out_dir=path_tpl.blast_out, split_dir=path_tpl.split)

				aligner.filter_by_ban_list(current_blast, ban_set, temp_file=temp_for_ban)
		except:
			print(actual_query)
			print(run_number)
			raise

		try:
			print("cleaning blast by coverage and id percent")
			aligner.clean(align_file=current_blast, cover=args.cov_thr, eval_=args.eval_thr, id=args.pident_thr)

			# check empty file
			if os.stat(current_blast).st_size == 0:
				raise utilitaire.NoSequenceFound("No sequences found")

		except:
			if run_number == 1:
				print("no homologous sequences were found.")
			else:
				print("\n\nlast iteration return empty file, considere the program ended, \
	if you are not satisfied try changing the parameter")
			raise utilitaire.NoSequenceFound("No sequences found")



		print("get edges from blast")
		# now we should look to overlap
		# and add corresponding sequence to the results file /edges/_edges_ and delete blast
		cover_to_blast.manages_edges_file(edges_file, current_blast, args.test_all_chain, args.cov_thr)


		print("extract new sequence and make the corresponding fasta")

		# get the sequence corresponding to blast output
		try:
			utilitaire.extract_seq(edges=edges_file, db_fa=path_tpl.db_faa_simpl,
						output_fa=path_tpl.current_faa_dir + str(run_number) + '_fasta.fa',
						output_seq=os.path.join(path_tpl.id, 'idToget' + str(run_number)), run_=run_number)
		except utilitaire.NoSequenceFound:
			raise
		except:
			raise

		edges_find = utilitaire.update_edges_find(file_=edges_file, run_=run_number, id_get=edges_find)
		liste_run_find.append((run_number, len(edges_find)))

		print('run_number, totalsequencefind : ', liste_run_find)


		if run_number >= 2:
				print("adding edge to ban list")
				child = subprocess.Popen('cat {} >> {}'.format(os.path.join(path_tpl.id, 'idToget' + str(run_number - 1)),
				                                               path_tpl.ban_list), shell=True)
				child.wait()

				id_get = subprocess.check_output("cat {}".format(os.path.join(path_tpl.id, 'idToget' + str(run_number - 1))),
				                                 shell=True).decode("utf-8").split("\n")
				ban_set.update(id_get)

				# removing OLD idtOGET
				utilitaire.remove(os.path.join(path_tpl.id, 'idToget' + str(run_number - 1)))

		utilitaire.remove(os.path.join(path_tpl.current_faa_dir, '/' + str(run_number - 1) + "_fasta.fa"))
		#child = subprocess.Popen(['rm', "{}_fasta.fa".format(path_tpl.current_faa_dir + '/' + str(run_number - 1))])
		#child.wait()
		run_number += 1

	return aligner