#/usr/local/bin/python3

#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import subprocess
import os

def line_generator(line_spt, reverse=False):
	""" input a line of the edges files allow to iterate over this line step by step"""
	# because I love generator
	# generator is beautiful
	# generator if life

	# input a line of the edges files:
	# allow to iterate over this line step by step

	if reverse:

		indice = 1

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[-indice], line_spt[-indice - 10].strip()
			if indice - 1 == 0:
				midle = line_spt[indice - 11: - 1]
			else:
				midle = line_spt[indice - 10:indice - 1]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)

	else:

		indice = 0

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[indice], line_spt[indice + 10].strip()
			midle = line_spt[indice + 1:indice + 10]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)


def construct_dictionnary(dict_files):
	
	dico = {}
	with open(dict_files) as dict_fil:
		
		for line in dict_fil:
			
			spt = line.strip().split('\t')
			dico[spt[1]] = spt[0]
			
	return dico
			
			
		

def rewrite_edges(dico_, files, val, dico2 = None):
	
	files =files.replace("//", "/")
	
	with open(files, 'r') as int_f, open("{}_.tmp".format(files), 'w') as tmp:
		
		if val == 0:
			
			for line in int_f:
				this_line = []
				
				for id_tuple, midle, round in line_generator(line.strip().split('\t')):
					
					this_line.append(dico_[id_tuple[0]])
					this_line.extend(midle)
					this_line.append(dico_[id_tuple[1]])
				
				tmp.write('\t'.join(this_line) + '\n')
					
		else:
			
			for line in int_f:
				this_line = []
				
				for id_tuple, midle, round in line_generator(line.strip().split('\t')):
					
					if round == 1:
						this_line.append(dico_[id_tuple[0]])
						this_line.extend(midle)
						this_line.append(dico2[id_tuple[1]])
						
					else:
						this_line.extend(midle)
						this_line.append(dico2[id_tuple[1]])

				tmp.write('\t'.join(this_line) + '\n')

		os.rename("{}_.tmp".format(files), files)
	
		#child = subprocess.Popen("mv {} {}".format("{}_.tmp".format(files), files), shell=True)
		#child.wait()
	
	return 0
			
def main_part(dico_db, dico_query, edges_dir):
	
	dictionary_db = construct_dictionnary(dico_db)
	dictionary_query = construct_dictionnary(dico_query)
	
	first_edges = os.path.join(edges_dir, '_edges_1')
	all_edges = os.path.join(edges_dir, '_edges_')

	#print(dictionary_db)
	if os.path.exists(first_edges):
		rewrite_edges(dico_=dictionary_query, files=first_edges, val=0)
	if os.path.exists(all_edges):
		rewrite_edges(dico_=dictionary_query, files=all_edges, val=1, dico2=dictionary_db)
	
	