#/usr/local/bin/python3


#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

import os
import subprocess
import tempfile
import shutil


class IfbError(Exception):

	# base exeption class for this module
	pass


class NoSequenceFound(IfbError):

	def __init__(self, message):
		self.message = message

def line_generator(line_spt, reverse=False):
	""" input a line of the edges files allow to iterate over this line step by step"""
	# because I love generator
	# generator is beautiful
	# generator if life

	# input a line of the edges files:
	# allow to iterate over this line step by step

	if reverse:

		indice = 1

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[-indice], line_spt[-indice - 10].strip()
			if indice - 1 == 0:
				midle = line_spt[indice - 11: - 1]
			else:
				midle = line_spt[indice - 10:indice - 1]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)

	else:

		indice = 0

		while indice + 10 <= len(line_spt):
			id0, id1 = line_spt[indice], line_spt[indice + 10].strip()
			midle = line_spt[indice + 1:indice + 10]
			indice += 10
			yield (id0, id1), midle, int(indice / 10)


def get_seq_one_by_one(file_):
	"""Generator return prompt sequence for each sequence"""

	sequence = ''
	prompt = ''

	for line in file_:

		if line.startswith('>'):

			if sequence:
				yield [prompt, sequence]

			sequence = ''
			prompt = line.strip()[1:]

		else:
			sequence += line.strip()

	yield [prompt, sequence]


def chunck_sequence(sequence):
	inc = 80
	cpt = 0
	seq = ""
	while cpt <= len(sequence):
		seq += sequence[cpt:cpt + inc] + '\n'
		cpt += inc
	return seq.strip()


def grep_fasta(sequence_file, id_file, out_file):

	set_id = set()
	if os.path.isfile(id_file):
		with open(id_file) as id_:
			for line in id_:
				set_id.add(line.strip())
	else:
		set_id = set([elem.replace(">", '') for elem in id_file])

	nb_seq = len(set_id)
	cpt = 0
	with open(sequence_file, 'r') as seqfile, open(out_file, 'w') as out_:
		for prompt, sequ in get_seq_one_by_one(seqfile):
			if prompt in set_id:
				out_.write(">{}\n{}\n".format(prompt, chunck_sequence(sequ)))
				cpt += 1
			if cpt == nb_seq:
				return


def update_edges_find(file_, run_, id_get):

	if run_ > 1:
		field = 11 + 10 * (run_ - 1)
	else:
		field = 11
	col1 = subprocess.check_output('cut -f{} {}'.format(field, file_), shell=True).decode("utf-8").split("\n")
	for element in col1:
		id_get.add(element)
	id_get = set(list(filter(None, id_get)))
	return id_get


def remove(*args):

	for element in args:

		if os.path.isfile(element):
			child = subprocess.Popen('rm {}'.format(element + '*'), shell=True)
			child.wait()
		elif os.path.isdir(element):
			child = subprocess.Popen('rm -r {}'.format(element), shell=True)
			child.wait()
		else:
			print("Warning: file {} didn't exists and thus can't be remove".format(element))


# I should put it elsewhere:
def extract_seq(output_fa, output_seq, db_fa, run_=None, blast=None, edges=None):
	"""this fonction first extract target output id then get related sequence"""

	def extract_id_fromblast(blast, output):

		"""Internal fonction get target id from a blast"""

		child_cut = subprocess.Popen(['cut', '-f2', '{}'.format(blast)], stdout=subprocess.PIPE)
		child_sort = subprocess.Popen(['sort'], stdin=child_cut.stdout, stdout=subprocess.PIPE)
		child_uniq = subprocess.Popen('uniq > {}'.format(output), shell=True, stdin=child_sort.stdout)
		child_cut.wait()
		child_sort.wait()
		child_uniq.wait()

	def extract_id_edges(edges, output, run_):

		"""Internal fonction get target id from edges list"""
		if run_ > 1:
			field = 11 + 10 * (run_ - 1)
		else:
			field = 11

		child_cut = subprocess.Popen(['cut', '-f{}'.format(field), '{}'.format(edges)], stdout=subprocess.PIPE)
		child_sort = subprocess.Popen(['sort'], stdin=child_cut.stdout, stdout=subprocess.PIPE)
		child_uniq = subprocess.Popen('uniq > {}'.format(output), shell=True, stdin=child_sort.stdout)
		child_cut.wait()
		child_sort.wait()
		child_uniq.wait()

	if blast:
		extract_id_fromblast(blast=blast, output=output_seq)
	elif edges:
		extract_id_edges(edges=edges, output=output_seq, run_=run_)
	else:
		raise AttributeError(' you should provides at least blast or id_ parameters')

	try:
		assert os.path.getsize(output_seq) > 0

	except:
		raise NoSequenceFound('no more sequence find, cover to blast 152')
	# print('no more sequnce find, cover to blast 162')
	# sys.exit(0)

	grep_fasta(sequence_file=db_fa, id_file=output_seq, out_file=output_fa)

	try:
		assert os.path.getsize(output_fa) > 0

	except:
		raise NoSequenceFound('no more sequence find, cover to blast 162')


def make_fasta_from_edge_dir(edge_dir, out_file, metage_fasta):

	set_ids = set()

	list_of_edges_files = os.listdir(edge_dir)
	path_ = os.path.abspath(edge_dir)
	list_of_edges_files = [path_ + '/' + elem for elem in list_of_edges_files]

	for edge_file in list_of_edges_files:

		with open(edge_file) as ed_file:

			for line in ed_file:
				for ids, midle, round in line_generator(line.strip().split('\t')):
					id_1, id_2 = ids
					set_ids.add(id_1)
					set_ids.add(id_2)

	with open(out_file, 'a') as output_file, open(metage_fasta) as met_fa:

		for prompt, sequence in get_seq_one_by_one(file_=met_fa):
			if prompt in set_ids:
				output_file.write(">{}\n{}\n".format(prompt, chunck_sequence(sequence)))


def rename_fasta(dico_file, fasta):
	my_dico = {}

	with open(dico_file) as dico:

		for line in dico:
			spt = line.strip().split("\t")
			my_dico[spt[1]] = spt[0]
	temp_fasta = fasta + ".temp"

	with open(temp_fasta, 'w') as my_temp_file, open(fasta) as fasta_:

		for prompt, seq in get_seq_one_by_one(fasta_):
				my_temp_file.write(">{}\n{}\n".format(my_dico[prompt], chunck_sequence(seq)))

	os.rename(temp_fasta, fasta)


def copy_files(src: list, dest: str):
	with open(dest, 'wb') as out_:
		for input_file in src:
			with open(input_file, 'rb') as inp_:
				shutil.copyfileobj(inp_, out_, 1024*1024*10)




