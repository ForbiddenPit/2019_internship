from . import fonction

#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

class Node:

	def __init__(self, id_):

		self.id_ = id_

		self.relative_seq_start = None
		self.igraph_id = None
		self.cluster = None
		self.round = 0
		self.dn_ds = None
		self.environmental = None  # Bolean
		self.abundance_mean = None
		self.abundance_site_upper_thr = None

		self.nearest_ncbi_percent_id = 0
		#  # # if found a tuple: prot_id, tax_id, top_name, basename, '/'.join(list(reversed(liste_all)))
		self.phylogeny = tuple(["unknown"])
		self.nearest_ncbi_domain = "undef"
		self.nearest_ncbi_prot = "undef"

	def dump_node(self):

		liste_ = [self.id_]
		if self.environmental:
			liste_.append("env")
			liste_.append(self.relative_seq_start)
		else:
			liste_.append("ref")
			liste_.append("none")


		liste_.append(self.round)
		liste_.append(self.dn_ds)
		liste_.append(self.nearest_ncbi_prot)
		liste_.append(self.nearest_ncbi_percent_id)
		liste_.append(";".join(self.phylogeny))

		return list(map(str, liste_))



class Cluster:

	def __init__(self, id_):

		self.density = None
		self.id_ = id_
		self.liste_nodes = []
		self.compteur_domain = {}
		self.dist_to_ref = {}

		self.distance_all_vs_all = 0

		self.mean_percent_dist_all_vs_nr = 0
		#
		self.mean_mean_abundance = 0
		#self.sd_mean_abundance = None
		#self.max_mean_abundance = None
		#self.min_mean_abundance = None
		self.mean_nb_site = 0

		self.proportion_environmental_sequence = None
		self.dn_ds = None

	def dump(self):

		liste = []
		liste.append(self.id_)
		liste.append("{}".format(','.join(self.dist_to_ref)))
		liste.append(round(len(self.liste_nodes), 2))
		liste.append(self.proportion_environmental_sequence)
		liste.append(self.distance_all_vs_all)
		liste.append(self.mean_percent_dist_all_vs_nr)

		liste.append(self.density)
		liste.append(self.dn_ds)

		liste.append(self.mean_mean_abundance)
		liste.append(self.mean_nb_site)
		return list(map(str, liste))

	@property
	def as_ref(self):

		for node_instance in self.liste_nodes:

			if not node_instance.environmental:

				return True

		return False


	def get_ref_env_id(self):
		set_ref, set_env = set(), set()
		for node_instance in self.liste_nodes:

			if node_instance.environmental:
				set_env.add(node_instance.id_)

			else:
				set_ref.add(node_instance.id_)

		return (set_ref, set_env)


	def update(self, nr=True, ab=True):
		"""

		:return: None: Update the value of the Clusters based on properties from genes
		 """

		if ab:
			sum_ab = []
			sum_site = []

		number_sequence = len(self.liste_nodes)
		dv_nr = []
		#mean_to_node_incluster = []
		set_env, set_ref = set(), set()
		for node_instance in self.liste_nodes:
			dv_nr.append(float(node_instance.nearest_ncbi_percent_id))
			if node_instance.environmental:
				set_env.add(node_instance.id_)
			else:
				set_ref.add(node_instance.id_)
			if ab and node_instance.environmental:
				sum_ab.append(node_instance.abundance_mean)
				sum_site.append(node_instance.abundance_site_upper_thr)
				#print(sum_site, sum_ab, print(node_instance.abundance_mean, node_instance.abundance_site_upper_thr))

		self.proportion_environmental_sequence = str(round(len(set_env) / (len(set_ref) + len(set_env)), 4))
		if nr:
			self.mean_percent_dist_all_vs_nr = str(round(sum(dv_nr) / number_sequence, 4))

		if ab and float(self.proportion_environmental_sequence) > 0:
			try:
				#print(sum_site, print(round(sum(sum_site) / len(sum_site), 2)))
				self.mean_nb_site = round(sum(sum_site) / len(sum_site), 2)
				self.mean_mean_abundance = round(sum(sum_ab) / len(sum_ab), 4)
			except:
				print(self.__dict__)
				print(sum_ab, sum_site)
				raise





	def get_proportion(self, key):

		cpt = 0

		if key not in self.compteur_domain:
			return 0

		for elem in self.compteur_domain:
			cpt += self.compteur_domain[elem]

		return self.compteur_domain[key] / cpt




