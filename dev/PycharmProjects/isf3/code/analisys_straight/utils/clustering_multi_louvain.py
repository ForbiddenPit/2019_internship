import os
import ete3
from . import classes, fonction
import igraph


#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#


def parse_blastp_AvA(blastp, cover_th=None, pident_th=None):
	"""
	# this function do not check anything blast must be cleaned before hand
	:param blastp: a cleaned blast file
	:return: a dico edges -> tuple(liste[node_id], line.strip().split())
	"""

	dico_edges = {}
	liste_node = set()
	with open(blastp) as in_file:
		for line in in_file:
			spt = line.strip().split('\t')

			if cover_th and not fonction.check_cover(split_line=spt, cover_th=cover_th):
				continue

			if pident_th and float(spt[3]) < pident_th:
				continue

			# get the sequence id
			id_1 = spt[0]
			id_2 = spt[1]

			# edges is undirected so sorting them allow to consider reciprocal hit
			edges = tuple(sorted([id_1, id_2]))

			# if the edges exist we keep the ones with the best (minimal) evalue
			# may be better adding a scoring evelu and coverage?

			dico_edges[edges] = spt
			liste_node.add(id_1)
			liste_node.add(id_2)

	return list(liste_node), dico_edges


def make_graph(dico_edges, list_of_actual_nodes, dico_nodes):
	"""

	:param dico_edges: a dico edges (tuple(id1, id2)) -> blast line.strip().split()
	:param list_of_actual_nodes: a list of all nodes ids
	:return:
	"""

	edges = []
	nodes = []
	node_real_names = {}  # will contain dico[igraph_id] = realnames
	node_igraph = {}  # will contain dico[realnames] = igraph_id

	weights = []

	for indice, node in enumerate(list_of_actual_nodes):
		node_real_names[indice] = node
		node_igraph[node] = indice
		nodes.append(indice)
		dico_nodes[node].igraph_id = indice

	for keys in dico_edges:

		try:
			edges.append((node_igraph[keys[0]], node_igraph[keys[1]]))
			midle = dico_edges[keys]
			weights.append(float(midle[3]))
		except:
			print(dico_edges)
			print(node_igraph)
			print(keys)
			print(keys[0])
			raise

	return (edges, nodes, node_real_names, node_igraph, weights)


def build_networks_from_blast(blastp, dico_nodes, cover_th=None, pident_th=None):
	liste_node, dico_edges = parse_blastp_AvA(blastp=blastp,  cover_th=cover_th, pident_th=pident_th)
	edges, nodes, node_real_names, node_igraph, weights = make_graph(list_of_actual_nodes=liste_node,
																	 dico_edges=dico_edges, dico_nodes=dico_nodes)
	g = igraph.Graph(vertex_attrs={"labels": nodes}, edges=edges, directed=False)
	g.es["weights"] = weights
	return (g, node_real_names, node_igraph)


def make_dico_node_tocomm_and_dico_comm_to_node(graph, dico_nodes):
	dico_clusters = {}  # clusters id -> clusters object
	# dico_node_to_communities = {}  # dico_communities[nodes] = communities
	# dico_communities_to_node = {}  # dico[comm_id] = [nodes, ..., nodes]

	for node_id in dico_nodes:
		# get the node instance
		node_instance = dico_nodes[node_id]

		# get the communty in which this node belong
		if node_instance.igraph_id:
			community = graph.vs[node_instance.igraph_id]['com']

			# set the community of this node instance
			node_instance.cluster = community

	return dico_clusters


def making_liste(dico_node_to_communities, size):
	liste = [-1] * size
	for element in dico_node_to_communities:
		liste[int(element)] = int(dico_node_to_communities[element])

	return liste



def make_dico_cluster(big_tree: ete3.Tree, node_real_names, g, dico_seq_idto_node_instance, dico_level_com_liste_nodes,
                      dn_ds_tuple, nr=True,ab = True
                      ):
	dico_cluster = {}
	dico_seq_id_to_cluster = {}
	#order_leaf_name = []
	new_level = False
	old_level = 0

	for leaf in big_tree.traverse(strategy="levelorder"):

		cluster_instance = classes.Cluster(leaf.name)
		leaf.add_features(cluster=cluster_instance)

		liste_id_ = dico_level_com_liste_nodes[leaf.name]
		liste_node_instance = [dico_seq_idto_node_instance[node_real_names[elem]] for elem in liste_id_]
		cluster_instance.liste_nodes = liste_node_instance

		if len(liste_id_) == 1:
			cluster_instance.density = 1
			cluster_instance.mean_of_edges_weigth = 0
			cluster_instance.distance_all_vs_all = 0
			cluster_instance.subgraph = None

		else:

			try:
				subg = g.subgraph(g.vs.select(liste_id_))

			except:
				print(liste_id_)
				raise

			cluster_instance.density = round(subg.density(), 2)

			try:
				sum_edges = round(fonction.sum_edges_weigth(subg), 2)
			except:
				print(liste_id_)
				print(list(subg.vs))
				print(list(subg.es))
				raise

			actual_number_edges = fonction.number_edges(subg)
			cluster_instance.distance_all_vs_all = round(sum_edges / actual_number_edges, 2)
			cluster_instance.subgraph = subg


		#set_ref, set_env = set(), set()
		for seq_id in liste_id_:
			if seq_id in dico_seq_idto_node_instance:
				dico_seq_id_to_cluster[seq_id].append(cluster_instance)
			else:
				dico_seq_id_to_cluster[seq_id] = cluster_instance

		cluster_instance.update(nr, ab)

		dico_cluster[cluster_instance.id_] = cluster_instance



	return dico_cluster, dico_seq_id_to_cluster



def get_level_from_node(name, increase=True):
	level_node = int(name.split('_')[0])

	if increase:
		level_node += 1

	return str(level_node)


def rename_community_with_basename(community, subgraph):
	new_list = []

	for elem in community:
		new_list.append(subgraph.vs[elem]['basename'])

	new_list.sort()

	return new_list

def prune_tree(tree):
	leafs = tree.get_leaves()
	for leave in leafs:
		while len(leave.up.children) == 1:
			leave.up.delete()
			
def multi_louvain_tree(g, attr="weights"):
	dico_level_com_liste_nodes = {}
	name_top = "0_0"
	big_tree = ete3.Tree(name=name_top)
	big_tree.add_features(finish=False, levels=0)
	g_0 = list(range(0, len(g.vs), 1))  # [0,1,2... numbre node - 1]
	dico_level_com_liste_nodes[name_top] = g_0

	# first clustering
	c = g.community_multilevel(weights=attr)

	# add corresponding nodes to the tree
	for indice, com in enumerate(c):
		name = "{}_{}".format(str(1), indice)
		new_tree = ete3.Tree(name=name)
		new_tree.add_features(finish=False, levels=1)
		big_tree.add_child(new_tree)
		dico_level_com_liste_nodes[name] = com

	indice_while = 0
	uniq_id = 0

	while True:

		indice_while += 1

		liste_leaf = []  # a bit akward but we will modify the tree,
		#  this way we don't modify it while we itere on it
		finish_status = []

		for leaf in big_tree.get_leaves():
			# current leaves of the tree
			liste_leaf.append(leaf)

			finish_status.append(leaf.finish)
		# breaking condition
		count = [1 if elem else 0 for elem in finish_status]
		if sum(count) == len(finish_status):
			break

		for tree_node in liste_leaf:

			if tree_node.finish:
				continue

			name_ = tree_node.name

			subgraph = g.subgraph(g.vs.select(dico_level_com_liste_nodes[name_]))
			c = subgraph.community_multilevel(weights=attr)
			my_level = get_level_from_node(name_)

			if len(c) == 1:
				tree_node.finish = True
				continue

			for indice, com in enumerate(c):
				uniq_id += 1
				my_name = "{}_{}_{}".format(my_level, indice, str(uniq_id))
				new_tree = ete3.Tree(name=my_name)
				new_tree.add_features(finish=False, levels=int(my_level))
				tree_node.add_child(new_tree)

				new_com = rename_community_with_basename(com, subgraph)
				dico_level_com_liste_nodes[my_name] = new_com

		prune_tree(big_tree)
	return big_tree, dico_level_com_liste_nodes, indice_while



def main(dico_seq_idto_node_instance,
		 blast_, out_file_basename, dn_ds_tuple, cover_th=None, pident_th=None, nr=True,ab = True):

	print("making graph from blast")
	g, node_real_names, node_igraph = build_networks_from_blast(blastp=blast_, dico_nodes=dico_seq_idto_node_instance,
	                                                            cover_th=cover_th, pident_th=pident_th)

	# a basename attribute that conserve the igraph id, even when doing subgraph
	g_base_name = [elem.index for elem in g.vs]
	g.vs['basename'] = g_base_name

	print("starting clustering")
	print("making louvain tree and write_it")
	big_tree, dico_level_com_liste_nodes, indice_while = multi_louvain_tree(g=g, attr="weights")

	#out_tree = out_file_basename + "tree_networks.nwk"
	big_tree.write(outfile=os.path.join(out_file_basename + "/tree_networks.nwk"), format=1)


	with open(os.path.join(out_file_basename + "/tree_networks_which_seq.nwk"), 'w') as out_file:
		for leaves in big_tree.get_leaves():
			leaves_name = leaves.name
			list_id = dico_level_com_liste_nodes[leaves_name]
			good_id = [node_real_names[elem] for elem in list_id]
			out_file.write(">{}\n".format(leaves_name))
			out_file.write("{}\n".format("\n".join(good_id)))

	# here we need to create cluster instance an return them -> dico[cluster_id ] => cluster_instance
	dico_cluster, dico_seq_id_to_cluster = make_dico_cluster(big_tree=big_tree,
									 dico_seq_idto_node_instance=dico_seq_idto_node_instance,
									 g=g, node_real_names=node_real_names,
									 dico_level_com_liste_nodes=dico_level_com_liste_nodes,
									 dn_ds_tuple=dn_ds_tuple, nr=nr, ab = ab)

	return dico_cluster, dico_seq_id_to_cluster, big_tree, node_real_names, node_igraph, g
#
# if __name__ == "__main__":
# 	parser = argparse.ArgumentParser(description='')
# 	parser.add_argument('-in_', help='input')
# 	parser.add_argument('-out', help='out')
# 	args = parser.parse_args()
#
# 	main(blast_=args.in_, out_file_basename=args.out)
#
