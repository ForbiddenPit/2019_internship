from . import fonction
from multiprocessing import Pool
import igraph
import os


#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#

"""
This module aim to manage short path analyses in the pipe-line
I will take all Cluster and igraph

"""
def get_nodes_of_levels(tree, levels: int):
	"""

	:param tree: ete3 tree with levels attributes on node
	:param levels: an int
	:return:
	"""
	return [leaf for leaf in tree.traverse() if leaf.levels == levels]


def get_clusters_id_by_levels(big_tree):
	"""

	:param big_tree: ete3 describing a clustering, with special naming convention for leaf name : levels_sub_uniq
	:return: a [[clusteids levels top], ...,[ clustersid levels bottom]] || carefull if a cluster ends ( no leaf) before the bottom
	is added to succesive levels
	"""

	levels = 0
	node_list_levels = get_nodes_of_levels(big_tree, levels)

	leaf_at_levels = {}
	community_at_levels = {}
	maxlevels = big_tree.get_farthest_leaf()[1] + 1

	# second condition seems to be enough may be I will may be remove the first
	while node_list_levels and levels < maxlevels:

		node_list_levels = get_nodes_of_levels(big_tree, levels)  # get all node at a levels
		community_at_levels[levels] = node_list_levels  # assign it to our dict to tract

		for node in node_list_levels:

			if node.is_leaf():

				if levels in leaf_at_levels:
					leaf_at_levels[levels].append(node)

				else:
					leaf_at_levels[levels] = [node]

		for levels_range in range(0, levels, 1):

			if levels_range in leaf_at_levels:
				community_at_levels[levels].extend(leaf_at_levels[levels_range])

		levels += 1

	return community_at_levels, levels - 1


def my_concat(graph, liste_membership):
	"""

	:param graph: a graph from igraph
	:param liste_membership: a list memebership like [0, 0, 2, 2, 4, 5, 5]
	:return: a new concat graph
	"""

	g = graph.copy()
	g.contract_vertices(liste_membership)
	g.simplify()

	to_delete_ids = [v.index for v in g.vs if v.degree() == 0]
	g.delete_vertices(to_delete_ids)

	return g


def get_subg_for_a_levels(clusters_list, levels, graph, node_igraph, dico_cluster):

	liste_clustering = [0] * len(graph.vs)

	cluster_num = 0
	dico_subg_clusterId_to_igraph = {}
	dico_subg_igraph_to_clusterId = {}

	my_liste = clusters_list[levels]

	for cluster_node in my_liste:

		cluster_id = cluster_node.name
		cluster_instance = dico_cluster[cluster_id]
		for node in cluster_instance.liste_nodes:
			liste_clustering[node_igraph[node.id_]] = cluster_num


		dico_subg_clusterId_to_igraph[cluster_instance.id_] = cluster_num
		dico_subg_igraph_to_clusterId[cluster_num] = cluster_instance.id_
		cluster_num += 1
	sub_g = my_concat(graph, liste_clustering)

	return sub_g, dico_subg_clusterId_to_igraph, dico_subg_igraph_to_clusterId


def dist_dfs(g, node, attr, set_aim):

	# keep trace of attr levels found
	dico_find = dict((attr_value, None) for attr_value in set_aim)

	found = set.intersection(node[attr], set_aim)

	if node[attr] in set_aim:
		dico_find[attr] = 0

	# mark node if visited
	visited = [0] * len(g.vs)
	g.vs["visited"] = visited

	# init depth levels
	level = 0

	# init my queue
	my_queue = [g.vs[node.index], "null"]

	while my_queue:

		# get the first node_indice
		current = my_queue.pop(0)
		# update levels if needed
		if current == "null":
			level += 1

			if len(my_queue) == 0:
				print("break no more node in queue")
				return dico_find

			my_queue.append("null")

			# because null got no neighboor we need to get another node here
			current = my_queue.pop(0)

		found = set.intersection(current[attr], set_aim)
		if found:
			for elem in found:
				if dico_find[elem]is None:
					dico_find[elem] = level


		test_end = [1 if value is None else 0 for value in dico_find.values()]
		if sum(test_end) == 0:
			#print('all finded')
			return dico_find

		neighbors = g.neighbors(g.vs[current.index])
		unvisited_neighbors = [node for node in neighbors if g.vs[node]["visited"] == 0]

		# mark those nodes
		for node in unvisited_neighbors:
			g.vs[node]["visited"] = 1

		my_queue.extend([g.vs[indice] for indice in unvisited_neighbors])


def annot_subg(g, dico_subg_igraph_to_clusterId, dico_clusters):

	annot = [None] * len(g.vs)

	for node in g.vs:
		to_add = set()
		index = node.index
		cluster_instance = dico_clusters[dico_subg_igraph_to_clusterId[index]]

		if cluster_instance.as_ref:
			annot[index] = set(["ref"])
		else:
			annot[index] = set(["env"])

	g.vs["taxo_ref"] = annot


def annot_cluster(subg: igraph.Graph, dico_clusters: dict, dico_subg_clusterId_to_igraph: dict, dico_subg_igraph_to_clusterId: dict):

	annot_subg(g=subg, dico_subg_igraph_to_clusterId=dico_subg_igraph_to_clusterId, dico_clusters=dico_clusters)
	for vertex in subg.vs:
		indice = vertex.index
		clusters_id_ = dico_subg_igraph_to_clusterId[indice]
		cluster_instance = dico_clusters[clusters_id_]
		# if cluster_instance.proportion_environmental_sequence == 1:
		distances = dist_dfs(g=subg, node=vertex, attr="taxo_ref",
		         set_aim=set(["ref"]))

		if cluster_instance.dist_to_ref:
			cluster_instance.dist_to_ref.append(str(list(distances.values())[0]))

		else:
			cluster_instance.dist_to_ref = [str(list(distances.values())[0])]
			#dist_dfs(g=subg, node=vertex, attr="taxo_ref",
			#                                                 set_aim=set(["ref"]))

			#cluster_instance.dist_to_ref = dist_dfs(g=subg, node=vertex, attr="taxo_ref",
			#                                                     set_aim=set(["ref"]))
		#from_graph_get_mean_weight(subg)
#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#


def main(big_tree, dico_id_to_cluster, tuple_args, th, graph, node_igraph, dn_ds=False):

	dico_comm_at_levels, max_levels = get_clusters_id_by_levels(big_tree)

	# dfs analyses

	# get_subg_for_a_levels()
	for levels in range( max_levels + 1):

		sub_g, dico_subg_clusterId_to_igraph, dico_subg_igraph_to_clusterId = get_subg_for_a_levels(dico_comm_at_levels,
																									levels,
																									graph,
																									node_igraph,
																									dico_cluster=dico_id_to_cluster)

		annot_cluster(subg=sub_g, dico_clusters=dico_id_to_cluster, dico_subg_clusterId_to_igraph=dico_subg_clusterId_to_igraph,
	 						  dico_subg_igraph_to_clusterId=dico_subg_igraph_to_clusterId)

	if dn_ds:
		# dn_ds analyses
		seq_fna_file, db_fna_file, seq_find_bases, temp_dn_dsbase, log_file = tuple_args
		set_already_checked = set()
		comm_to_check = []

		cpt = 0
		for key, value in dico_comm_at_levels.items():

			for tree_node in value:
				comm_name = tree_node.name

				if key == 0:
					continue

				if comm_name in set_already_checked:
					continue

				comm_to_check.append(comm_name)
				set_already_checked.add(comm_name)


			liste_args = []
			for community_id in comm_to_check:
				cpt += 1

				temp_ds = temp_dn_dsbase + str(cpt)
				# while os.path.exists(temp_ds):
				# 	cpt += 1
				# 	temp_ds = temp_dn_dsbase + str(cpt)
				cluster_instance = dico_id_to_cluster[community_id]
				set_ref, set_env = cluster_instance.get_ref_env_id()
				if (len(set_env) + len(set_ref)) == 1:
					dico_id_to_cluster[community_id].dn_ds = "NA"
					continue
				tuple_for_args = (set_ref, set_env, db_fna_file, seq_fna_file, seq_find_bases, temp_ds, log_file)
				liste_args.append(tuple_for_args)
			try:
				with Pool(th) as pool:
					x = pool.map(fonction.get_dn_ds, liste_args)
			except:
				print("error file: {}".format(big_tree))
				raise
			try:
				real_indice = 0
				for indice, community_id in enumerate(comm_to_check):

					if dico_id_to_cluster[community_id].dn_ds == "NA":
						indice -= 1
						continue
					elif x[real_indice] == "NA":
						dico_id_to_cluster[community_id].dn_ds = None
					else:
						dico_id_to_cluster[community_id].dn_ds = round(x[real_indice], 4)
					real_indice += 1
			except:
				print(x, community_id, indice)
				print(len(comm_to_check), print(len(x)))
				print(comm_to_check)
				print(liste_args, sep='\n')
				raise
			comm_to_check = []

