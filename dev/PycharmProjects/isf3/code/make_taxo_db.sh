#
#        Written by Romain Lannes, 2016-2018
#
#        This file is part of IterativeSafeFinder.
#
#        IterativeSafeFinder is shared under Creative commons licence:
#
#        Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
#
#        See https://creativecommons.org/licenses/by-nc-sa/4.0/
#
check_sum="$( cd "$(dirname "$0")" ; pwd -P )"/check_md5sum.sh
echo $check_sum
wd=$1

mkdir $wd

cd $wd

boolean="false"
while [[ $boolean == "false" ]]
do
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz.md5

    boolean=$($check_sum "new_taxdump.tar.gz" "new_taxdump.tar.gz.md5")

    if [[ $boolean == "false" ]]
        then
        rm new_taxdump.tar.gz new_taxdump.tar.gz.md5
    else
        tar -zxvf new_taxdump.tar.gz
    fi
done


boolean="false"
while [[ $boolean == "false" ]]
do
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz.md5

    boolean=$($check_sum "prot.accession2taxid.gz" "prot.accession2taxid.gz.md5")

    if [[ $boolean == "false" ]]
        then
        rm prot.accession2taxid.gz prot.accession2taxid.gz.md5
    else
        gunzip prot.accession2taxid.gz
    fi

done

boolean="false"
while [[ $boolean == "false" ]]
do
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/pdb.accession2taxid.gz
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/pdb.accession2taxid.gz.md5

    boolean=$($check_sum "pdb.accession2taxid.gz" "pdb.accession2taxid.gz.md5")

    if [[ $boolean == "false" ]]
        then
            rm pdb.accession2taxid.gz pdb.accession2taxid.gz.md5
        else
            gunzip pdb.accession2taxid.gz
    fi
done


