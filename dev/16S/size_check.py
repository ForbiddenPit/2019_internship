#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import argparse
import os

parser = argparse.ArgumentParser(description='')

parser.add_argument('-i', help='input', required=True)

args = parser.parse_args()


def get_seq_one_by_one(file_):
    """Generator return a list with prompt, sequence for each sequence"""
    liste = list()
    bolean = False
    with open(file_) as file_:
        for line in file_:
            if line.startswith('>'):
                if bolean == True:
                    liste.append(sequence)
                    yield (liste)
                    liste = list()
                bolean = True
                liste.append(line.strip())
                sequence = ''
                continue
            if bolean == True:
                sequence += line.strip()
        liste.append(sequence)
        yield(liste)


def get_max_min_sizefa(fasta_file):
    """Return min and max sequence length in a fasta file"""

    min = 0
    max = 0
    Value = []
    init = True

    for sequence in get_seq_one_by_one(fasta_file):
        
        Value.append(len(sequence[1]))
        if init:
            min = len(sequence[1])
            max = len(sequence[1])
            init = False

        if len(sequence[1]) > max:
            max = len(sequence[1])
        if len(sequence[1]) < min:
            min = len(sequence[1])
    return min, max, Value


def moyenne(liste_element):
    return round(sum(liste_element) / len (liste_element), 2)


def variance(liste_elem):
    m = moyenne(liste_elem)
    return  moyenne([(x-m)**2 for x in liste_elem])


def ecart_type(liste_elem):
    return round(variance(liste_elem)**0.5, 2)


if os.path.isdir(args.i):
    list_dir = os.listdir(args.i)
    args.i = [os.path.abspath(args.i) + '/' + element for element in list_dir]

else:
    args.i = [args.i]
    
for element in args.i:
    if os.path.isfile(element):
        min_, max_, value = get_max_min_sizefa(element)
        print(os.path.basename(element).split('.')[0], (min_, max_), moyenne(value), ecart_type(value), len(value),
                sep='\t')