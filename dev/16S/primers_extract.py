#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

"""
Usage:
    ./primers_extract.py
        -f /home/disque_4To_A/pierre/data/color_peak/gg_13_5.fasta
        -d /home/disque_4To_A/pierre/data/color_peak/gg_13_5_taxonomy.txt
        -o /home/disque_4To_A/pierre/data/dallol/
"""

import os
import argparse
import re

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-f', '--fasta',
                    metavar='FILE', help='Fasta file (REQUIRED). Default: none.',
                    default='', dest='fasta_file')
parser.add_argument('-o', '--output',
                    metavar='DIR', help='Output directory (REQUIRED). Default: none.',
                    default='', dest='output_dir')
args = parser.parse_args()

# Function opening the dictionary table and putting it in a dictionary
def open_files(fasta_file):
    
    print("opening fasta file...")
    
    indices = dict()
    seqs = dict()
    output_name = os.path.basename(fasta_file).split('.')[0]
    with open(fasta_file) as f:
        i = 0
        for line in f:
            line = line.rstrip("\n")
            if re.search(">", line):
                line = line.strip(">")
                indices[i] = "{0}".format(line)
            else:
                seqs[i] = line
                i += 1
    f.close()

    print("file opened successfuly")
    return indices, seqs, output_name

def check_primers(seq):
    i1 = seq.find("CAGCAGCCGCGGTAATAC")
    i2 = seq.find("AAACTCAAAGGAATTGACGG")
    if i1 > -1 and i2 > -1 and i1 < i2:
        return True

def find_primers(seqs):
    selected = list()
    for i in range(0,len(seqs)):
        if check_primers(seqs[i]):
            selected.append(i)
    return selected

def write_fasta(selected, seqs, indices, output_dir, output_name):
    output = open('{0}/ptrim_{1}.fasta'.format(output_dir, output_name), 'a')
    for s in selected:
        output.write(">" + indices[s] + "\n" + seqs[s] + "\n")
    output.close()
    print("wrote output")

########
# Main #
########

# Args var storage
fasta_file = args.fasta_file
# dict_file = args.dict_file
output_dir = args.output_dir

# Store indices and sequences
indices, seqs, output_name = open_files(fasta_file)

# Find primers
selected = find_primers(seqs)

# Write fasta
write_fasta(selected, seqs, indices, output_dir, output_name)