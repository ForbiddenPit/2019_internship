#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import os
import argparse
import re
import sys

sys.path.append('/home/pierre/Documents/2019_internship/dev/16S/parser/')
from functions_16s import *

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-s', '--swarm',
                    metavar='FILE', help='Swarm file. Default: none.',
                    default='', dest='swarm_file')
parser.add_argument('-d', '--dict',
                    metavar='FILE', help='Dictionary file. Default: none.',
                    default='', dest='dict_file')
# parser.add_argument('-t', '--taxo',
#                     metavar='FILE', help='Taxonomy file. Default: none.',
#                     default='', dest='taxo_file')

args = parser.parse_args()

# -------------------------------------------------------------------

########
# Main #
########

# Args var storage
if args.swarm_file:
	swarm_file = args.swarm_file
# if args.dict_file:
# 	dict_file = args.dict_file
# if args.taxo_file:
# 	taxo_file = args.taxo_file

# dic = open_dict(dict_file)
blast_id(swarm_file)