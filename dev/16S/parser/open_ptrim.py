#!/usr/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin                                                             #
#                                                                            #
#  Master 2 Internship 2019                                                  #
#                                                                            #
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138        #
#  Evolution Paris Seine - Institut de Biologie Paris Seine                  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

# Function opening the dictionary table and putting it in a dictionary
def open_ptrim_files(swarm_file, derep_file, taxo_file):

    print("opening swarm, dictionary and taxonomy files...")
    
    swarm = []
    with open(swarm_file) as s:
        for line in s:
            ls = line.split(" ")
            for l in ls:
                swarm.append(l.split("_")[0])
    
    derep = dict()
    with open(derep_file) as d:
        for line in d:
            if line[0].isdigit():
                ls = line.split("\t")
                derep_id = ls[0].rstrip("\n")
                original_ids = ls[1].rstrip("\n").split(";")[:-1]
                if len(original_ids) > 1:
                    derep[derep_id] = [original_ids[0].strip(";")]
                    for i in range(1,len(original_ids)):
                        derep[derep_id].append(original_ids[i].strip(";"))
                else:
                    derep[derep_id] = [original_ids[0].strip(";")]
            else:
                continue
    
    taxo = dict()
    with open(taxo_file) as t:
        for line in t:
            ls = line.split("\t")
            taxo[ls[0].rstrip("\n")] = ls[1].rstrip("\n")

    print("files opened successfuly")
    return swarm, derep, taxo

def determine_swarm(swarm, derep, taxo):
    output = open("swarm_taxonomy",'w+')
    for swarm_id in swarm:
        for derep_id in derep[swarm_id]:
            output.write(taxo[derep_id]+"\n")
    output.close()