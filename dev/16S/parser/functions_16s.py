#!/usr/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
#  Pierre Martin															 #
#																			#
#  Master 2 Internship 2019												  #
#																			#
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138		#
#  Evolution Paris Seine - Institut de Biologie Paris Seine				  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

from Bio import SeqIO
import os
import re
import sys

# Function opening the dictionary table and putting it in a dictionary
def open_dict(dict_file):

	print('opening dictionary file...')

	dic = dict()
	with open(dict_file) as d:
		for line in d:
			if re.match('^[0-9]', line):
				ls = line.split('\t')
				derep_id = ls[0]
				original_id = ls[1].split(';')[0].split('_')[0]
				dic[derep_id] = original_id

	print('dictionary opened successfuly')
	return dic

def open_taxo(taxo_file):

	print('opening taxonomy file...')

	taxo = dict()
	list_taxo = set()
	with open(taxo_file) as t:
		for line in t:
			ls = line.split('\t')
			taxo_id = ls[0].rstrip('\n')
			whole_taxo = ls[1].split(' ')
			taxonomy = whole_taxo[0].strip(';')
			taxo[taxo_id] = taxonomy
			list_taxo.add(taxo_id)

	print('taxonomy opened successfuly')
	return taxo, list_taxo

def original_id(swarm_file, dic):
	output = open('oid_{}'.format(os.path.basename(swarm_file)), 'w+')
	with open(swarm_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			output.write('>'+str(dic[record.id.split('_')[0]])+'\n'+str(record.seq)+'\n')

def blast_id(fasta_file):
	output1 = open('blastid_{}'.format(os.path.basename(fasta_file).strip('oid_')), 'w+')
	output2 = open('count_table_{}'.format(os.path.basename(fasta_file).strip('oid_')), 'w+')
	count_table = 0
	with open(fasta_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			output1.write('>'+str(count_table)+'\n'+str(record.seq)+'\n')
			output2.write(str(count_table)+'\t'+str(record.id)+'\n')
			count_table += 1

def swarm_handling(swarm_file, dic, taxo):
	print('writing output taxo file...')
	output = open('otu_taxo_{}'.format(os.path.basename(swarm_file).strip('.fasta')), 'w')
	with open(swarm_file) as f:
		for record in SeqIO.parse(f, 'fasta'):
			swarm_id = record.id.split('_')[0]
			output.write('>'+str(swarm_id)+': '+taxo[dic[swarm_id]]+'\n')
	print('done')


def blast_handling(blast_file, taxo, list_taxo):

	def coverage(start, end, length):
		return round(((end - start + 1) / length * 100), 2)

	output = open('taxo_{}'.format(os.path.basename(blast_file)), 'w')
	line_count = 0

	with open(blast_file) as f:
		for line in f:

			line_count += 1

			ls = line.split()
			qStart = float(ls[5])
			qEnd = float(ls[6])
			qLen = float(ls[7])
			sStart = float(ls[8])
			sEnd = float(ls[9])
			sLen = float(ls[10])

			qCov = coverage(qStart, qEnd, qLen)
			sCov = coverage(sStart, sEnd, sLen)

			# if qCov > 80 and sCov > 80:

			id1 = ls[0]
			id2 = ls[1]
			eVal = ls[2]
			pid = round(float(ls[3]), 1)

			if id1 in list_taxo:
				taxo1 = taxo[id1]
			else:
				taxo1 = 'dallol'

			if id2 in list_taxo:
				taxo2 = taxo[id2]
			else:
				taxo2 = 'dallol'

			sys.stdout.write('Treating line:\t'+str(line_count)+'\r')
			sys.stdout.flush()

			output.write(id1+'\t'+id2+'\t'+taxo1+'\t'+taxo2+'\t'+eVal+'\t'+str(pid)+'\t'+str(qCov)+'\t'+str(sCov)+'\n')