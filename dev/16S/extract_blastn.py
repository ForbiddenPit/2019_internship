#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Pierre Martin

Master 2 Internship 2019

Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138
Evolution Paris Seine - Institut de Biologie Paris Seine
Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France
"""

import os
import argparse
import re

# Calling arguments from console when using the script
parser = argparse.ArgumentParser()
# Adding mandatory arguments:
parser.add_argument('-b', metavar='FILE', help='Blast file (REQUIRED). Default: none.',
					default='', dest='blast_file')
args = parser.parse_args()

########
# Main #
########

with open(args.blast_file) as b:
	for line in b:
		if line.startswith('Query='):
			file_name = line.split(' ')[1].rstrip('\n')
			output = open('split/{}'.format(file_name), 'w+')
		elif re.match('\s{2}[1-9]', line):
			output.write(line)
		else:
			continue
