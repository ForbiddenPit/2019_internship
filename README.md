# 2019_internship
#----------------------------------------------------------------------------#
#  Pierre Martin                                                             #
#                                                                            #
#  Master 2 Internship 2019                                                  #
#                                                                            #
#  Team 'Adaptation, Integration, Reticulation, Evolution' - UMR 7138        #
#  Evolution Paris Seine - Institut de Biologie Paris Seine                  #
#  Université Pierre et Marie Curie, 7 quai St Bernard, 75005 Paris, France  #
#----------------------------------------------------------------------------#

## TODO:

18/03  &rarr;  31/04 :
- Search for binned (or 'binnable') metagenomes; 
- Store all of them and try to find a binning method; 
- Document on Lansfield (Romain) & ColorPeak (Guillaume);

01/05  &rarr;  31/06:
- Analyse the data (ISF, then HMM); 
- Integrate HMM profiles into ISF;
- Optimize dereplication of unwanted data; 

01/07  &rarr;  07/08:
- Analyse the results, explore new methods; 
- Write the memoir (deadline 31/08);

08/08  &rarr;  31/08:
- Finish the memoir; 
- Work on the oral presentation;

01/09  &rarr;  18/09:
- Get some well-deserved rest, boy.